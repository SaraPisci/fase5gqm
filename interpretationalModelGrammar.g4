grammar interpretationalModelGrammar;

@parser::members {
  public void reportError(RecognitionException e) {
    throw new RuntimeException("I quit!\n" + e.getMessage());
  }
}

@lexer::members {
  public void reportError(RecognitionException e) {
    throw new RuntimeException("I quit!\n" + e.getMessage());
  }
}

main_expression: or_expression|and_expression|termin;

or_expression: (and_expression|termin) OR (and_expression|termin) | or_expression OR (and_expression|termin);

and_expression: termin AND termin | and_expression AND termin;

termin: metric condition number;

number: NUMBER | DOUBLE;

metric: ID;

condition: EQ | LEQ | GEQ | NEQ | GRET | LESS;


EQ: '==';
LEQ: '<=';
GEQ: '>=';
NEQ: '!=';
GRET: '>';
LESS: '<';

AND: 'and';
OR: 'or';

DOUBLE: NUMBER POINT NUMBER;

NUMBER: DIGIT+;

POINT: '.';

ID: ((LETTER) (LETTER | DIGIT | '_')*) | ('_' (LETTER | DIGIT | '_')+); // Can be componed of letters, digits


WS: [ \t\r\n]+ -> skip; // Ignoring spaces, tabs, returns and newlines
fragment LETTER: ('a'..'z'|'A'..'Z'|'è'|'ò'|'à'|'ù');
fragment DIGIT: '0'..'9';

