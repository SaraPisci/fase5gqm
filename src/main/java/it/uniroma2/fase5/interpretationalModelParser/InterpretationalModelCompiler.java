package it.uniroma2.fase5.interpretationalModelParser;


import it.uniroma2.fase5.utility.Threshold;

import java.util.ArrayList;

public class InterpretationalModelCompiler extends interpretationalModelGrammarBaseListener {

    private ArrayList<Threshold> terminList = new ArrayList<Threshold>();

    @Override
    public void exitTermin(interpretationalModelGrammarParser.TerminContext ctx) {
        super.exitTermin(ctx);
        String metric = ctx.metric().getText();
        System.out.println("metric = " + metric);
        Double threashold = Double.parseDouble(ctx.number().getText());
        System.out.println("threashold = " + threashold);
        String condition = ctx.condition().getText();
        System.out.println("condition = " + condition);

        terminList.add(new Threshold(metric, threashold, condition));

    }

    public ArrayList<Threshold> getTerminList() {
        return terminList;
    }
}
