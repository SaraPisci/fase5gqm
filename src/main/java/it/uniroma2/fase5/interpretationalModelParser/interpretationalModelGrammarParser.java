package it.uniroma2.fase5.interpretationalModelParser;// Generated from /Users/Cecilia/git/provaJEXL/src/interpretationalModelGrammar.g4 by ANTLR 4.7
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class interpretationalModelGrammarParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		EQ=1, LEQ=2, GEQ=3, NEQ=4, GRET=5, LESS=6, AND=7, OR=8, DOUBLE=9, NUMBER=10, 
		POINT=11, ID=12, WS=13;
	public static final int
		RULE_main_expression = 0, RULE_or_expression = 1, RULE_and_expression = 2, 
		RULE_termin = 3, RULE_number = 4, RULE_metric = 5, RULE_condition = 6;
	public static final String[] ruleNames = {
		"main_expression", "or_expression", "and_expression", "termin", "number", 
		"metric", "condition"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'=='", "'<='", "'>='", "'!='", "'>'", "'<'", "'and'", "'or'", null, 
		null, "'.'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "EQ", "LEQ", "GEQ", "NEQ", "GRET", "LESS", "AND", "OR", "DOUBLE", 
		"NUMBER", "POINT", "ID", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "interpretationalModelGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


	  public void reportError(RecognitionException e) {
	    throw new RuntimeException("I quit!\n" + e.getMessage());
	  }

	public interpretationalModelGrammarParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class Main_expressionContext extends ParserRuleContext {
		public Or_expressionContext or_expression() {
			return getRuleContext(Or_expressionContext.class,0);
		}
		public And_expressionContext and_expression() {
			return getRuleContext(And_expressionContext.class,0);
		}
		public TerminContext termin() {
			return getRuleContext(TerminContext.class,0);
		}
		public Main_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_main_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof interpretationalModelGrammarListener ) ((interpretationalModelGrammarListener)listener).enterMain_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof interpretationalModelGrammarListener ) ((interpretationalModelGrammarListener)listener).exitMain_expression(this);
		}
	}

	public final Main_expressionContext main_expression() throws RecognitionException {
		Main_expressionContext _localctx = new Main_expressionContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_main_expression);
		try {
			setState(17);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(14);
				or_expression(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(15);
				and_expression(0);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(16);
				termin();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Or_expressionContext extends ParserRuleContext {
		public TerminalNode OR() { return getToken(interpretationalModelGrammarParser.OR, 0); }
		public List<And_expressionContext> and_expression() {
			return getRuleContexts(And_expressionContext.class);
		}
		public And_expressionContext and_expression(int i) {
			return getRuleContext(And_expressionContext.class,i);
		}
		public List<TerminContext> termin() {
			return getRuleContexts(TerminContext.class);
		}
		public TerminContext termin(int i) {
			return getRuleContext(TerminContext.class,i);
		}
		public Or_expressionContext or_expression() {
			return getRuleContext(Or_expressionContext.class,0);
		}
		public Or_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_or_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof interpretationalModelGrammarListener ) ((interpretationalModelGrammarListener)listener).enterOr_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof interpretationalModelGrammarListener ) ((interpretationalModelGrammarListener)listener).exitOr_expression(this);
		}
	}

	public final Or_expressionContext or_expression() throws RecognitionException {
		return or_expression(0);
	}

	private Or_expressionContext or_expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Or_expressionContext _localctx = new Or_expressionContext(_ctx, _parentState);
		Or_expressionContext _prevctx = _localctx;
		int _startState = 2;
		enterRecursionRule(_localctx, 2, RULE_or_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(22);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				{
				setState(20);
				and_expression(0);
				}
				break;
			case 2:
				{
				setState(21);
				termin();
				}
				break;
			}
			setState(24);
			match(OR);
			setState(27);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				{
				setState(25);
				and_expression(0);
				}
				break;
			case 2:
				{
				setState(26);
				termin();
				}
				break;
			}
			}
			_ctx.stop = _input.LT(-1);
			setState(37);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Or_expressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_or_expression);
					setState(29);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(30);
					match(OR);
					setState(33);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
					case 1:
						{
						setState(31);
						and_expression(0);
						}
						break;
					case 2:
						{
						setState(32);
						termin();
						}
						break;
					}
					}
					} 
				}
				setState(39);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class And_expressionContext extends ParserRuleContext {
		public List<TerminContext> termin() {
			return getRuleContexts(TerminContext.class);
		}
		public TerminContext termin(int i) {
			return getRuleContext(TerminContext.class,i);
		}
		public TerminalNode AND() { return getToken(interpretationalModelGrammarParser.AND, 0); }
		public And_expressionContext and_expression() {
			return getRuleContext(And_expressionContext.class,0);
		}
		public And_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_and_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof interpretationalModelGrammarListener ) ((interpretationalModelGrammarListener)listener).enterAnd_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof interpretationalModelGrammarListener ) ((interpretationalModelGrammarListener)listener).exitAnd_expression(this);
		}
	}

	public final And_expressionContext and_expression() throws RecognitionException {
		return and_expression(0);
	}

	private And_expressionContext and_expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		And_expressionContext _localctx = new And_expressionContext(_ctx, _parentState);
		And_expressionContext _prevctx = _localctx;
		int _startState = 4;
		enterRecursionRule(_localctx, 4, RULE_and_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(41);
			termin();
			setState(42);
			match(AND);
			setState(43);
			termin();
			}
			_ctx.stop = _input.LT(-1);
			setState(50);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			while ( _alt!=2 && _alt!=ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new And_expressionContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_and_expression);
					setState(45);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(46);
					match(AND);
					setState(47);
					termin();
					}
					} 
				}
				setState(52);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class TerminContext extends ParserRuleContext {
		public MetricContext metric() {
			return getRuleContext(MetricContext.class,0);
		}
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public TerminContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_termin; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof interpretationalModelGrammarListener ) ((interpretationalModelGrammarListener)listener).enterTermin(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof interpretationalModelGrammarListener ) ((interpretationalModelGrammarListener)listener).exitTermin(this);
		}
	}

	public final TerminContext termin() throws RecognitionException {
		TerminContext _localctx = new TerminContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_termin);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(53);
			metric();
			setState(54);
			condition();
			setState(55);
			number();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public TerminalNode NUMBER() { return getToken(interpretationalModelGrammarParser.NUMBER, 0); }
		public TerminalNode DOUBLE() { return getToken(interpretationalModelGrammarParser.DOUBLE, 0); }
		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof interpretationalModelGrammarListener ) ((interpretationalModelGrammarListener)listener).enterNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof interpretationalModelGrammarListener ) ((interpretationalModelGrammarListener)listener).exitNumber(this);
		}
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_number);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(57);
			_la = _input.LA(1);
			if ( !(_la==DOUBLE || _la==NUMBER) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MetricContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(interpretationalModelGrammarParser.ID, 0); }
		public MetricContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_metric; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof interpretationalModelGrammarListener ) ((interpretationalModelGrammarListener)listener).enterMetric(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof interpretationalModelGrammarListener ) ((interpretationalModelGrammarListener)listener).exitMetric(this);
		}
	}

	public final MetricContext metric() throws RecognitionException {
		MetricContext _localctx = new MetricContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_metric);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(59);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public TerminalNode EQ() { return getToken(interpretationalModelGrammarParser.EQ, 0); }
		public TerminalNode LEQ() { return getToken(interpretationalModelGrammarParser.LEQ, 0); }
		public TerminalNode GEQ() { return getToken(interpretationalModelGrammarParser.GEQ, 0); }
		public TerminalNode NEQ() { return getToken(interpretationalModelGrammarParser.NEQ, 0); }
		public TerminalNode GRET() { return getToken(interpretationalModelGrammarParser.GRET, 0); }
		public TerminalNode LESS() { return getToken(interpretationalModelGrammarParser.LESS, 0); }
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof interpretationalModelGrammarListener ) ((interpretationalModelGrammarListener)listener).enterCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof interpretationalModelGrammarListener ) ((interpretationalModelGrammarListener)listener).exitCondition(this);
		}
	}

	public final ConditionContext condition() throws RecognitionException {
		ConditionContext _localctx = new ConditionContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_condition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(61);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ) | (1L << LEQ) | (1L << GEQ) | (1L << NEQ) | (1L << GRET) | (1L << LESS))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 1:
			return or_expression_sempred((Or_expressionContext)_localctx, predIndex);
		case 2:
			return and_expression_sempred((And_expressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean or_expression_sempred(Or_expressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean and_expression_sempred(And_expressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\17B\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\3\2\3\2\3\2\5\2\24\n\2\3\3"+
		"\3\3\3\3\5\3\31\n\3\3\3\3\3\3\3\5\3\36\n\3\3\3\3\3\3\3\3\3\5\3$\n\3\7"+
		"\3&\n\3\f\3\16\3)\13\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4\63\n\4\f\4"+
		"\16\4\66\13\4\3\5\3\5\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\b\2\4\4\6\t\2"+
		"\4\6\b\n\f\16\2\4\3\2\13\f\3\2\3\b\2A\2\23\3\2\2\2\4\25\3\2\2\2\6*\3\2"+
		"\2\2\b\67\3\2\2\2\n;\3\2\2\2\f=\3\2\2\2\16?\3\2\2\2\20\24\5\4\3\2\21\24"+
		"\5\6\4\2\22\24\5\b\5\2\23\20\3\2\2\2\23\21\3\2\2\2\23\22\3\2\2\2\24\3"+
		"\3\2\2\2\25\30\b\3\1\2\26\31\5\6\4\2\27\31\5\b\5\2\30\26\3\2\2\2\30\27"+
		"\3\2\2\2\31\32\3\2\2\2\32\35\7\n\2\2\33\36\5\6\4\2\34\36\5\b\5\2\35\33"+
		"\3\2\2\2\35\34\3\2\2\2\36\'\3\2\2\2\37 \f\3\2\2 #\7\n\2\2!$\5\6\4\2\""+
		"$\5\b\5\2#!\3\2\2\2#\"\3\2\2\2$&\3\2\2\2%\37\3\2\2\2&)\3\2\2\2\'%\3\2"+
		"\2\2\'(\3\2\2\2(\5\3\2\2\2)\'\3\2\2\2*+\b\4\1\2+,\5\b\5\2,-\7\t\2\2-."+
		"\5\b\5\2.\64\3\2\2\2/\60\f\3\2\2\60\61\7\t\2\2\61\63\5\b\5\2\62/\3\2\2"+
		"\2\63\66\3\2\2\2\64\62\3\2\2\2\64\65\3\2\2\2\65\7\3\2\2\2\66\64\3\2\2"+
		"\2\678\5\f\7\289\5\16\b\29:\5\n\6\2:\t\3\2\2\2;<\t\2\2\2<\13\3\2\2\2="+
		">\7\16\2\2>\r\3\2\2\2?@\t\3\2\2@\17\3\2\2\2\b\23\30\35#\'\64";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}