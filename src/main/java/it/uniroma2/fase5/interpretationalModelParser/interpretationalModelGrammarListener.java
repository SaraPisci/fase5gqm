package it.uniroma2.fase5.interpretationalModelParser;// Generated from /Users/Cecilia/git/provaJEXL/src/interpretationalModelGrammar.g4 by ANTLR 4.7
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link interpretationalModelGrammarParser}.
 */
public interface interpretationalModelGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link interpretationalModelGrammarParser#main_expression}.
	 * @param ctx the parse tree
	 */
	void enterMain_expression(interpretationalModelGrammarParser.Main_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link interpretationalModelGrammarParser#main_expression}.
	 * @param ctx the parse tree
	 */
	void exitMain_expression(interpretationalModelGrammarParser.Main_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link interpretationalModelGrammarParser#or_expression}.
	 * @param ctx the parse tree
	 */
	void enterOr_expression(interpretationalModelGrammarParser.Or_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link interpretationalModelGrammarParser#or_expression}.
	 * @param ctx the parse tree
	 */
	void exitOr_expression(interpretationalModelGrammarParser.Or_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link interpretationalModelGrammarParser#and_expression}.
	 * @param ctx the parse tree
	 */
	void enterAnd_expression(interpretationalModelGrammarParser.And_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link interpretationalModelGrammarParser#and_expression}.
	 * @param ctx the parse tree
	 */
	void exitAnd_expression(interpretationalModelGrammarParser.And_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link interpretationalModelGrammarParser#termin}.
	 * @param ctx the parse tree
	 */
	void enterTermin(interpretationalModelGrammarParser.TerminContext ctx);
	/**
	 * Exit a parse tree produced by {@link interpretationalModelGrammarParser#termin}.
	 * @param ctx the parse tree
	 */
	void exitTermin(interpretationalModelGrammarParser.TerminContext ctx);
	/**
	 * Enter a parse tree produced by {@link interpretationalModelGrammarParser#number}.
	 * @param ctx the parse tree
	 */
	void enterNumber(interpretationalModelGrammarParser.NumberContext ctx);
	/**
	 * Exit a parse tree produced by {@link interpretationalModelGrammarParser#number}.
	 * @param ctx the parse tree
	 */
	void exitNumber(interpretationalModelGrammarParser.NumberContext ctx);
	/**
	 * Enter a parse tree produced by {@link interpretationalModelGrammarParser#metric}.
	 * @param ctx the parse tree
	 */
	void enterMetric(interpretationalModelGrammarParser.MetricContext ctx);
	/**
	 * Exit a parse tree produced by {@link interpretationalModelGrammarParser#metric}.
	 * @param ctx the parse tree
	 */
	void exitMetric(interpretationalModelGrammarParser.MetricContext ctx);
	/**
	 * Enter a parse tree produced by {@link interpretationalModelGrammarParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(interpretationalModelGrammarParser.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link interpretationalModelGrammarParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(interpretationalModelGrammarParser.ConditionContext ctx);
}