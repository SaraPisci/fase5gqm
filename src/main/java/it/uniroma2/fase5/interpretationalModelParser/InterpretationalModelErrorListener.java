package it.uniroma2.fase5.interpretationalModelParser;

import org.antlr.v4.runtime.ConsoleErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

import java.util.ArrayList;

public class InterpretationalModelErrorListener extends ConsoleErrorListener{
    public String getParsingException() {
        return parsingException;
    }

    private String parsingException = new String("");

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
        parsingException = parsingException + msg + "; ";
        super.syntaxError(recognizer, offendingSymbol, line, charPositionInLine, msg, e);

        //System.exit(1);
    }
}
