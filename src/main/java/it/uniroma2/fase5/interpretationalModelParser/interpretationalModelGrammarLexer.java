package it.uniroma2.fase5.interpretationalModelParser;// Generated from /Users/Cecilia/git/provaJEXL/src/interpretationalModelGrammar.g4 by ANTLR 4.7
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class interpretationalModelGrammarLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		EQ=1, LEQ=2, GEQ=3, NEQ=4, GRET=5, LESS=6, AND=7, OR=8, DOUBLE=9, NUMBER=10, 
		POINT=11, ID=12, WS=13;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"EQ", "LEQ", "GEQ", "NEQ", "GRET", "LESS", "AND", "OR", "DOUBLE", "NUMBER", 
		"POINT", "ID", "WS", "LETTER", "DIGIT"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'=='", "'<='", "'>='", "'!='", "'>'", "'<'", "'and'", "'or'", null, 
		null, "'.'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "EQ", "LEQ", "GEQ", "NEQ", "GRET", "LESS", "AND", "OR", "DOUBLE", 
		"NUMBER", "POINT", "ID", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	  public void reportError(RecognitionException e) {
	    throw new RuntimeException("I quit!\n" + e.getMessage());
	  }


	public interpretationalModelGrammarLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "interpretationalModelGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\17a\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\3\2\3\2\3\2\3\3\3\3"+
		"\3\3\3\4\3\4\3\4\3\5\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\b\3\b\3\t\3\t\3"+
		"\t\3\n\3\n\3\n\3\n\3\13\6\13>\n\13\r\13\16\13?\3\f\3\f\3\r\3\r\3\r\3\r"+
		"\7\rH\n\r\f\r\16\rK\13\r\3\r\3\r\3\r\3\r\6\rQ\n\r\r\r\16\rR\5\rU\n\r\3"+
		"\16\6\16X\n\16\r\16\16\16Y\3\16\3\16\3\17\3\17\3\20\3\20\2\2\21\3\3\5"+
		"\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\2\37\2\3"+
		"\2\4\5\2\13\f\17\17\"\"\b\2C\\c|\u00e2\u00e2\u00ea\u00ea\u00f4\u00f4\u00fb"+
		"\u00fb\2g\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2"+
		"\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27"+
		"\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\3!\3\2\2\2\5$\3\2\2\2\7\'\3\2\2\2\t"+
		"*\3\2\2\2\13-\3\2\2\2\r/\3\2\2\2\17\61\3\2\2\2\21\65\3\2\2\2\238\3\2\2"+
		"\2\25=\3\2\2\2\27A\3\2\2\2\31T\3\2\2\2\33W\3\2\2\2\35]\3\2\2\2\37_\3\2"+
		"\2\2!\"\7?\2\2\"#\7?\2\2#\4\3\2\2\2$%\7>\2\2%&\7?\2\2&\6\3\2\2\2\'(\7"+
		"@\2\2()\7?\2\2)\b\3\2\2\2*+\7#\2\2+,\7?\2\2,\n\3\2\2\2-.\7@\2\2.\f\3\2"+
		"\2\2/\60\7>\2\2\60\16\3\2\2\2\61\62\7c\2\2\62\63\7p\2\2\63\64\7f\2\2\64"+
		"\20\3\2\2\2\65\66\7q\2\2\66\67\7t\2\2\67\22\3\2\2\289\5\25\13\29:\5\27"+
		"\f\2:;\5\25\13\2;\24\3\2\2\2<>\5\37\20\2=<\3\2\2\2>?\3\2\2\2?=\3\2\2\2"+
		"?@\3\2\2\2@\26\3\2\2\2AB\7\60\2\2B\30\3\2\2\2CI\5\35\17\2DH\5\35\17\2"+
		"EH\5\37\20\2FH\7a\2\2GD\3\2\2\2GE\3\2\2\2GF\3\2\2\2HK\3\2\2\2IG\3\2\2"+
		"\2IJ\3\2\2\2JU\3\2\2\2KI\3\2\2\2LP\7a\2\2MQ\5\35\17\2NQ\5\37\20\2OQ\7"+
		"a\2\2PM\3\2\2\2PN\3\2\2\2PO\3\2\2\2QR\3\2\2\2RP\3\2\2\2RS\3\2\2\2SU\3"+
		"\2\2\2TC\3\2\2\2TL\3\2\2\2U\32\3\2\2\2VX\t\2\2\2WV\3\2\2\2XY\3\2\2\2Y"+
		"W\3\2\2\2YZ\3\2\2\2Z[\3\2\2\2[\\\b\16\2\2\\\34\3\2\2\2]^\t\3\2\2^\36\3"+
		"\2\2\2_`\4\62;\2` \3\2\2\2\n\2?GIPRTY\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}