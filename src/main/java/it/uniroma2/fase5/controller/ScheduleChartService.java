package it.uniroma2.fase5.controller;

import it.uniroma2.fase5.model.rest.DTOresponse;

import org.json.JSONException;
import org.springframework.http.ResponseEntity;

public interface ScheduleChartService {

	ResponseEntity<DTOresponse> createScheduleChart(String scheduleId,String fase, String startDate,String lastDate) throws JSONException;
	ResponseEntity<DTOresponse> getScheduleChart();
	ResponseEntity<DTOresponse> deleteAllScheduleChart();
	ResponseEntity<DTOresponse> deleteScheduleChart(String scheduleId);
}
