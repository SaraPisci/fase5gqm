package it.uniroma2.fase5.controller.implementation;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import it.uniroma2.fase5.controller.SendingOutComesFase6Service;
import it.uniroma2.fase5.model.SendingOutComesFase6;
import it.uniroma2.fase5.model.rest.DTOresponse;
import it.uniroma2.fase5.repositories.SendingOutComesFase6Repository;

import it.uniroma2.isssr.integrazione.BusException;
import it.uniroma2.isssr.integrazione.BusMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class SendingOutComesFase6ServiceImpl implements SendingOutComesFase6Service {

	//private String busAdd = "http://ermes.sweng.uniroma2.it/Bus/inboundChannel.html";
	@Value("${bus.address}")
	private String busAdd;

	@Autowired
	SendingOutComesFase6Repository sendingOutComesFase6repository;

	@Override
	public ResponseEntity<DTOresponse> createSendingOutComeFase6(String fileId,
																 String json) throws JSONException {



		System.out.println("JSON-------------->" + json.toString());
		JSONObject jo = new JSONObject(json);

		for (int i=0; i<jo.length();i++) {

			if (jo.get("type").equals("questions")) {

				// TODO Auto-generated method stub
				SendingOutComesFase6 sending = new SendingOutComesFase6(fileId, json);
				sendingOutComesFase6repository.save(sending);

				JSONObject jsonRead = new JSONObject();

				String busAddress = this.busAdd;
				String msg = null;//msg di ritorno
				String payload = json;
				/* il formato da adottare per le nostre enità nel db hanno formato
				 * base64-Fase5-typeObject													*/
				System.out.println("\n\n\n SENDING QUESTIONS-------------> " + json + "<------------------\n\n\n");
				jsonRead.put("objIdLocalToPhase", "6");
				jsonRead.put("typeObj", "base64-question");
				jsonRead.put("instance", "question " + Math.round(100 * Math.random()));
				jsonRead.put("busVersion", "1");
				jsonRead.put("tags", "[]");
				jsonRead.put("payload", payload);

				try {
					System.out.println("\n\n\n-------------->" + jsonRead.toString() + "<-----------------------\n\n\n");
					BusMessage busMs = new BusMessage(BusMessage.OPERATION_CREATE, "phase6", jsonRead.toString());
					msg = busMs.send(busAddress);
					System.out.println("MSG QUESTION: " + msg);


				} catch (MalformedURLException ex) {
					ex.printStackTrace();
				} catch (IOException ioex) {
					ioex.printStackTrace();
				} catch (BusException e) {
					e.printStackTrace();
				}

			}

			else if (jo.get("type").equals("metric")) {

				// TODO Auto-generated method stub
				SendingOutComesFase6 sending = new SendingOutComesFase6(fileId, json);
				sendingOutComesFase6repository.save(sending);

				JSONObject jsonRead = new JSONObject();

				String busAddress = this.busAdd;
				String msg = null;//msg di ritorno
				String payload = json;
				/* il formato da adottare per le nostre enità nel db hanno formato
				 * base64-Fase5-typeObject													*/
				System.out.println("\n\n\n SENDING METRIC-------------> " + json + "<------------------\n\n\n");
				jsonRead.put("objIdLocalToPhase", "6");
				jsonRead.put("typeObj", "base64-metric");
				jsonRead.put("instance", "metric prova" + Math.round(100 * Math.random()));
				jsonRead.put("busVersion", "1");
				jsonRead.put("tags", "[]");
				jsonRead.put("payload", payload);

				try {
					System.out.println("\n\n\n-------------->" + jsonRead.toString() + "<-----------------------\n\n\n");
					BusMessage busMs = new BusMessage(BusMessage.OPERATION_CREATE, "phase6", jsonRead.toString());
					msg = busMs.send(busAddress);
					System.out.println("MSG METRIC: " + msg);


				} catch (MalformedURLException ex) {
					ex.printStackTrace();
				} catch (IOException ioex) {
					ioex.printStackTrace();
				} catch (BusException e) {
					e.printStackTrace();
				}

			}
		}

		DTOresponse dtoresponse = new DTOresponse();
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse, HttpStatus.OK);

		return response;
	}

	@Override
	public ResponseEntity<DTOresponse> getSendingOutComeFase6() {
		List<SendingOutComesFase6> sendings = sendingOutComesFase6repository.findAll();
		System.out.println("sendings = " + sendings);
		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setSendingOutcomesFase6(sendings);

		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(
				dtoresponse, HttpStatus.OK);

		return response;
	}

	@Override
	public ResponseEntity<DTOresponse> deleteAllSendingOutComeFase6() {
		// TODO Auto-generated method stub
		sendingOutComesFase6repository.delete(sendingOutComesFase6repository.findAll());
		return null;
	}


}
