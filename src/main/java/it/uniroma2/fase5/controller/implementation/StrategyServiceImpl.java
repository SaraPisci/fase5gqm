package it.uniroma2.fase5.controller.implementation;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import it.uniroma2.fase5.model.classesFromBus.external.OrganizationalGoalBus;
import it.uniroma2.fase5.model.classesFromBus.repository.OrganizationalGoalRepository;
import it.uniroma2.isssr.integrazione.BusException;
import it.uniroma2.isssr.integrazione.BusMessage;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import it.uniroma2.fase5.controller.StrategyService;
import it.uniroma2.fase5.model.Strategy;
import it.uniroma2.fase5.model.rest.DTOresponse;
import it.uniroma2.fase5.repositories.StrategyRepository;

@Service("StrategyService")
public class StrategyServiceImpl implements StrategyService {

	@Autowired
	StrategyRepository strategyRepository;
	@Autowired
	OrganizationalGoalRepository goalRepository;

	//private String busAdd = "http://ermes.sweng.uniroma2.it/Bus/inboundChannel.html";
	@Value("${bus.address}")
	private String busAdd;

	@Override
	public ResponseEntity<DTOresponse> createStrategy(String strategyId,String strategyName,
			List<String> context, List<String> assumptions, String description,
			String creationDate, String lastModified, String timeFrame,
			int version,List<String> goalRef) throws JSONException {
		// TODO Auto-generated method stub
		List<OrganizationalGoalBus> temp= new ArrayList<OrganizationalGoalBus>();
		for( String s : goalRef ){
			if(goalRepository.findOne(s)!=null)
				temp.add(goalRepository.findOne(s));				
		}
		if (temp.size()==0){
			return new ResponseEntity<DTOresponse>(new DTOresponse(),HttpStatus.BAD_REQUEST);
		}
		
		Strategy strategy = new Strategy(strategyId,strategyName, context, assumptions,description, creationDate, lastModified, timeFrame, version, temp);
		strategyRepository.save(strategy);

		JSONObject jsonRead = new JSONObject();

		String busAddress= this.busAdd;
		String msg= null;//msg di ritorno
		String payload = "{\"strategyId\":\"" + strategy.getStrategyId() + "\","+ "\"strategyName\":\"" + strategy.getStrategyName()+ "\","+ "\"description\":\"" + strategy.getDescription() + "\","+ "\" creationDate\":\""+ strategy.getCreationDate() + "\","+ "\"lastModified\":\"" + strategy.getCreationDate()+ "\","+ "\"timeFrame\":\"" + strategy.getTimeFrame() + "\","+ "\"context\":\"" + strategy.getContext()
				+ "\","+ "\"assumptions\":\"" + strategy.getAssumptions() + "\"," + "\"version\":\"" + strategy.getVersion() + "\","+ "\"goalRef\":\"" + strategy.getGoalRef() +"\"}";
		/* il formato da adottare per le nostre enità nel db hanno formato
		 * base64-typeObject													*/

		jsonRead.put("objIdLocalToPhase", "5");
		jsonRead.put("typeObj", "base64-strategy");
		jsonRead.put("instance", "strategy"+strategy.getStrategyId());
		jsonRead.put("busVersion", "1");
		jsonRead.put("tags", "[]");
		jsonRead.put("payload" , payload);

		try {
			System.out.println("\n\n\n-------------->"+jsonRead.toString()+"<-----------------------\n\n\n");
			BusMessage busMs = new BusMessage(BusMessage.OPERATION_CREATE,"phase5",jsonRead.toString());
			msg= busMs.send(busAddress);
			System.out.println("MSG STRATEGY: "+msg);


		} catch(MalformedURLException ex) {
			ex.printStackTrace();
		} catch(IOException ioex) {
			ioex.printStackTrace();
		}catch(BusException e){
			e.printStackTrace();
		}


		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setMessage(msg);
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(
				dtoresponse, HttpStatus.OK);

		return response;
	}
	@Override
	public ResponseEntity<DTOresponse> createStrategy(String strategyId,String strategyName,
			List<String> context, List<String> assumptions, String description,
			String creationDate, String lastModified, String timeFrame,
			int version) throws JSONException {
		Strategy strategy = new Strategy(strategyId, strategyName,context, assumptions,
				description, creationDate, lastModified, timeFrame, version);
		strategyRepository.save(strategy);

		JSONObject jsonRead = new JSONObject();

		String busAddress= this.busAdd;
		String msg= null;//msg di ritorno
		String payload = "{\"strategyId\":\"" + strategy.getStrategyId() + "\","+ "\"strategyName\":\"" + strategy.getStrategyName()+ "\","+ "\"description\":\"" + strategy.getDescription() + "\","+ "\" creationDate\":\""+ strategy.getCreationDate() + "\","+ "\"lastModified\":\"" + strategy.getCreationDate()+ "\","+ "\"timeFrame\":\"" + strategy.getTimeFrame() + "\","+ "\"context\":\"" + strategy.getContext()
				+ "\","+ "\"assumptions\":\"" + strategy.getAssumptions() + "\"," + "\"version\":\"" + strategy.getVersion() +"\"}";
		/* il formato da adottare per le nostre enità nel db hanno formato
		 * base64-typeObject													*/

		jsonRead.put("objIdLocalToPhase", "5");
		jsonRead.put("typeObj", "base64-strategy");
		jsonRead.put("instance", "strategy"+strategy.getStrategyId());
		jsonRead.put("busVersion", "1");
		jsonRead.put("tags", "[]");
		jsonRead.put("payload" , payload);

		try {
			System.out.println("\n\n\n-------------->"+jsonRead.toString()+"<-----------------------\n\n\n");
			BusMessage busMs = new BusMessage(BusMessage.OPERATION_CREATE,"phase5",jsonRead.toString());
			msg= busMs.send(busAddress);
			System.out.println("MSG STRATEGY: "+msg);


		} catch(MalformedURLException ex) {
			ex.printStackTrace();
		} catch(IOException ioex) {
			ioex.printStackTrace();
		}catch(BusException e){
			e.printStackTrace();
		}

		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setMessage(msg);
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(
				dtoresponse, HttpStatus.OK);

		return response;
		
	}

	@Override
	public ResponseEntity<DTOresponse> getStrategies() {
		List<Strategy> strategies = strategyRepository.findAll();

		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setStrategies(strategies);

		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(
				dtoresponse, HttpStatus.OK);

		return response;

	}

	@Override
	public ResponseEntity<DTOresponse> deleteStrategy(String strategyId) {
		// TODO Auto-generated method stub

		strategyRepository.delete(strategyRepository.findOne(strategyId));

		return null;
	}

}
