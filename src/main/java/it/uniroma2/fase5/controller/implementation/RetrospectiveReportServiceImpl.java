package it.uniroma2.fase5.controller.implementation;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Random;

import it.uniroma2.fase5.model.Feedback;
import it.uniroma2.fase5.repositories.FeedbackRepository;
import it.uniroma2.isssr.integrazione.BusException;
import it.uniroma2.isssr.integrazione.BusMessage;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import it.uniroma2.fase5.controller.RetrospectiveReportService;
import it.uniroma2.fase5.model.RetrospectiveReport;
import it.uniroma2.fase5.model.rest.DTOresponse;
import it.uniroma2.fase5.repositories.RetrospectiveReportRepository;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Service("RetrospectiveReportService")
public class RetrospectiveReportServiceImpl implements RetrospectiveReportService {

	@Autowired
	RetrospectiveReportRepository retrospectiveReportRepository;

	@Autowired
	FeedbackRepository feedbackRepository;
	//private String busAdd = "http://ermes.sweng.uniroma2.it/Bus/inboundChannel.html";
	@Value("${bus.address}")
	private String busAdd;

	private String check_phase(int i){
		String phase = "";
		if (i==0)
			phase = "phase0";
		if (i==1)
			phase = "phase1";
		if (i==2)
			phase = "phase2";
		if (i==3)
			phase = "phase3";
		if (i==4)
			phase = "phase4";
		if (i==5)
			phase = "phase5";
		if (i==6)
			phase = "phase6";

		return phase;
	}
	
	@Override
	public ResponseEntity<DTOresponse> deleteRetrospectiveReport(String retrospectiveReportId) {

		RetrospectiveReport rr = retrospectiveReportRepository.findOne(retrospectiveReportId);
		retrospectiveReportRepository.delete(rr);
		for (Feedback fb: rr.getFeedbackList()){
			feedbackRepository.delete(fb);
		}
		return null;
	}
	
	@Override
	public ResponseEntity<DTOresponse> deleteAllRetrospectiveReport() {

		List<RetrospectiveReport> list = retrospectiveReportRepository.findAll();
		retrospectiveReportRepository.delete(list);
		for (RetrospectiveReport rr: list) {
			for (Feedback fb: rr.getFeedbackList()) {
				feedbackRepository.delete(fb);
			}
		}
		return null;
	}

	@Test
	@Override
	public ResponseEntity<DTOresponse> createRetrospectiveReport(String retrospectiveReportId, String conclusion, String lastModified, List<Integer> phases, List<String> priorityList, List<String> descriptionList) throws JSONException {

		List<Feedback> feedbackList=new ArrayList<Feedback>();

		List<String> feedbackRef=new ArrayList<>();
		System.out.println("Fasi"+phases.get(0));

		Random random=new Random();
		for(int i=0;i<phases.size();i++){
			Feedback feedback=new Feedback(Integer.toString(random.nextInt(1400)),phases.get(i),priorityList.get(i),descriptionList.get(i));
			feedbackRepository.save(feedback);
			//feedbackList.add(feedback);
			feedbackRef.add(feedback.getFeedbackID());

		}


		//TODO Restituisce Feedback@ non si sa il perchè
		for( String s :feedbackRef){
			if(feedbackRepository.findOne(s)!=null)
				System.out.println("REPO"+feedbackRepository.findOne(s));
				feedbackList.add(feedbackRepository.findOne(s));
		}
		

		RetrospectiveReport retrospectiveReport= new RetrospectiveReport(retrospectiveReportId, conclusion,lastModified,feedbackList);
		retrospectiveReportRepository.save(retrospectiveReport);

		JSONObject jsonRead = new JSONObject();
		String busAddress= this.busAdd;
		String msg= null;//msg di ritorno

		List<String> encodedFeedbacks = new ArrayList<>();
		for (Feedback i: retrospectiveReport.getFeedbackList()){
			encodedFeedbacks.add("\""+new String(Base64.getEncoder().encode(i.toString().getBytes()))+"\"");
		}

		String payload = "{\"retrospectiveReportId\":\"" + retrospectiveReport.getRetrospectiveReportId() + "\","+ "\"conclusion\":\"" + new String (Base64.getEncoder().encode(retrospectiveReport.getConclusion().getBytes()))+ "\","+ "\"lastModified\":\"" + retrospectiveReport.getLastModified() + "\", \"feedbackList\":"+encodedFeedbacks +" }";
		/* il formato da adottare per le nostre enità nel db hanno formato
		 * base64-Fase5-typeObject													*/

		jsonRead.put("objIdLocalToPhase", "5");
		jsonRead.put("typeObj", "base64-retrospectiveReport");
		jsonRead.put("instance", "RETROSPECTIVE REPORT ->"+retrospectiveReport.getRetrospectiveReportId());
		jsonRead.put("busVersion", "1");
		jsonRead.put("tags", "[]");
		jsonRead.put("payload" , payload);

		System.out.println(phases.get(0));

		for (int i=0; i<feedbackList.size();i++) {
			try {
				String phase="";

				phase = check_phase(feedbackList.get(i).getPhase());
				System.out.println("\n\n\n-------------->" + jsonRead.toString() + "<-----------------------\n\n\n");
				BusMessage busMs = new BusMessage(BusMessage.OPERATION_CREATE, phase, jsonRead.toString());
				msg = busMs.send(busAddress);
				System.out.println("MSG RETROSPECTIVEREPORT: " + msg);


			} catch (MalformedURLException ex) {
				ex.printStackTrace();
			} catch (IOException ioex) {
				ioex.printStackTrace();
			} catch (BusException e) {
				e.printStackTrace();
			}

		}

		DTOresponse dtoresponse = new DTOresponse();
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse,HttpStatus.OK);
		assertTrue(true);

		return response;
	}

	@Override
	public ResponseEntity<DTOresponse> getRetrospectiveReports() {
		
		List<RetrospectiveReport> retrospectiveReport = retrospectiveReportRepository.findAll();
		
		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setRetrospectiveReports(retrospectiveReport);
		
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse,HttpStatus.OK);	
	
		return response;
		
	}
	
	
	@Override
	public ResponseEntity<DTOresponse> getRetrospectiveReportsById(String retrospectiveReportId) {
		
		List<RetrospectiveReport> RetrospectiveReport = retrospectiveReportRepository.findAll();
		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setRetrospectiveReports(RetrospectiveReport);
		System.out.println("CIAOOOOOOOOO"+dtoresponse.getRetrospectiveReports().get(0).getIds());
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse,HttpStatus.OK);	
	
		return response;
		
	}
}
