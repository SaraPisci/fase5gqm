package it.uniroma2.fase5.controller.implementation;

import it.uniroma2.fase5.controller.BusService;
import it.uniroma2.fase5.controller.RetrospectiveReportService;
import it.uniroma2.fase5.controller.StrategyService;
import it.uniroma2.fase5.model.Bus;
import it.uniroma2.fase5.model.RetrospectiveReport;
import it.uniroma2.fase5.model.ScheduleChart;
import it.uniroma2.fase5.model.SendingOutComesFase6;
import it.uniroma2.fase5.model.ValidateData;
import it.uniroma2.fase5.model.classesFromBus.external.InstanceProject;
import it.uniroma2.fase5.model.classesFromBus.repository.*;
import it.uniroma2.fase5.model.classesFromBus.service.MeasurementGoalBusService;
import it.uniroma2.fase5.model.classesFromBus.service.OntologyBusService;
import it.uniroma2.fase5.model.classesFromBus.service.OrganizationalGoalService;
import it.uniroma2.fase5.model.classesFromBus.service.QuestionBusService;
import it.uniroma2.fase5.model.rest.DTOresponse;
import it.uniroma2.fase5.repositories.BusRepository;
import it.uniroma2.fase5.repositories.RetrospectiveReportRepository;
import it.uniroma2.fase5.repositories.ScheduleChartRepository;
import it.uniroma2.fase5.repositories.SendingOutComesFase6Repository;
import it.uniroma2.fase5.repositories.StrategyRepository;
import it.uniroma2.fase5.repositories.ValidateDataRepository;
import it.uniroma2.isssr.integrazione.BusException;
import it.uniroma2.isssr.integrazione.BusMessage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@Service("BusService")
public class BusServiceImplementation implements BusService {

	@Autowired
	BusRepository busRepository;
	@Autowired
	MeasurementGoalBusService measurementGoalService;
	@Autowired
	OrganizationalGoalService goalService;
	@Autowired
	StrategyService strategyService;
	@Autowired
	QuestionBusService questionService;
	@Autowired
	OntologyBusService ontologyBusService;
	@Autowired
	RetrospectiveReportService retrospectiveReportService;
	@Autowired
	OntologyBusRepository ontologyBusRepository;
	@Autowired
	InstanceProjectRepository projectRepository;
	@Autowired
	QuestionBusRepository questionRepository;
	@Autowired
	MeasurementGoalBusRepository measurementRepository;
	@Autowired
	OrganizationalGoalRepository goalRepository;
	@Autowired
	StrategyRepository strategyRepository;
	@Autowired
	RetrospectiveReportRepository retrospectiveReportRepository;
	@Autowired
	ScheduleChartRepository scheduleChartRepository ;
	@Autowired
	SendingOutComesFase6Repository sendingOutComesFase6Repository ;
	@Autowired
	ValidateDataRepository validateDataRepository;

	@Value("${bus.address}")
	private String busAddress;
	//static final String busAddress = "http://ermes.sweng.uniroma2.it/Bus/inboundChannel.html";


	@Override
	public ResponseEntity<DTOresponse> sendBusMessage(String tag,
			List<String> content, String typeObject, String originAddress,
			String resolveAddress, String id) {
		content = new ArrayList<String>();
		System.out.println("Send BUS MESSAGE");
		switch (typeObject.toLowerCase()) {
			case "projectId" :
				content.add("Fase5");
				content.add("READ");
				try {
					  String html = "";
					  URL url = new URL("http://localhost:8082/getProjectId/");
					  HttpURLConnection connection = (HttpURLConnection) url.openConnection();
					  BufferedReader read = new BufferedReader(new InputStreamReader(connection.getInputStream()));
					  String line = read.readLine();
					  
					  while(line!=null) {
					    html += line;
					    line = read.readLine();
					  }
					  if(!html.equals(""))
						  content.add(html);
			//		  System.out.println(content);
					  
					} catch(MalformedURLException ex) {
					        ex.printStackTrace();
					} catch(IOException ioex) {
					        ioex.printStackTrace();
					}
							break;
		case "goal":
			content.add("Fase5");
			content.add("READ");
			try {
				String html = "";
				System.out.println("GUARDA!");
				URL url = new URL("http://localhost:8082/getGoals/");
				  HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				  BufferedReader read = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				  String line = read.readLine();

				  System.out.println("GUARDA " + line);
				  
				  while(line!=null) {
				    html += line;
				    line = read.readLine();
				  }
				  if(!html.equals(""))
					  content.add(html);
		//		  System.out.println(content);
				  
				} catch(MalformedURLException ex) {
				        ex.printStackTrace();
				} catch(IOException ioex) {
				        ioex.printStackTrace();
				}
		
			break;
		case "strategy":
			content.add("Fase5");
			content.add("READ");
			try {
				  String html = "";
				  URL url = new URL("http://localhost:8082/getStrategies/");
				  HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				  BufferedReader read = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				  String line = read.readLine();
				  
				  while(line!=null) {
				    html += line;
				    line = read.readLine();
				  }
				  if(!html.equals(""))
					  content.add(html);
		//		  System.out.println(content);
				  
				} catch(MalformedURLException ex) {
				        ex.printStackTrace();
				} catch(IOException ioex) {
				        ioex.printStackTrace();
				}
		
			break;
		case "measurementgoal":
			content.add("Fase5");
			content.add("READ");
			try {
				  String html = "";
				  URL url = new URL("http://localhost:8082/getMeasurementGoals/");
				  HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				  BufferedReader read = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				  String line = read.readLine();
				  
				  while(line!=null) {
				    html += line;
				    line = read.readLine();
				  }
				  if(!html.equals(""))
					  content.add(html);
		//		  System.out.println(content);
				  
				} catch(MalformedURLException ex) {
				        ex.printStackTrace();
				} catch(IOException ioex) {
				        ioex.printStackTrace();
				}
		
			break;
		case "question":
			content.add("Fase5");
			content.add("READ");
			try {
				  String html = "";
				  URL url = new URL("http://localhost:8082/getQuestions/");
				  HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				  BufferedReader read = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				  String line = read.readLine();
				  
				  while(line!=null) {
				    html += line;
				    line = read.readLine();
				  }
				  if(!html.equals(""))
					  content.add(html);
				  
		//		  System.out.println(content);
				  
				} catch(MalformedURLException ex) {
				        ex.printStackTrace();
				} catch(IOException ioex) {
				        ioex.printStackTrace();
				}
		
			break;
		case "metric":
			content.add("Fase5");
			content.add("READ");
			//content.add(ontologyBusService.getOntologies().toString());
			//String json = EntityUtils.toString(response.getEntity());
			//System.out.println("x:"+ontologyBusService.getOntologies().getBody().getMessage());
			
			try {
				  String html = "";
				  URL url = new URL("http://localhost:8082/getOntologies/");
				  HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				  BufferedReader read = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				  String line = read.readLine();
				  
				  while(line!=null) {
				    html += line;
				    line = read.readLine();
				  }
				  if(!html.equals(""))
					  content.add(html);
				  
				//  System.out.println(content);
				  
				} catch(MalformedURLException ex) {
				        ex.printStackTrace();
				} catch(IOException ioex) {
				        ioex.printStackTrace();
				}
				
			break;
		case "retrospectivereport":
			content.add("Fase5");
			content.add("READ");
			try {
				  String html = "";
				  URL url = new URL("http://localhost:8082/getRetrospectiveReports/");
				  HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				  BufferedReader read = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				  String line = read.readLine();
				  
				  while(line!=null) {
				    html += line;
				    line = read.readLine();
				  }
				  if(!html.equals(""))
					  content.add(html);
		//		  System.out.println(content);
				  
				} catch(MalformedURLException ex) {
				        ex.printStackTrace();
				} catch(IOException ioex) {
				        ioex.printStackTrace();
				}
				break;
			default: 
				DTOresponse dtoresponse = new DTOresponse();
				ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(
						dtoresponse, HttpStatus.BAD_REQUEST);

				return response;
				
				}
		Bus bus = new Bus(tag, content, typeObject, originAddress,
				resolveAddress, id);

		busRepository.save(bus);
//		System.out.println("BUS: "+bus);
//		System.out.println("BUS.ToString: "+bus.toString());
//		System.out.println(bus.getContent());
//		System.out.println(bus.getTypeObject());
		DTOresponse dtoresponse = new DTOresponse();
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(
				dtoresponse, HttpStatus.OK);
//		ontologyRepository.findAll();
//		JSON js = new JSON();
//		js.parse(ontologyRepository.findAll().toString());
//		System.out.println("JS: "+ js);
		return response;
	}

	// Questa chiamata permette la ricerca della risposta con tutti gli elementi
	// all'interno del contento con quel typeobject
	// filtrata attraverso il typeobject.
	@Override
	public ResponseEntity<DTOresponse> receiveBusMessage(String typeObject) {
		// TODO Auto-generated method stub
		List<Bus> list=busRepository.findAll();
		
		DTOresponse dtoresponse = new DTOresponse();
		for(Bus b : list){
			if(b.getTypeObject().equals(typeObject)){
				dtoresponse.setBusResponse(b);
				break;
			}
		}
		
		//System.out.println("busMessage: "+ busMessage.getContent());
		
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(
				dtoresponse, HttpStatus.OK);

		return response;
	}

	// è una cazzo di GET per prelevare dal BUS di merda l'oggetto che abbia tag
	// e typeobjcet scelto da noi.

	@Override
	public ResponseEntity<DTOresponse> queryTagAndTypeMessage(String tag,
			String typeObject) {
		// TODO Auto-generated method stub
		List<Bus> busMessage = busRepository.findAll();
		Bus busResponse = null;
		for (int i = 0; i < busMessage.size(); i++) {
			if (busMessage.get(i).getTag().equals(tag)
					&& busMessage.get(i).getTypeObject().equals(typeObject))
				busResponse = busMessage.get(i);
		}
		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setBusResponse(busResponse);
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(
				dtoresponse, HttpStatus.OK);

		return response;
	}

	@Override
	public ResponseEntity<DTOresponse> queryTagMessage(String tag) {
		Bus busMessage = busRepository.findOne(tag);

		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setBusResponse(busMessage);
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(
				dtoresponse, HttpStatus.OK);

		return response;
	}
	@Override
	public ResponseEntity<DTOresponse> deleteAllBusMessage() {
		busRepository.delete(busRepository.findAll());

		return null;
	}
//	
//	content:
//		fase
//		"read"
//		objIdLocalToPhase
//		typeObj
//		instance//nome oggetto anche ""
//		busVersion //""
//		tags[]//query
//		
//	String bus address
//	String msg//msg di ritorno
//	try{
//		BusMessage busms = new BusMessage(BusMessage.OPERATION_READ,fase,content.toString())
//		msg= busms.send(bus address);
//	}catch(BusException e){
//		e.printStackTrace("error from bus");
//	}
//	
//	
//	
	@Override
	public ResponseEntity<DTOresponse> receiveRealBusMessage(String typeObject) throws JSONException {
		// TODO Auto-generated method stub
		//List<String> content = new ArrayList<String>();

        System.out.println("Typeobject = " + typeObject);

		JSONObject jsonRead = new JSONObject();

		String msg= null;//msg di ritorno
		
		/* il formato da adottare per le nostre entità nel db hanno formato
		 * base64-Fase5-typeObject													*/
		
		
		jsonRead.put("objIdLocalToPhase", "");
		jsonRead.put("typeObj", "base64-"+typeObject);
		//jsonRead.put("typeObj","basa64-outcomes5");
		jsonRead.put("instance", "");
		jsonRead.put("busVersion", "");
		jsonRead.put("tags", "[]");
		try {					
			
				BusMessage busMs = new BusMessage(BusMessage.OPERATION_READ,"phase5",jsonRead.toString());
				msg= busMs.send(busAddress);
				System.out.println("msg "+typeObject+":"+msg);
			
			  
		} catch(MalformedURLException ex) {
			        ex.printStackTrace();
			} catch(IOException ioex) {
			        ioex.printStackTrace();
			}catch(BusException e){
				e.printStackTrace();
			}
		


		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setMessage(msg);
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse, HttpStatus.OK);

		
		return response;
	}

	@Override
	public ResponseEntity<DTOresponse> createBusEntity(String objIdLocalToPhase,
			String typeObject, String payload) throws JSONException {
		String read=null;
		String jsonInString= null;
		JSONObject jsonRead = new JSONObject();
		//System.out.println("parsedData(payload): "+parsedData(payload,"Project1").toString());
		//JSONArray array=new JSONArray(parsedData(payload));
		//JSONArray test = new JSONArray();
		try {

			if(typeObject.toLowerCase().equals("schedulechart")){
				List<ScheduleChart> listOfEntity = scheduleChartRepository.findAll();
				
				
				ObjectMapper mapper = new ObjectMapper();				
				jsonInString = mapper.writeValueAsString(listOfEntity);
			}
			if(typeObject.toLowerCase().equals("retrospectivereport")){
				List<RetrospectiveReport> listOfEntity = retrospectiveReportRepository.findAll();
				
				ObjectMapper mapper = new ObjectMapper();				
				jsonInString = mapper.writeValueAsString(listOfEntity);
			}
			if(typeObject.toLowerCase().equals("sendingoutcomesfase6")){
				List<SendingOutComesFase6> listOfEntity =sendingOutComesFase6Repository.findAll();
				
				ObjectMapper mapper = new ObjectMapper();
				jsonInString = "[";
				for(int i=0;i<listOfEntity.size();i++){
					jsonInString += mapper.writeValueAsString(listOfEntity.get(i));
					if(i < listOfEntity.size()-1)
						jsonInString+=",";
				}
				jsonInString+= "]";
				System.out.println("jsonInString compute:"+jsonInString);
				
			}
			if(typeObject.toLowerCase().equals("validateddata")){
				List<ValidateData> listOfEntity = validateDataRepository.findAll();
				
				ObjectMapper mapper = new ObjectMapper();				
				jsonInString = mapper.writeValueAsString(listOfEntity);
			}
			if(typeObject.toLowerCase().equals("projectId")){
				List<InstanceProject> listOfEntity = projectRepository.findAll();
				
				ObjectMapper mapper = new ObjectMapper();				
				jsonInString = mapper.writeValueAsString(listOfEntity);
			}
			
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("jsoninstring"+jsonInString);

	
		
		
		jsonRead.put("objIdLocalToPhase", typeObject+"Test");
		jsonRead.put("typeObj", "base64-Fase5-"+typeObject);
		jsonRead.put("instance", typeObject+"Test");
		
		jsonRead.put("busVersion", "");
		jsonRead.put("tags", "[]");
		jsonRead.put("payload", jsonInString.toString());
		

		//String msg= null;//msg di ritorno
		try {										
				
			  BusMessage readBus = new BusMessage(BusMessage.OPERATION_CREATE,"phase5",jsonRead.toString());
			  read = readBus.send(busAddress);
			   
		} catch(MalformedURLException ex) {
			        ex.printStackTrace();
			} catch(IOException ioex) {
			        ioex.printStackTrace();
			}catch(BusException e){
				e.printStackTrace();
			}
		if(read.contains("ERR4")){
			try {										
			
			  BusMessage readBus = new BusMessage(BusMessage.OPERATION_UPDATE,"phase5",jsonRead.toString());
			  read = readBus.send(busAddress);
			   
			} catch(MalformedURLException ex) {
				        ex.printStackTrace();
				} catch(IOException ioex) {
				        ioex.printStackTrace();
				}catch(BusException e){
					e.printStackTrace();
				}
		}
		
		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setMessage(read);
		System.out.println("DTO RESPONSE: "+dtoresponse.getMessage());
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(
				dtoresponse, HttpStatus.OK);
		return response;
	}
	

	
	//Start Here
	@Override
	public ResponseEntity<DTOresponse> createSendingBus(String payload) throws JSONException {
		
		String objIdLocalToPhase = "OutComes5";
		
		String read=null;
		JSONObject jsonRead = new JSONObject();
		//JSONObject test = new JSONObject();
	//	test.put("object", payload.toString())
		
		jsonRead.put("objIdLocalToPhase", objIdLocalToPhase);
		jsonRead.put("typeObj", "base64-outcomes5"); // "Outcomes"
		jsonRead.put("instance", "outcomes5");  
		
		jsonRead.put("busVersion", "");
		jsonRead.put("tags", "[]");
		jsonRead.put("payload",payload );
		System.out.println("array"+payload.toString() );
		

		try {										
			
			  BusMessage readBus = new BusMessage(BusMessage.OPERATION_CREATE,"phase5",jsonRead.toString());
			   read = readBus.send(busAddress);
			 System.out.println("Create Message: " + read);
			
		} catch(MalformedURLException ex) {
			        ex.printStackTrace();
			} catch(IOException ioex) {
			        ioex.printStackTrace();
			}catch(BusException e){
				e.printStackTrace();
			}
		if(read.contains("ERR4")){
			try {										
				
				  BusMessage updateBus = new BusMessage(BusMessage.OPERATION_UPDATE,"phase5",jsonRead.toString());
				   read = updateBus.send(busAddress);
				 System.out.println("UPDATE MESSAGE " + read);
				
			} catch(MalformedURLException ex) {
				        ex.printStackTrace();
				} catch(IOException ioex) {
				        ioex.printStackTrace();
				}catch(BusException e){
					e.printStackTrace();
				}
		}
		
		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setMessage(read);
		// System.out.println("CREATE Metric5 msg: "+read);
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse, HttpStatus.OK);
		return response;
		
		
	}
	
	//finish here
	//Start Here
		@Override
		public ResponseEntity<DTOresponse> sendDataAdjustementToBus(String payload) throws JSONException {
			
			String objIdLocalToPhase = "DataAdjustement";
			
			String read=null;
			JSONObject jsonRead = new JSONObject();
	
			
			jsonRead.put("objIdLocalToPhase", objIdLocalToPhase);
			jsonRead.put("typeObj", "base64-Fase5-DataAdjustement"); // "Outcomes"
			jsonRead.put("instance", "dataAdjustment");  
			
			jsonRead.put("busVersion", "");
			jsonRead.put("tags", "[]");
			jsonRead.put("payload",payload );
			System.out.println("array"+payload );
			

			try {										
				
				  BusMessage readBus = new BusMessage(BusMessage.OPERATION_CREATE,"phase3",jsonRead.toString());
				   read = readBus.send(busAddress);
				 System.out.println("Create Message: " + read);
				
			} catch(MalformedURLException ex) {
				        ex.printStackTrace();
				} catch(IOException ioex) {
				        ioex.printStackTrace();
				}catch(BusException e){
					e.printStackTrace();
				}
			if(read.contains("ERR4")){
				try {										
					
					  BusMessage updateBus = new BusMessage(BusMessage.OPERATION_UPDATE,"phase5",jsonRead.toString());
					   read = updateBus.send(busAddress);
					 System.out.println("UPDATE MESSAGE " + read);
					
				} catch(MalformedURLException ex) {
					        ex.printStackTrace();
					} catch(IOException ioex) {
					        ioex.printStackTrace();
					}catch(BusException e){
						e.printStackTrace();
					}
			}
			
			DTOresponse dtoresponse = new DTOresponse();
			dtoresponse.setMessage(read);
			ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse, HttpStatus.OK);
			return response;
			
			
		}
	
}
