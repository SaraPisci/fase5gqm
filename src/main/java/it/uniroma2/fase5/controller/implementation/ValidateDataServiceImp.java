package it.uniroma2.fase5.controller.implementation;

import it.uniroma2.fase5.controller.ValidateDataService;
import it.uniroma2.fase5.model.ValidateData;
import it.uniroma2.fase5.model.classesFromBus.metric.Ontology;
import it.uniroma2.fase5.model.classesFromBus.repository.OntologyBusRepository;
import it.uniroma2.fase5.model.rest.DTOresponse;
import it.uniroma2.fase5.repositories.ValidateDataRepository;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import it.uniroma2.isssr.integrazione.BusException;
import it.uniroma2.isssr.integrazione.BusMessage;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("ValidateDataService")
public class ValidateDataServiceImp implements ValidateDataService {
	
	@Autowired
	ValidateDataRepository validateDataRepository;
	@Autowired
	OntologyBusRepository ontologyRepository;

	//private String busAdd = "http://ermes.sweng.uniroma2.it/Bus/inboundChannel.html";
	@Value("${bus.address}")
	private String busAdd;
	
	
	@Override
	public ResponseEntity<DTOresponse> createValidateData(String dataId,
			List<String> data,String workFlowInstanceId ,
			String metricRef) throws JSONException {
		

			Ontology temp2= new Ontology();
			if(ontologyRepository.findOne(metricRef)!=null)
				temp2=(ontologyRepository.findOne(metricRef));
		
			if (temp2==null){
				return new ResponseEntity<DTOresponse>(new DTOresponse(),HttpStatus.BAD_REQUEST);
			}
		
			ValidateData dataSet= new ValidateData(dataId, data ,workFlowInstanceId,temp2);
		
			validateDataRepository.save(dataSet);

			JSONObject jsonRead = new JSONObject();

		String busAddress= this.busAdd;
		String msg= null;//msg di ritorno
		System.out.println("CONTROLLO"+dataSet.getData());

		String payload = "{\"dataId\":\"" + dataSet.getDataId() + "\","+ "\"data\":" + dataSet.getData()+ ","+ "\"businessWorkflowInstanceId\":\"" + dataSet.getBusinessWorkflowInstanceId() + "\"," + "\","+ "\"metricRef\":\"" + dataSet.getOntologyRef()+ "\"}";
		/* il formato da adottare per le nostre enità nel db hanno formato
		 * base64-typeObject													*/

		jsonRead.put("objIdLocalToPhase", "5");
		jsonRead.put("typeObj", "base64-validatedData");
		jsonRead.put("instance", "validatedData"+dataSet.getDataId());
		jsonRead.put("busVersion", "1");
		jsonRead.put("tags", "[]");
		jsonRead.put("payload" , payload);

		try {
			System.out.println("\n\n\n-------------->"+jsonRead.toString()+"<-----------------------\n\n\n");
			BusMessage busMs = new BusMessage(BusMessage.OPERATION_CREATE,"phase5",jsonRead.toString());
			msg= busMs.send(busAddress);
			System.out.println("MSG VALIDATEDDATA: "+msg);


		} catch(MalformedURLException ex) {
			ex.printStackTrace();
		} catch(IOException ioex) {
			ioex.printStackTrace();
		}catch(BusException e){
			e.printStackTrace();
		}

		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setMessage(msg);
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse,HttpStatus.OK);
			
		return response;
	}

	@Override
	public ResponseEntity<DTOresponse> getValidateData() {
		List<ValidateData> datas = validateDataRepository.findAll();

		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setValidateData(datas);

		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(
				dtoresponse, HttpStatus.OK);

		return response;
	}

	public List<String> changeData(List<String> data) {
		int i ;
		List<String> data2 = new ArrayList<>();
		for(i=0; i<data.size(); i++) {
			data2.add("\"" +data.get(i) + "\"");
		}
		return data2;
	}

}
