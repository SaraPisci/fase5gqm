package it.uniroma2.fase5.controller.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import it.uniroma2.fase5.controller.IssueMessageResourceService;
import it.uniroma2.fase5.model.IssueMessageResource;
import it.uniroma2.fase5.model.rest.DTOresponse;
import it.uniroma2.fase5.repositories.IssueMessageResourceRepository;

@Service("IssueMessageResourceService")
public class IssueMessageResourceServiceImpl implements IssueMessageResourceService {

	@Autowired
	IssueMessageResourceRepository issueMessageResourceRepository;
	
	
	@Override
	public ResponseEntity<DTOresponse> createIssueMessageResource(String name, String url, String description) {
		
		IssueMessageResource issueMessageResource = new IssueMessageResource(name, url, description);
		issueMessageResourceRepository.save(issueMessageResource);
				
		DTOresponse dtoresponse = new DTOresponse();		
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse,HttpStatus.OK);
			
		return response;
	}
	
	@Override
	public ResponseEntity<DTOresponse> deleteIssueMessageResource(String name) {
	
		
		issueMessageResourceRepository.delete(issueMessageResourceRepository.findOne(name));
		
		return null;
	}
	
	@Override
	public ResponseEntity<DTOresponse> getIssueMessageResource() {
		
		List<IssueMessageResource> issueMessageResource = issueMessageResourceRepository.findAll();
		
		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setIssueMessageResource(issueMessageResource);
		
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse,HttpStatus.OK);	
	
		return response;		
	}
	
	public static void main(String[] args) throws NullPointerException {
	}

}
