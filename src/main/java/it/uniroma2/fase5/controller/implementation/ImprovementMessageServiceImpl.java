package it.uniroma2.fase5.controller.implementation;

import it.uniroma2.fase5.controller.ImprovementMessageService;
import it.uniroma2.fase5.model.ImprovementMessage;
import it.uniroma2.fase5.model.classesFromBus.external.OrganizationalGoalBus;
import it.uniroma2.fase5.model.classesFromBus.metric.Ontology;
import it.uniroma2.fase5.model.classesFromBus.repository.OntologyBusRepository;
import it.uniroma2.fase5.model.classesFromBus.repository.OrganizationalGoalRepository;
import it.uniroma2.fase5.model.rest.DTOresponse;
import it.uniroma2.fase5.repositories.ImprovementMessageRepository;
import it.uniroma2.isssr.integrazione.BusException;
import it.uniroma2.isssr.integrazione.BusMessage;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;


//private String busAdd = "http://ermes.sweng.uniroma2.it/Bus/inboundChannel.html";







@Service("ImprovementMessageService")
public class ImprovementMessageServiceImpl implements ImprovementMessageService {

    @Autowired
    private ImprovementMessageRepository improvementMessageRepository;
    @Autowired
    private OrganizationalGoalRepository goalRepository;
    @Autowired
    private OntologyBusRepository ontologyBusRepository;
    @Value("${bus.address}")
    private String busAdd;

    @Override
    public ResponseEntity<DTOresponse> createImprovementMessage(String improvementMessageId, String message, String goalId, boolean active) throws JSONException {

        OrganizationalGoalBus goal = goalRepository.findOne(goalId);
        ImprovementMessage improvementMessage= new ImprovementMessage(message,goal,active);
        improvementMessageRepository.save(improvementMessage);

        //Utile serializzare?
        JSONObject jsonRead = new JSONObject();
        String busAddress= this.busAdd;
        String msg= null;//msg di ritorno
        //\"improvementMessageId\":\"" + improvementMessage.getImprovementMessageId() + "
        String payload = "{\","+ "\"message\":\"" + improvementMessage.getMessage()+ "\","+ "\"goal\":\"" + improvementMessage.getGoal() + "\","+ "}";

        /* il formato da adottare per le nostre enità nel db hanno formato
         * base64-Fase5-typeObject													*/

        jsonRead.put("objIdLocalToPhase", "5");
        jsonRead.put("typeObj", "base64-businessWorkflowIssueMessage");
        jsonRead.put("instance", "InstanceImprovementMessage"+ improvementMessage.getImprovementMessageId());
        jsonRead.put("busVersion", "1");
        jsonRead.put("tags", "[]");
        jsonRead.put("payload" , payload);

        try {
            System.out.println("\n\n\n-------------->"+jsonRead.toString()+"<-----------------------\n\n\n");
            BusMessage busMs = new BusMessage(BusMessage.OPERATION_CREATE,"phase5",jsonRead.toString());
            msg= busMs.send(busAddress);
            System.out.println("MSG BusinessWorkflow: "+msg);


        } catch(MalformedURLException ex) {
            ex.printStackTrace();
        } catch(IOException ioex) {
            ioex.printStackTrace();
        }catch(BusException e){
            e.printStackTrace();
        }

        DTOresponse dtoresponse = new DTOresponse();
        ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(
                dtoresponse, HttpStatus.OK);

        return response;
    }

    @Override
    public ResponseEntity<DTOresponse> getImprovementMessage() {
        List<ImprovementMessage> improvementMessageList = improvementMessageRepository.findAll();
        ArrayList<ImprovementMessage> improvementMessageListFinal=new ArrayList<ImprovementMessage>();
        DTOresponse dtoresponse = new DTOresponse();
        for(int x=0;x<improvementMessageList.size();x++){
            if(improvementMessageList.get(x).isActive()){
                improvementMessageListFinal.add(improvementMessageList.get(x));
            }
        }
        dtoresponse.setImprovementMessageList(improvementMessageListFinal);
        ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse,HttpStatus.OK);

        return response;
    }


}
