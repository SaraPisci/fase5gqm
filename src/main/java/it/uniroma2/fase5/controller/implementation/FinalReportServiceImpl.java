package it.uniroma2.fase5.controller.implementation;

import it.uniroma2.fase5.controller.FinalReportService;
import it.uniroma2.fase5.model.FinalReport;
import it.uniroma2.fase5.model.classesFromBus.external.InstanceProject;
import it.uniroma2.fase5.model.classesFromBus.repository.InstanceProjectRepository;
import it.uniroma2.fase5.model.rest.DTOresponse;
import it.uniroma2.fase5.repositories.FinalReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("FinalReportService")
public class FinalReportServiceImpl implements FinalReportService {

    @Autowired
    FinalReportRepository finalReportRepository;

    @Autowired
    InstanceProjectRepository projectRepository;

    @Override
    public ResponseEntity<DTOresponse> createFinalReport(String finalReportId, String dataModified, String newProposals, String projectRef) {
        InstanceProject pr = null;
        pr = projectRepository.findOne(projectRef);
        if (pr == null) {
            return new ResponseEntity<DTOresponse>(new DTOresponse(), HttpStatus.BAD_REQUEST);
        }
        FinalReport finalReport = new FinalReport(finalReportId, dataModified, newProposals, pr, true);
        finalReportRepository.save(finalReport);
        DTOresponse dtoResponse = new DTOresponse();
        ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoResponse,HttpStatus.OK);
        return response;
    }

    @Override
    public ResponseEntity<DTOresponse> findFinalReport() {
        System.out.println("IN FINDFINALREPORT");
        List<FinalReport> finalReports =  finalReportRepository.findAll();
        if (finalReports.size() != 0) {
            FinalReport finalReport = finalReports.get(0);
            DTOresponse dtoResponse = new DTOresponse();
            dtoResponse.setFinalReport(finalReport);
            ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoResponse, HttpStatus.OK);
            return response;
        }else{
            DTOresponse dtoResponse = new DTOresponse();
            System.out.println("ERR non ci sono finalReport");
            ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoResponse, HttpStatus.OK);
            return response;
        }
    }

/*    @Override
    public ResponseEntity<DTOresponse> getFinalReport() {
        return null;
    }

    @Override
    public ResponseEntity<DTOresponse> updateDataModified(String id, String dataModified, String newProposals, boolean active) {
        List<FinalReport> finalReports = finalReportRepository.findRecentActive();

        System.out.println(" stato fr " + active);
        if (finalReports.size() == 0){
            System.out.println("final report null in updatedatamodified");
        }
        FinalReport finalReportWithMaxID = finalReports.get(0);
        for (int i = 0; i < finalReports.size(); i++){
            if (Integer.parseInt(finalReports.get(i).getId()) > Integer.parseInt(finalReportWithMaxID.getId())){
                finalReportWithMaxID = finalReports.get(i);
            }
        }

        String newDataModified;
        String newNewProposals;

        if (dataModified == null){
            newDataModified = finalReportWithMaxID.getDataModified();
        }
        else{
            if (finalReportWithMaxID.getDataModified() == ""){
                newDataModified = dataModified;
            }else {
                newDataModified = finalReportWithMaxID.getDataModified() + "<br>" + dataModified;
            }
        }

        if (newProposals == null){
            newNewProposals = finalReportWithMaxID.getNewProposals();
        }else {
            newNewProposals = newProposals;
        }


        FinalReport finalReportSave = new FinalReport(id, newDataModified, newNewProposals, finalReportWithMaxID.getProjectRef(), active);
        finalReportRepository.save(finalReportSave);
        DTOresponse dtoResponse = new DTOresponse();
        ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoResponse,HttpStatus.OK);
        return response;
    }

    @Override
    public ResponseEntity<DTOresponse> deleteFinalReport(String finalReportId) {
        return null;
    }

    @Override
    public ResponseEntity<DTOresponse> deleteAllFinalReport() {
        return null;
    }

    @Override
    public ResponseEntity<DTOresponse> findFinalReportById(String id) {
        FinalReport finalReport = finalReportRepository.findOne("1");
        DTOresponse dtoResponse = new DTOresponse();
        dtoResponse.setFinalReport(finalReport);
        ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoResponse, HttpStatus.OK);
        return response;
    }

    @Override
    public ResponseEntity<DTOresponse> findRecentActiveFinalReport() {
        List<FinalReport> finalReports = finalReportRepository.findRecentActive();
        if (finalReports.size() == 0){
            DTOresponse dtoResponse = new DTOresponse();
            dtoResponse.setFinalReport(null);
            ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoResponse, HttpStatus.OK);
            return response;
        }
        FinalReport finalReportWithMaxID = finalReports.get(0);
        for (int i = 0; i < finalReports.size(); i++){
            if (Integer.parseInt(finalReports.get(i).getId()) > Integer.parseInt(finalReportWithMaxID.getId())){
                finalReportWithMaxID = finalReports.get(i);
            }
        }
        DTOresponse dtoResponse = new DTOresponse();
        dtoResponse.setFinalReport(finalReportWithMaxID);
        ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoResponse, HttpStatus.OK);
        return response;
    }

    @Override
    public ResponseEntity<DTOresponse> findFinalReportWithMaxId() {
        System.out.println("IN findFinalReportWithMaxId");

        List<FinalReport> finalReports = finalReportRepository.findAll();
        System.out.println("finalReports = " + finalReports);
        if (finalReports.size() == 0){
            FinalReport firstFinalReport = new FinalReport(String.valueOf(0), "", "", new Project(), true);
            DTOresponse dtoResponse = new DTOresponse();
            dtoResponse.setFinalReport(firstFinalReport);
            ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoResponse, HttpStatus.OK);
            return response;
        }
        FinalReport maxFinalReport = finalReports.get(0);
        for (int i = 0; i < finalReports.size(); i++){
            if (Integer.parseInt(finalReports.get(i).getId()) > Integer.parseInt(maxFinalReport.getId())){
                maxFinalReport = finalReports.get(i);
            }
        }
        System.out.println("Id trovato" + Integer.parseInt(maxFinalReport.getId()));
        maxFinalReport.setId(String.valueOf(Integer.parseInt(maxFinalReport.getId())+1));
        DTOresponse dtoResponse = new DTOresponse();
        dtoResponse.setFinalReport(maxFinalReport);
        ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoResponse, HttpStatus.OK);
        return response;
    }
*/
}
