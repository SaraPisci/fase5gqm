package it.uniroma2.fase5.controller.implementation;

import it.uniroma2.fase5.controller.BusinessWorkflowIssueMessageService;
import it.uniroma2.fase5.model.BusinessWorkflowIssueMessage;
import it.uniroma2.fase5.model.IssueMessageResource;
import it.uniroma2.fase5.model.rest.DTOresponse;
import it.uniroma2.fase5.repositories.BusinessWorkflowIssueMessageRepository;
import it.uniroma2.fase5.repositories.IssueMessageResourceRepository;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import it.uniroma2.isssr.integrazione.BusException;
import it.uniroma2.isssr.integrazione.BusMessage;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("BusinessWorkflowIssueMessageService")
public class BusinessWorkflowIssueMessageServiceImpl implements BusinessWorkflowIssueMessageService {

	@Autowired
	BusinessWorkflowIssueMessageRepository businessWorkflowIssueMessageRepository;
	
	@Autowired
	IssueMessageResourceRepository issueMessageResourceRepository;

	//private String busAdd = "http://ermes.sweng.uniroma2.it/Bus/inboundChannel.html";
	@Value("${bus.address}")
	private String busAdd;
	
	@Override
	public ResponseEntity<DTOresponse> createBusinessWorkflowIssueMessage(String businessWorkflowInstanceId, String issueMessage, List<String> issueMessageResources) throws JSONException {
	
		List<IssueMessageResource> temp= new ArrayList<IssueMessageResource>();
		for(String s : issueMessageResources ){
			if(issueMessageResourceRepository.findOne(s)!=null)
				temp.add(issueMessageResourceRepository.findOne(s));	
		}
		
		if (temp.size()==0){
			return new ResponseEntity<DTOresponse>(new DTOresponse(),HttpStatus.BAD_REQUEST);
		}

		BusinessWorkflowIssueMessage businessWorkflowIssueMessage= new BusinessWorkflowIssueMessage(businessWorkflowInstanceId, issueMessage, temp);
		businessWorkflowIssueMessageRepository.save(businessWorkflowIssueMessage);


		JSONObject jsonRead = new JSONObject();

		String busAddress= this.busAdd;
		String msg= null;//msg di ritorno
		String payload = "{\"businessWorkflowInstanceId\":\"" + businessWorkflowIssueMessage.getBusinessWorkflowInstanceId() + "\","+ "\"issueMessage\":\"" + businessWorkflowIssueMessage.getIssueMessage()+ "\","+ "\"issueMessageResources\":" + businessWorkflowIssueMessage.getIssueMessageResources()+"}";
		/* il formato da adottare per le nostre enità nel db hanno formato
		 * base64-Fase5-typeObject													*/

		jsonRead.put("objIdLocalToPhase", "5");
		jsonRead.put("typeObj", "base64-businessWorkflowIssueMessage");
		jsonRead.put("instance", "metric"+ businessWorkflowIssueMessage.getBusinessWorkflowInstanceId());
		jsonRead.put("busVersion", "1");
		jsonRead.put("tags", "[]");
		jsonRead.put("payload" , payload);

		try {
			System.out.println("\n\n\n-------------->"+jsonRead.toString()+"<-----------------------\n\n\n");
			BusMessage busMs = new BusMessage(BusMessage.OPERATION_CREATE,"phase3",jsonRead.toString());
			msg= busMs.send(busAddress);
			System.out.println("MSG BusinessWorkflow: "+msg);


		} catch(MalformedURLException ex) {
			ex.printStackTrace();
		} catch(IOException ioex) {
			ioex.printStackTrace();
		}catch(BusException e){
			e.printStackTrace();
		}

		DTOresponse dtoresponse = new DTOresponse();
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(
				dtoresponse, HttpStatus.OK);

		return response;
	}
	
	@Override
	public ResponseEntity<DTOresponse> createBusinessWorkflowIssueMessage(String businessWorkflowInstanceId, String issueMessage) throws JSONException {
	
		BusinessWorkflowIssueMessage businessWorkflowIssueMessage= new BusinessWorkflowIssueMessage(businessWorkflowInstanceId, issueMessage);
		businessWorkflowIssueMessageRepository.save(businessWorkflowIssueMessage);


		JSONObject jsonRead = new JSONObject();

		String busAddress= this.busAdd;
		String msg= null;//msg di ritorno
		String payload = "{\"businessWorkflowInstanceId\":\"" + businessWorkflowIssueMessage.getBusinessWorkflowInstanceId() + "\","+ "\"issueMessage\":\"" + businessWorkflowIssueMessage.getIssueMessage()+"}";
		/* il formato da adottare per le nostre enità nel db hanno formato
		 * base64-Fase5-typeObject													*/

		jsonRead.put("objIdLocalToPhase", "5");
		jsonRead.put("typeObj", "base64-businessWorkflowIssueMessage");
		jsonRead.put("instance", "metric"+ businessWorkflowIssueMessage.getBusinessWorkflowInstanceId());
		jsonRead.put("busVersion", "1");
		jsonRead.put("tags", "[]");
		jsonRead.put("payload" , payload);

		try {
			System.out.println("\n\n\n-------------->"+jsonRead.toString()+"<-----------------------\n\n\n");
			BusMessage busMs = new BusMessage(BusMessage.OPERATION_CREATE,"phase3",jsonRead.toString());
			msg= busMs.send(busAddress);
			System.out.println("MSG BusinessWorkflow: "+msg);


		} catch(MalformedURLException ex) {
			ex.printStackTrace();
		} catch(IOException ioex) {
			ioex.printStackTrace();
		}catch(BusException e){
			e.printStackTrace();
		}

		DTOresponse dtoresponse = new DTOresponse();
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(
				dtoresponse, HttpStatus.OK);

		return response;
	}


	@Override
	public ResponseEntity<DTOresponse> getBusinessWorkflowIssueMessage() {
		List<BusinessWorkflowIssueMessage> businessWorkflowIssueMessage = businessWorkflowIssueMessageRepository.findAll();
		
		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setBusinessWorkflowIssueMessage(businessWorkflowIssueMessage);
		
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse,HttpStatus.OK);	
	
		return response;
	}

	@Override
	public ResponseEntity<DTOresponse> deleteBusinessWorkflowIssueMessage(String businessWorkflowInstanceId) {
		businessWorkflowIssueMessageRepository.delete(businessWorkflowIssueMessageRepository.findOne(businessWorkflowInstanceId));
		
		return null;
	}

}
