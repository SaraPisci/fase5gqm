package it.uniroma2.fase5.controller.implementation;

import it.uniroma2.fase5.controller.ReportFase6Service;
import it.uniroma2.fase5.model.ImprovementMessage;
import it.uniroma2.fase5.model.ReportFase6;
import it.uniroma2.fase5.model.SendingOutComesFase6;
import it.uniroma2.fase5.model.rest.DTOresponse;
import it.uniroma2.fase5.repositories.ImprovementMessageRepository;
import it.uniroma2.fase5.repositories.ReportFase6Repository;
import it.uniroma2.fase5.repositories.SendingOutComesFase6Repository;
import it.uniroma2.isssr.integrazione.BusException;
import it.uniroma2.isssr.integrazione.BusMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

@Service("ReportFase6Service")
public class ReportFase6ServiceImpl implements ReportFase6Service {


    //private String busAdd = "http://ermes.sweng.uniroma2.it/Bus/inboundChannel.html";
    @Value("${bus.address}")
    private String busAdd;

    @Autowired
    ReportFase6Repository reportFase6repository;
    @Autowired
    ImprovementMessageRepository improvementMessageRepository;

    @Override
    public ResponseEntity<DTOresponse> createReportFase6(String fileId,
                                                                 String json) throws JSONException {
        System.out.println("JSON-------------->" + json.toString());
        JSONObject jo = new JSONObject(json);
        JSONArray improvementList= jo.getJSONArray("improvementList");
        for (int i=0;i<improvementList.length();i++){
            JSONObject obj = improvementList.getJSONObject(i);
            String improvementId = obj.getString("improvementMessageId");
            ImprovementMessage improvementMessage=improvementMessageRepository.findOne(improvementId);
            ImprovementMessage improvementMessagesave=new ImprovementMessage(improvementMessage.getImprovementMessageId(),improvementMessage.getMessage(),improvementMessage.getGoal(),false);
            improvementMessageRepository.save(improvementMessagesave);
        }
       // ArrayList<ImprovementMessage> improvementMessageList=jo.get("improvementList");

                // TODO Auto-generated method stub
                System.out.println("ID: "+fileId);
                ReportFase6 sending = new ReportFase6(fileId, json);
                reportFase6repository.save(sending);
                JSONObject jsonRead = new JSONObject();
                String busAddress = this.busAdd;
                //10.220.32.198
                String msg = null;//msg di ritorno
                String payload = json;
                /* il formato da adottare per le nostre enità nel db hanno formato
                 * base64-Fase5-typeObject													*/
                System.out.println("\n\n\n SENDING QUESTIONS-------------> " + json + "<------------------\n\n\n");
                jsonRead.put("objIdLocalToPhase", "6");
                jsonRead.put("typeObj", "base64-Report");
                jsonRead.put("instance", "reportInstance " + Math.round(100 * Math.random()));
                jsonRead.put("busVersion", "1");
                jsonRead.put("tags", "[]");
                jsonRead.put("payload", payload);

                try {
                    System.out.println("\n\n\n-------------->" + jsonRead.toString() + "<-----------------------\n\n\n");
                    BusMessage busMs = new BusMessage(BusMessage.OPERATION_CREATE, "phase6", jsonRead.toString());
                    msg = busMs.send(busAddress);
                    System.out.println("MSG QUESTION: " + msg);


                } catch (MalformedURLException ex) {
                    ex.printStackTrace();
                } catch (IOException ioex) {
                    ioex.printStackTrace();
                } catch (BusException e) {
                    e.printStackTrace();
                }
        DTOresponse dtoresponse = new DTOresponse();
        ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse, HttpStatus.OK);

        return response;
    }



    @Override
    public ResponseEntity<DTOresponse> getReportFase6() {
        return null;
    }
}
