package it.uniroma2.fase5.controller.implementation;

import it.uniroma2.fase5.controller.ScheduleChartService;
import it.uniroma2.fase5.model.ScheduleChart;
import it.uniroma2.fase5.model.rest.DTOresponse;
import it.uniroma2.fase5.repositories.ScheduleChartRepository;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import it.uniroma2.isssr.integrazione.BusException;
import it.uniroma2.isssr.integrazione.BusMessage;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("ScheduleChartService")
public class ScheduleChartServiceImpl implements ScheduleChartService {

	@Autowired
	ScheduleChartRepository scheduleChartRepository;

	//private String busAdd = "http://ermes.sweng.uniroma2.it/Bus/inboundChannel.html";
	@Value("${bus.address}")
	private String busAdd;
	
	@Override
	public ResponseEntity<DTOresponse> createScheduleChart(String scheduleId,
			String fase, String startDate, String lastDate) throws JSONException {
		// TODO Auto-generated method stub
		ScheduleChart schedule = new ScheduleChart(scheduleId, fase, startDate, lastDate);
		scheduleChartRepository.save(schedule);

		JSONObject jsonRead = new JSONObject();

		String busAddress= this.busAdd;
		String msg= null;//msg di ritorno
		String payload = "{\"scheduleId\":\"" + schedule.getscheduleId() + "\","+ "\"fase\":\"" + schedule.getFase()+ "\","+ "\"startDate\":\"" + schedule.getStartDate() + "\","+ "\" lastDate\":\""+schedule.getLastDate() + "\"}";
		/* il formato da adottare per le nostre enità nel db hanno formato
		 * base64-Fase5-typeObject													*/

		jsonRead.put("objIdLocalToPhase", "5");
		jsonRead.put("typeObj", "base64-scheduleChart");
		jsonRead.put("instance", "metric"+ schedule.getscheduleId());
		jsonRead.put("busVersion", "1");
		jsonRead.put("tags", "[]");
		jsonRead.put("payload" , payload);

		try {
			System.out.println("\n\n\n-------------->"+jsonRead.toString()+"<-----------------------\n\n\n");
			BusMessage busMs = new BusMessage(BusMessage.OPERATION_CREATE,"phase5",jsonRead.toString());
			msg= busMs.send(busAddress);
			System.out.println("MSG METRIC: "+msg);


		} catch(MalformedURLException ex) {
			ex.printStackTrace();
		} catch(IOException ioex) {
			ioex.printStackTrace();
		}catch(BusException e){
			e.printStackTrace();
		}


		DTOresponse dtoresponse = new DTOresponse();		
		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse,HttpStatus.OK);
			
		return response;
	}

	@Override
	public ResponseEntity<DTOresponse> getScheduleChart() {
		// TODO Auto-generated method stub
		
		List<ScheduleChart> schedules = scheduleChartRepository.findAll();
		DTOresponse dtoresponse = new DTOresponse();
		dtoresponse.setScheduleChart(schedules);

		ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(
				dtoresponse, HttpStatus.OK);

		return response;
	}

	@Override
	public ResponseEntity<DTOresponse> deleteAllScheduleChart() {
		// TODO Auto-generated method stub
		scheduleChartRepository.delete(scheduleChartRepository.findAll());
		return null;
	}

	@Override
	public ResponseEntity<DTOresponse> deleteScheduleChart(String scheduleId) {
		// TODO Auto-generated method stub
		scheduleChartRepository.delete(scheduleChartRepository.findOne(scheduleId));

		return null;
	}

}

