package it.uniroma2.fase5.controller;


import org.springframework.http.ResponseEntity;

import it.uniroma2.fase5.model.rest.DTOresponse;

public interface IssueMessageResourceService {

	ResponseEntity<DTOresponse> createIssueMessageResource(String name,String url, String description);

	ResponseEntity<DTOresponse> getIssueMessageResource();

	ResponseEntity<DTOresponse> deleteIssueMessageResource(String name);
}
