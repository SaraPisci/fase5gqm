package it.uniroma2.fase5.controller;

import it.uniroma2.fase5.model.rest.DTOresponse;
import org.json.JSONException;
import org.springframework.http.ResponseEntity;

public interface ReportFase6Service {

    ResponseEntity<DTOresponse> createReportFase6 (String reportId, String json) throws JSONException;
    ResponseEntity<DTOresponse> getReportFase6 ();

}
