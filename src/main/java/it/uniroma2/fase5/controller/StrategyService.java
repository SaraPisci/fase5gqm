package it.uniroma2.fase5.controller;


import it.uniroma2.fase5.model.rest.DTOresponse;

import java.util.List;

import org.json.JSONException;
import org.springframework.http.ResponseEntity;

public interface StrategyService {
	ResponseEntity<DTOresponse> createStrategy(String strategyId,String strategyName,
			List<String> context, List<String> assumptions, String description,
			String creationDate, String lastModified, String timeFrame,
			int version, List<String> goalRef) throws JSONException;

	ResponseEntity<DTOresponse> createStrategy(String strategyId,String strategyName,
			List<String> context, List<String> assumptions, String description,
			String creationDate, String lastModified, String timeFrame,
			int version) throws JSONException;
			
	ResponseEntity<DTOresponse> getStrategies();

	ResponseEntity<DTOresponse> deleteStrategy(String strategyId);

}
