package it.uniroma2.fase5.controller;

import it.uniroma2.fase5.model.rest.DTOresponse;

import java.util.List;

import org.json.JSONException;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface  BusService {
	ResponseEntity<DTOresponse> sendBusMessage(String tag, List <String> content,String typeObject, String originAddress,String resolveAddress,String id);
	ResponseEntity<DTOresponse> receiveBusMessage(String typeObject);
	ResponseEntity<DTOresponse> queryTagMessage(String tag);
	ResponseEntity<DTOresponse> queryTagAndTypeMessage(String tag,String typeObject);
	ResponseEntity<DTOresponse> deleteAllBusMessage();
	ResponseEntity<DTOresponse> receiveRealBusMessage(String typeObject) throws JSONException ;
	ResponseEntity<DTOresponse> createBusEntity(String objIdLocalToPhase, String typeObject,String payload)throws JSONException, JsonProcessingException;
	ResponseEntity<DTOresponse> createSendingBus(String payload) throws JSONException;
	ResponseEntity<DTOresponse> sendDataAdjustementToBus(String payload)throws JSONException;
}
