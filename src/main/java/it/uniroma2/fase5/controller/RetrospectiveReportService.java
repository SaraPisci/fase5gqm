package it.uniroma2.fase5.controller;

import java.util.List;

import it.uniroma2.fase5.model.Feedback;
import org.json.JSONException;
import org.springframework.http.ResponseEntity;

import it.uniroma2.fase5.model.rest.DTOresponse;

public interface RetrospectiveReportService {

	ResponseEntity<DTOresponse> createRetrospectiveReport(String retrospectiveReportId,
			String conclusion, String lastModified,List<Integer> phases,List<String> priorityList, List<String> descriptionList) throws JSONException;
	ResponseEntity<DTOresponse> getRetrospectiveReports();
	ResponseEntity<DTOresponse> getRetrospectiveReportsById(String retrospectiveReportId);
	ResponseEntity<DTOresponse> deleteRetrospectiveReport(String retrospectiveReportId);
	ResponseEntity<DTOresponse> deleteAllRetrospectiveReport();
}
