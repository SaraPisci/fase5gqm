package it.uniroma2.fase5.controller;

import it.uniroma2.fase5.model.rest.DTOresponse;
import org.springframework.http.ResponseEntity;

public interface FinalReportService {

    ResponseEntity<DTOresponse> createFinalReport(String finalReportId, String dataModified, String newProposals, String projectRef);
    ResponseEntity<DTOresponse> findFinalReport();
/*    ResponseEntity<DTOresponse> getFinalReport();
    ResponseEntity<DTOresponse> updateDataModified(String id, String dataModified, String newProposals, boolean active);
    ResponseEntity<DTOresponse> deleteFinalReport(String finalReportId);
    ResponseEntity<DTOresponse> deleteAllFinalReport();
    ResponseEntity<DTOresponse> findFinalReportById(String id);
    ResponseEntity<DTOresponse> findRecentActiveFinalReport();
    ResponseEntity<DTOresponse> findFinalReportWithMaxId(); */
}
