package it.uniroma2.fase5.controller;

import java.util.List;

import org.json.JSONException;
import org.springframework.http.ResponseEntity;

import it.uniroma2.fase5.model.rest.DTOresponse;

public interface BusinessWorkflowIssueMessageService {

	ResponseEntity<DTOresponse> createBusinessWorkflowIssueMessage(String businessWorkflowInstanceId,String issueMessage, List<String> list) throws JSONException;
	
	ResponseEntity<DTOresponse> createBusinessWorkflowIssueMessage(String businessWorkflowInstanceId,String issueMessage) throws JSONException;

	ResponseEntity<DTOresponse> getBusinessWorkflowIssueMessage();

	ResponseEntity<DTOresponse> deleteBusinessWorkflowIssueMessage(String businessWorkflowInstanceId);
}
