package it.uniroma2.fase5.controller;

import it.uniroma2.fase5.model.rest.DTOresponse;

import org.json.JSONException;
import org.springframework.http.ResponseEntity;

public interface SendingOutComesFase6Service {
	ResponseEntity<DTOresponse> createSendingOutComeFase6 (String fileId,String json) throws JSONException;
	ResponseEntity<DTOresponse> getSendingOutComeFase6 ();
	ResponseEntity<DTOresponse> deleteAllSendingOutComeFase6 ();
}
