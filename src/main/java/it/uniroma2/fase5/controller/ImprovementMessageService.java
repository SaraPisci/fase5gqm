package it.uniroma2.fase5.controller;

import it.uniroma2.fase5.model.rest.DTOresponse;
import org.json.JSONException;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ImprovementMessageService {

    ResponseEntity<DTOresponse> createImprovementMessage(String improvementMessageId, String Message, String goalId,boolean active) throws JSONException;

    ResponseEntity<DTOresponse> getImprovementMessage();

    //ResponseEntity<DTOresponse> deleteMessage(String improvementMessageId);*/
}
