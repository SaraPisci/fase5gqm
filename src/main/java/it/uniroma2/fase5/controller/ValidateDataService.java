package it.uniroma2.fase5.controller;

import it.uniroma2.fase5.model.rest.DTOresponse;

import java.util.List;

import org.json.JSONException;
import org.springframework.http.ResponseEntity;

public interface ValidateDataService {
	ResponseEntity<DTOresponse> createValidateData(String dataId,List<String>data,String WorkFlowInstanceId,String metricRef) throws JSONException;

	ResponseEntity<DTOresponse> getValidateData();

}
