package it.uniroma2.fase5.repositories;

import it.uniroma2.fase5.model.ScheduleChart;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ScheduleChartRepository extends MongoRepository<ScheduleChart, String> {

}

