package it.uniroma2.fase5.repositories;

import it.uniroma2.fase5.model.ValidateData;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ValidateDataRepository extends MongoRepository<ValidateData, String> {

}
