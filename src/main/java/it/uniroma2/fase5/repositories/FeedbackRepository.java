package it.uniroma2.fase5.repositories;


import it.uniroma2.fase5.model.Feedback;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FeedbackRepository extends MongoRepository<Feedback, String> {
}
