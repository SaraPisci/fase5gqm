package it.uniroma2.fase5.repositories;

import it.uniroma2.fase5.model.ReportFase6;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ReportFase6Repository extends MongoRepository<ReportFase6, String> {
}
