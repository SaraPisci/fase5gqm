package it.uniroma2.fase5.repositories;

import it.uniroma2.fase5.model.FinalReport;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface FinalReportRepository extends MongoRepository<FinalReport, String> {
    @Query(value = "{ 'active' : true}")
    List<FinalReport> findRecentActive();
}
