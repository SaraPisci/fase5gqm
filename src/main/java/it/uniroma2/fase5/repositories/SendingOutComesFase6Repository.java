package it.uniroma2.fase5.repositories;

import it.uniroma2.fase5.model.SendingOutComesFase6;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface SendingOutComesFase6Repository  extends  MongoRepository<SendingOutComesFase6, String> {

}
