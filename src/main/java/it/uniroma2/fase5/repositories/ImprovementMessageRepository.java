package it.uniroma2.fase5.repositories;

import it.uniroma2.fase5.model.ImprovementMessage;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ImprovementMessageRepository extends MongoRepository<ImprovementMessage, String> {
}
