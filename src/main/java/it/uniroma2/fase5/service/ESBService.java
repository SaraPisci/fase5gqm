package it.uniroma2.fase5.service;

import it.uniroma2.fase5.model.classesFromBus.external.PointerBus;
import it.uniroma2.fase5.model.rest.DTOresponse;
import it.uniroma2.isssr.integrazione.BusException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

public interface ESBService {
    void pullFinalReport() throws JSONException;
    ResponseEntity<DTOresponse> pullDataFromPhase2() throws JSONException, IOException, BusException;

    void pushGoals(String payload, String title, String id) throws JSONException;

    String getJSONStringFromBus(String operation, JSONObject obj, String phase) throws JSONException;

}
