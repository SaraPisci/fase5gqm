package it.uniroma2.fase5.utility;

import it.uniroma2.fase5.model.rest.DTOresponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface JEXLValidatorService {
    void validateGoals();

    ResponseEntity<DTOresponse> analyzeInterpretationModel(String interpretationModel);
}
