package it.uniroma2.fase5.utility;

public enum ConditionTypes {
    eq,
    geq,
    leq,
    less,
    gret,
    neq;

    public static ConditionTypes toEnum(String value) {
        if (value.equals("==")) return eq;
        if (value.equals(">=")) return geq;
        if (value.equals("<=")) return leq;
        if (value.equals("<")) return less;
        if (value.equals(">")) return gret;
        if (value.equals("!=")) return neq;
        else return null;
    }


}
