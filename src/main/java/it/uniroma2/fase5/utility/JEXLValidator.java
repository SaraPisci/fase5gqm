package it.uniroma2.fase5.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.uniroma2.fase5.controller.BusService;
import it.uniroma2.fase5.interpretationalModelParser.InterpretationalModelCompiler;
import it.uniroma2.fase5.interpretationalModelParser.InterpretationalModelErrorListener;
import it.uniroma2.fase5.interpretationalModelParser.interpretationalModelGrammarLexer;
import it.uniroma2.fase5.interpretationalModelParser.interpretationalModelGrammarParser;
import it.uniroma2.fase5.model.classesFromBus.external.OrganizationalGoalBus;
import it.uniroma2.fase5.model.classesFromBus.external.PointerBus;
import it.uniroma2.fase5.model.classesFromBus.measurementGoal.MeasurementGoalBus;
import it.uniroma2.fase5.model.classesFromBus.metric.MeasurementResult;
import it.uniroma2.fase5.model.classesFromBus.metric.Ontology;
import it.uniroma2.fase5.model.classesFromBus.repository.MeasurementGoalBusRepository;
import it.uniroma2.fase5.model.classesFromBus.repository.OntologyBusRepository;
import it.uniroma2.fase5.model.classesFromBus.repository.OrganizationalGoalRepository;
import it.uniroma2.fase5.model.rest.DTOresponse;
import it.uniroma2.fase5.repositories.BusRepository;
import it.uniroma2.isssr.integrazione.BusException;
import it.uniroma2.isssr.integrazione.BusMessage;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.jexl3.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class JEXLValidator implements JEXLValidatorService {

    @Autowired
    MeasurementGoalBusRepository measurementGoalBusRepository;

    @Autowired
    OntologyBusRepository ontologyBusRepository;

    @Autowired
    OrganizationalGoalRepository organizationalGoalRepository;

    public Boolean validate(String expressionStr, ArrayList<Value> values) {

        // Create a JexlEngine (could reuse one instead)
        JexlEngine jexl = new JexlBuilder().create();
        // Create an expression object equivalent to 'car.getEngine().checkStatus()':
        JexlExpression expression = jexl.createExpression(expressionStr);
        // The car we have to handle coming as an argument...

        JexlContext jc = new MapContext();

        for (Value v : values) {
            // Create a context and add data
            jc.set(v.getName(), v.getValue());

        }

        // Now evaluate the expression, getting the result
        Object o = expression.evaluate(jc);
        return (Boolean) o;
    }

    private Boolean validateMeasurementGoal(MeasurementGoalBus mgoal) {
        List<PointerBus> pointerMetrics = mgoal.getMetrics();

        List<Ontology> metrics = new ArrayList<>();

        List<Ontology> list = ontologyBusRepository.findAll();

        for (PointerBus p : pointerMetrics) {
            Ontology ontology = ontologyBusRepository.findOne(p.getInstance());
            if (ontology != null)
                metrics.add(ontology);
        }

        ArrayList<Value> values = new ArrayList<>();

        for (Ontology metric : metrics) {
            String n = metric.getMeasurementModel().getName();

            ArrayList<MeasurementResult> results = metric.getMeasurements();
            if (results == null)
                continue;
            double v = Float.parseFloat(results.get(results.size() - 1).getValue());
            values.add(new Value(v, n));
        }

        Boolean validated = validate(mgoal.getInterpretationModel().getFunctionJavascript(), values);

        return validated;
    }

    private boolean isValidated(OrganizationalGoalBus organizationalGoalBus) {
        return organizationalGoalBus.getState() != null && (organizationalGoalBus.getState().equals("FAILURE") || organizationalGoalBus.getState().equals("SUCCESS"));
    }

    private String getValidatedString(boolean value) {
        return value ? "SUCCESS" : "FAILURE";
    }


    @Override
    public void validateGoals() {
        List<OrganizationalGoalBus> organizationalGoalsBus = organizationalGoalRepository.findAll();

        while (organizationalGoalsBus.size() != 0) {
            ArrayList<OrganizationalGoalBus> pending = new ArrayList<>();
            for (OrganizationalGoalBus organizationalGoalBus : organizationalGoalsBus) {
                List<OrganizationalGoalBus> subGoals = organizationalGoalBus.getSubGoals();
                if (subGoals == null || subGoals.size() == 0) {
                    MeasurementGoalBus measurementGoalBus = measurementGoalBusRepository.findAllByOrganizationalGoalId_InstanceContaining(organizationalGoalBus.getTitle());
                    organizationalGoalBus.setState(getValidatedString(validateMeasurementGoal(measurementGoalBus)));
                    organizationalGoalRepository.save(organizationalGoalBus);
                } else {
                    int validated = 0;
                    for (OrganizationalGoalBus subGoal : subGoals) {
                        if (!isValidated(subGoal)) {
                            pending.add(organizationalGoalBus);
                            break;
                        }
                        if (subGoal.getState().equals("FAILURE")) {
                            organizationalGoalBus.setState("FAILURE");
                            organizationalGoalRepository.save(organizationalGoalBus);
                            break;
                        }
                        else validated++;
                    }
                    if (validated == subGoals.size()) {
                        organizationalGoalBus.setState("SUCCESS");
                        organizationalGoalRepository.save(organizationalGoalBus);
                    }
                }
            }
            organizationalGoalsBus = new ArrayList<>();
            for(OrganizationalGoalBus o: pending){
                organizationalGoalsBus.add(organizationalGoalRepository.findOne(o.getId()));
            }
            pending = new ArrayList<>();
        }
    }

    @Override
    public ResponseEntity<DTOresponse> analyzeInterpretationModel(String interpretationModel) {
        InterpretationalModelErrorListener interpretationalModelErrorListener = new InterpretationalModelErrorListener();

        interpretationalModelGrammarLexer lexer = null;
        lexer = new interpretationalModelGrammarLexer(CharStreams.fromString(interpretationModel));
        lexer.removeErrorListeners();
        lexer.addErrorListener(interpretationalModelErrorListener);

        interpretationalModelGrammarParser parser = new interpretationalModelGrammarParser(new CommonTokenStream(lexer));
        parser.removeErrorListeners();
        parser.addErrorListener(interpretationalModelErrorListener);

        ParseTreeWalker walker = new ParseTreeWalker();
        InterpretationalModelCompiler listener = new InterpretationalModelCompiler();

        DTOresponse dtOresponse = new DTOresponse();

        try {
            walker.walk(listener, parser.main_expression());
            if (!interpretationalModelErrorListener.getParsingException().equals(""))
                throw new Exception(interpretationalModelErrorListener.getParsingException());
        } catch (Exception e) {
            dtOresponse.setMessage(e.getMessage());
            ResponseEntity<DTOresponse> responseEntity = new ResponseEntity<>(dtOresponse, HttpStatus.INTERNAL_SERVER_ERROR);
            return responseEntity;
        }


        ArrayList<Threshold> thresholdArrayList = listener.getTerminList();

        dtOresponse.setThresholdList(thresholdArrayList);

        ResponseEntity<DTOresponse> responseEntity = new ResponseEntity<>(dtOresponse, HttpStatus.OK);

        return responseEntity;
    }

}
