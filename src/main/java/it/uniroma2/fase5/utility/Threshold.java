package it.uniroma2.fase5.utility;

public class Threshold {
    private String metric;
    private Double threshold;
    private String conditionTypes;

    public Threshold(String metric, Double threshold, String conditionTypes) {
        this.metric = metric;
        this.threshold = threshold;
        this.conditionTypes = conditionTypes;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public Double getThreshold() {
        return threshold;
    }

    public void setThreshold(Double threshold) {
        this.threshold = threshold;
    }

    public String getConditionTypes() {
        return conditionTypes;
    }

    public void setConditionTypes(String conditionTypes) {
        this.conditionTypes = conditionTypes;
    }
}
