package it.uniroma2.fase5.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class LoginControllerImplementation implements LoginController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenRepository tokenRepository;

    @Override
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(DTOLogin dto, HttpServletResponse response) {

        User user = userRepository.findByUsername(dto.getUsername());

        if (user == null) {
            return ResponseEntity.notFound().build();
            //user = new User("kermit", "kermit", "", "", "", new Date(), "", "");
        }

        List<Token> list = tokenRepository.findAll();

        if (list.size() != 1) {
            //            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            list.add(new Token(String.valueOf("ciao").getBytes()));
        }

        String toHash = user.getUsername() + list.get(0).toString();
        MessageDigest digest = null;

        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        byte[] encodedHash = digest.digest(
                toHash.getBytes(StandardCharsets.UTF_8));
        String compare = Base64.getEncoder().encodeToString(encodedHash);
        System.out.println("Base64.getEncoder().encodeToString(encodedhash) = " + Base64.getEncoder().encodeToString(encodedHash));

        if (compare.equals(dto.getHash())) {
            System.out.println("Base64.getDecoder().decode(dto.getHash())) = " + Base64.getDecoder().decode(dto.getHash()));
            return new ResponseEntity<>(null, HttpStatus.OK);
        } else {
            System.out.println("Base64.getDecoder().decode(dto.getHash())) = " + Base64.getDecoder().decode(dto.getHash()));
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
