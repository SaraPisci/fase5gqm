package it.uniroma2.fase5.authentication;

import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;

@Document
public class Token {

    private byte[] token;

    public byte[] getToken() {
        return token;
    }

    public Token(byte[] token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "Token{" +
                "token=" + Arrays.toString(token) +
                '}';
    }
}
