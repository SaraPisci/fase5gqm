package it.uniroma2.fase5.rest;

import com.fasterxml.jackson.databind.ser.Serializers;
import it.uniroma2.fase5.controller.*;
import it.uniroma2.fase5.model.classesFromBus.service.*;
import it.uniroma2.fase5.model.rest.*;

import it.uniroma2.fase5.service.ESBService;
import it.uniroma2.fase5.utility.JEXLValidatorService;
import it.uniroma2.isssr.integrazione.BusException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Base64;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/")
@EnableAsync
public class RestPresentation {

	@Autowired
	ESBService integrationService;
	@Autowired
	StrategyService strategyService;
	@Autowired
	FinalReportService finalReportService;
	@Autowired
	RetrospectiveReportService retrospectiveReportService;
	@Autowired
	BusService busService;
	@Autowired
	ValidateDataService validateDataService;
	@Autowired
	ScheduleChartService scheduleChartService;
	@Autowired
	SendingOutComesFase6Service sendingOutComesFase6Service;
	@Autowired
	BusinessWorkflowIssueMessageService businessWorkflowIssueMessageService;
	@Autowired
	ImprovementMessageService improvementMessageService;
	@Autowired
	IssueMessageResourceService issueMessageResourceService;
	@Autowired
	ReportFase6Service reportFase6Service;
	@Autowired
	OrganizationalGoalService organizationalGoalService;
	@Autowired
	InstanceProjectService instanceProjectService;
	@Autowired
	MeasurementGoalBusService measurementGoalBusService;
	@Autowired
	OntologyBusService ontologyBusService;
	@Autowired
	QuestionBusService questionBusService;

	@Autowired
	JEXLValidatorService jexlValidatorService;

	@RequestMapping(value = "/pullFromPhase2", method = RequestMethod.PUT)
	public ResponseEntity<DTOresponse> pullFromPhase2() throws JSONException, IOException, BusException {
		return integrationService.pullDataFromPhase2();
	}



	@RequestMapping(value = "/createFinalReport/", method = RequestMethod.POST)
		public ResponseEntity<DTOresponse> createFinalReport(@RequestBody DTOFinalReport dtoFinalReport) throws JSONException {
			return finalReportService.createFinalReport(dtoFinalReport.getId(), dtoFinalReport.getDataModified(), dtoFinalReport.getNewProposals(),dtoFinalReport.getProjectRef());
		}


	@RequestMapping(value = "/getFinalReport", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getFinalReport() {
		return finalReportService.findFinalReport();
	}

	@RequestMapping(value = "/getStrategies/", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getStrategies() {
		return strategyService.getStrategies();
	}

	@RequestMapping(value = "/createStrategy/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> createStrategy(
			@RequestBody DTOStrategy dtoStrategy) throws JSONException {
		
		if(dtoStrategy.getGoalRef()==null)
			return strategyService.createStrategy(dtoStrategy.getStrategyId(),dtoStrategy.getStrategyName(),
					dtoStrategy.getContext(), dtoStrategy.getAssumptions(),
					dtoStrategy.getDescription(), dtoStrategy.getCreationDate(),
					dtoStrategy.getLastModified(), dtoStrategy.getTimeFrame(),
					dtoStrategy.getVersion() );
		else			
			return strategyService.createStrategy(dtoStrategy.getStrategyId(),dtoStrategy.getStrategyName(),
					dtoStrategy.getContext(), dtoStrategy.getAssumptions(),
					dtoStrategy.getDescription(), dtoStrategy.getCreationDate(),
					dtoStrategy.getLastModified(), dtoStrategy.getTimeFrame(),
					dtoStrategy.getVersion(), dtoStrategy.getGoalRef());
	}

	@RequestMapping(value = "/deleteStrategy/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> deleteStrategy(
			@RequestBody DTOStrategy dtoStrategy) {

		return strategyService.deleteStrategy(dtoStrategy.getStrategyId());

	}


	@RequestMapping(value = "/getRetrospectiveReports/", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getRetrospectiveReports() {
		return retrospectiveReportService.getRetrospectiveReports();
	}
	
	@RequestMapping(value = "/getRetrospectiveReportsById/{retrospectiveReportId}", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getRetrospectiveReportsById(@PathVariable(value="retrospectiveReportId") final String retrospectiveReportId) {
		return retrospectiveReportService.getRetrospectiveReportsById(retrospectiveReportId);
	}

	@RequestMapping(value = "/createRetrospectiveReport/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> createRetrospectiveReport(@RequestBody DTORetrospectiveReport dtoReport) throws JSONException {


		System.out.println("ID"+dtoReport.getIds());
		return retrospectiveReportService.createRetrospectiveReport(dtoReport.getRetrospectiveReportId(),
				dtoReport.getConclusion(), dtoReport.getLastModified(), dtoReport.getIds(),dtoReport.getPriorityList(),dtoReport.getDescriptionList());
	}
	@RequestMapping(value = "/deleteRetrospectiveReport/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> deleteRetrospectiveReport(@RequestBody DTORetrospectiveReport dtoReport) {

		return retrospectiveReportService.deleteRetrospectiveReport(dtoReport.getRetrospectiveReportId());

	}

	@RequestMapping(value = "/deleteAllRetrospectiveReports/", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> deleteAllRetrospectiveReport() {

		return retrospectiveReportService.deleteAllRetrospectiveReport();

	}
	
	@RequestMapping(value = "/createBusEntity/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> createBusMessage(@RequestBody DTOBus dtoBus) throws JsonProcessingException, JSONException {
		ResponseEntity<DTOresponse> temp=null;
		try {			
			 temp = busService.createBusEntity("",dtoBus.getTypeObject(),"");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		

		//busService.sendBusMessage(dtoBus.getTag(),dtoBus.getContent(),dtoBus.getTypeObject(),dtoBus.getOriginAddress(),dtoBus.getResolveAddress(),dtoBus.getId());
	return temp;
	}

	@RequestMapping(value = "/receiveRealBusMessage/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> receiveRealBusMessage(@RequestBody DTOBus dtoBus) throws JSONException {

		ResponseEntity<DTOresponse> temp=null;
		String t= dtoBus.getTypeObject();


		System.out.println("TYPE OBJECT "+ t);

		try {
			temp = busService.receiveRealBusMessage(dtoBus.getTypeObject());
			System.out.println("TEMP: "+t+" "+ temp.getBody().getMessage());

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("TEMP: "+ temp.getBody().getMessage());

		System.out.println("BODY--------------> "+ temp.getBody().getMessage() );

		JSONArray array = new JSONArray(temp.getBody().getMessage());
		System.out.println("LUNGHEZZA ARRAY = " + array.length());
	    for(int i=0; i<array.length(); i++){
	        JSONObject jsonObj  = array.getJSONObject(i);
	      //  System.out.println(jsonObj.getString("No"));
	        System.out.println("payload:"+jsonObj.getString("payload").toString());
	        temp.getBody().setMessage(jsonObj.getString("payload").toString());

	    }
	    
		return temp;
	}
	
	
	@RequestMapping(value = "/receiveBusMessage/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> receiveBusMessage(@RequestBody DTOBus dtoBus) throws JSONException {
		
		return busService.receiveBusMessage(dtoBus.getTypeObject());//se fallisce la chiamata al bus
		
	}
	
	
	
	

	@RequestMapping(value = "/createSendingBus/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> createSendingBus(@RequestBody String payload) throws JSONException {
		
		System.out.println("body to string: " + payload);
		return busService.createSendingBus(payload);
		
	}
	
	@RequestMapping(value = "/SendDataAdjustementToBus/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> sendDataAdjustementToBus(@RequestBody String payload) throws JSONException {
		
		System.out.println("body to string: " + payload);
		return busService.sendDataAdjustementToBus(payload);
		
	}

	
	@RequestMapping(value = "/queryTagMessage/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> queryTagMessage(@RequestBody DTOBus dtoBus) {

		return busService.queryTagMessage(dtoBus.getTag());
	}
	@RequestMapping(value = "/queryTagAndTypeMessage/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> queryTagAndTypeMessage(@RequestBody DTOBus dtoBus) {

		return busService.queryTagAndTypeMessage(dtoBus.getTag(),dtoBus.getTypeObject());
	}


	@RequestMapping(value = "/createValidateData/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> createValidateData(
			@RequestBody DTOValidateData dtoValidateData) throws JSONException {
		
		return validateDataService.createValidateData(dtoValidateData.getDataId(),dtoValidateData.getData(),dtoValidateData.getBusinessWorkflowInstanceId(),dtoValidateData.getMetricRef());

	}

	@RequestMapping(value = "/getValidateData/", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getValidateData() {
		return validateDataService.getValidateData();
	}

	@RequestMapping(value = "/deleteAllBusMessage/", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> deleteAllBusMessage() {

		return busService.deleteAllBusMessage();

	}


	@RequestMapping(value = "/createScheduleChart/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> createScheduleChart(
			@RequestBody DTOScheduleChart dtoScheduleChart) throws JSONException {

		return scheduleChartService.createScheduleChart(dtoScheduleChart.getScheduleId(),dtoScheduleChart.getFase(),dtoScheduleChart.getStartDate(),dtoScheduleChart.getLastDate());

	}

	@RequestMapping(value = "/getScheduleChart/", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getScheduleChart() {
		return scheduleChartService.getScheduleChart();
	}

	@RequestMapping(value = "/deleteAllScheduleChart/", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> deleteAllScheduleChart() {

		return scheduleChartService.deleteAllScheduleChart();

	}
	@RequestMapping(value = "/deleteScheduleChart/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> deleteScheduleChart(@RequestBody DTOScheduleChart dtoScheduleChart) {

		return scheduleChartService.deleteScheduleChart(dtoScheduleChart.getScheduleId());

	}

	@RequestMapping(value = "/createSendingOutComeFase6/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> createSendingOutComeFase6(
			@RequestBody DTOSendingOutComesFase6 dtoSendingOutComesFase6) throws JSONException {
		return sendingOutComesFase6Service.createSendingOutComeFase6(dtoSendingOutComesFase6.getFileId(),dtoSendingOutComesFase6.getjson());

	}

	//TODO Cancellare vecchi report prima di metterne uno nuovo
	@RequestMapping(value = "/createReportFase6/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> createReportFase6(
			@RequestBody DTOReportFase6 dtoReportFase6) throws JSONException {
	    System.out.println("REPORT: "+dtoReportFase6.getReportId());
        System.out.println("Json: "+dtoReportFase6.getJson());
		return reportFase6Service.createReportFase6(dtoReportFase6.getReportId(),dtoReportFase6.getJson());

	}



	@RequestMapping(value = "/getSendingOutComeFase6/", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getSendingOutComeFase6() {
		return sendingOutComesFase6Service.getSendingOutComeFase6();
	}
	
	@RequestMapping(value = "/deleteAllSendingOutComeFase6/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> deleteAllSendingOutComeFase6(@RequestBody DTOSendingOutComesFase6 dtoSendingOutComesFase6) {

		return sendingOutComesFase6Service.deleteAllSendingOutComeFase6();

	}
	
	@RequestMapping(value = "/getDataAdjustements/", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getBusinessWorkflowIssueMessages() {

		return businessWorkflowIssueMessageService.getBusinessWorkflowIssueMessage();
	}
	
	@RequestMapping(value = "/createDataAdjustement/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> createBusinessWorkflowIssueMessage(@RequestBody DTOBusinessWorkflowIssueMessage dtoDataAdj) throws JSONException {

		if( dtoDataAdj.getIssueMessageResources() == null)
			return businessWorkflowIssueMessageService.createBusinessWorkflowIssueMessage(dtoDataAdj.getBusinessWorkflowInstanceId(), dtoDataAdj.getIssueMessage());
		else return businessWorkflowIssueMessageService.createBusinessWorkflowIssueMessage(dtoDataAdj.getBusinessWorkflowInstanceId(), dtoDataAdj.getIssueMessage(), dtoDataAdj.getIssueMessageResources());
	}

    @RequestMapping(value = "/createDataImprovement/", method = RequestMethod.POST)
    public ResponseEntity<DTOresponse> createImprovementMessage(@RequestBody DTOImprovementMessage dtoImprovementMessage) throws JSONException {

            return improvementMessageService.createImprovementMessage(dtoImprovementMessage.getImprovementMessageId(), dtoImprovementMessage.getMessage(),dtoImprovementMessage.getGoalId(),dtoImprovementMessage.isActive());
    }

    @RequestMapping(value = "/getDataImprovement/", method = RequestMethod.GET)
    public ResponseEntity<DTOresponse> getDataImprovement() {
        return improvementMessageService.getImprovementMessage();
    }

	@RequestMapping(value = "/createIssueMessageResource/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> createIssueMessageResource(@RequestBody DTOIssueMessageResource dtoIssueMessageResource) {

		return issueMessageResourceService.createIssueMessageResource(dtoIssueMessageResource.getName(), dtoIssueMessageResource.getUrl(), dtoIssueMessageResource.getDescription());
	}

	
	@RequestMapping(value = "/deleteDataAdjustement/", method = RequestMethod.POST)
	public ResponseEntity<DTOresponse> deleteBusinessWorkflowIssueMessage(@RequestBody DTOBusinessWorkflowIssueMessage dtoDataAdj)
	{
		return businessWorkflowIssueMessageService.deleteBusinessWorkflowIssueMessage(dtoDataAdj.getBusinessWorkflowInstanceId());
	}

	@RequestMapping(value = "/getGoals", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getGoals() {
		return organizationalGoalService.getGoals();
	}

	@RequestMapping(value = "/getProject", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getProject() {
		return instanceProjectService.getProject();
	}


	@RequestMapping(value = "/getMeasurementGoal/{title}", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getMeasurementGoalByOrgGoalId(@PathVariable (value = "title") final String title) {
		System.out.println("title = " + title);
		title.replace("%20", " ");
		return measurementGoalBusService.getMeasurementGoalByOrgGoalId(title);
	}

	@RequestMapping(value = "/getMeasurementGoal", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getAllMeasurementGoals() {
		return measurementGoalBusService.getAllMeasurementGoals();
	}

	@RequestMapping(value = "/getOntologies", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getAllOntologies() {
		return ontologyBusService.getAllOntologies();
	}

	@RequestMapping(value = "/getQuestions", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getAllQuestions() {

		return questionBusService.getAllQuestions();
	}

	@RequestMapping(value = "/getThresholds/{im}", method = RequestMethod.GET)
	public ResponseEntity<DTOresponse> getThresholds(@PathVariable String im) {
		String interpretionModel = new String(Base64.getDecoder().decode(im));
		System.out.println("INTERPRETATION MODEL");
		System.out.println(interpretionModel);
		interpretionModel.replace("%20", " ");
		interpretionModel.replace("%3C", "<");
		interpretionModel.replace("%3E", ">");


		return jexlValidatorService.analyzeInterpretationModel(interpretionModel);
	}
	
}