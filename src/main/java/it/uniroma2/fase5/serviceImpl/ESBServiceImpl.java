package it.uniroma2.fase5.serviceImpl;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.uniroma2.fase5.authentication.LoginController;
import it.uniroma2.fase5.authentication.User;
import it.uniroma2.fase5.authentication.UserRepository;
import it.uniroma2.fase5.controller.FinalReportService;
import it.uniroma2.fase5.model.FinalReport;
import it.uniroma2.fase5.model.classesFromBus.external.InstanceProject;
import it.uniroma2.fase5.model.classesFromBus.external.OrganizationalGoalBus;
import it.uniroma2.fase5.model.classesFromBus.external.PointerBus;
import it.uniroma2.fase5.model.classesFromBus.external.StrategyBus;
import it.uniroma2.fase5.model.classesFromBus.measurementGoal.MeasurementGoalBus;
import it.uniroma2.fase5.model.classesFromBus.metric.Ontology;
import it.uniroma2.fase5.model.classesFromBus.question.QuestionBus;
import it.uniroma2.fase5.model.classesFromBus.service.*;
import it.uniroma2.fase5.model.rest.DTOFinalReport;

import it.uniroma2.fase5.model.rest.DTOresponse;
import it.uniroma2.fase5.service.ESBService;
import it.uniroma2.fase5.utility.JEXLValidator;
import it.uniroma2.fase5.utility.JEXLValidatorService;
import it.uniroma2.isssr.integrazione.BusException;
import it.uniroma2.isssr.integrazione.BusMessage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class ESBServiceImpl implements ESBService {

    @Value("${bus.address}")
    private String currentBusVersion;
    @Value("${bus.address}")
    private String currentBusVersion2;

    private static final String PHASE_06 = "phase6";
    private static final String PHASE_02 = "phase2";
    private static final String PHASE_04 = "phase4";
    private static final String PHASE_0 = "phase0";

    @Autowired
    FinalReportService finalReportService;
    @Autowired
    MeasurementGoalBusService measurementGoalService;
    @Autowired
    OntologyBusService ontologyService;
    @Autowired
    QuestionBusService questionBusService;
    @Autowired
    OrganizationalGoalService organizationalGoalService;
    @Autowired
    InstanceProjectService instanceProjectService;
    @Autowired
    StrategyBusService strategyBusService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    JEXLValidatorService validator;


    @Override
    public void pullFinalReport() throws JSONException {
        FinalReport finalReport;

        DTOFinalReport finalReportDb = getFinalReportFromESB();
        if (finalReportDb != null) {
            System.out.println("finalReport FINALE= " + finalReportDb);

            finalReportService.createFinalReport(finalReportDb.getId(), finalReportDb.getDataModified(), finalReportDb.getNewProposals(), finalReportDb.getProjectRef());
            //TODO finalReportDao -> db locale fase 5??
        }
        /*
        InstanceProject projectId = null;
        ArrayList<InstanceProjectDTO> remoteEntityList = null;


        remoteEntityList = getAllInstanceProjectsFromESB();


        if (remoteEntityList != null) {
            for (InstanceProjectDTO instanceprojectDTO : remoteEntityList) {
                projectId = modelMapper.map(instanceprojectDTO, InstanceProject.class);
            }
        }
        finalReport = modelMapper.map(finalReportDb, FinalReport.class);
        finalReport.setProjectRef(projectId);
        System.out.println("finalReport = " + finalReport);
        finalReportDao.save(finalReport); */
    }

    @Override
    public ResponseEntity<DTOresponse> pullDataFromPhase2() throws JSONException, BusException {


        pullProject();
        pullMeasurementGoals();
        pullStrategies();
        pullOntologiesPhase2();

        validator.validateGoals();

        pushGoalsToBus();

        pullFinalReport();



        return new ResponseEntity<>(HttpStatus.OK);
    }

    private void pullUsers() {

        String address = "";
        List<User> users = new ArrayList<User>();


        // Bus ready
        ObjectMapper mapper = new ObjectMapper();
        BusMessage busMessage = null;
        String responseFromBus;

        // Bus address
        address = currentBusVersion;
        JSONObject jsonGetUsers = new JSONObject();
        jsonGetUsers.put("objIdLocalToPhase", "");
        jsonGetUsers.put("typeObj", "user");
        jsonGetUsers.put("instance", "");
        jsonGetUsers.put("busVersion", "");

        try {
            busMessage = new BusMessage(BusMessage.OPERATION_GETUSERS, "users", jsonGetUsers.toString());

            responseFromBus = busMessage.send(address);
            users = mapper.readValue(responseFromBus, mapper.getTypeFactory()
                    .constructCollectionType(List.class, User.class));
        } catch (BusException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (users != null) {
            userRepository.save(users);
        }
    }


    public void pushGoalsToBus() throws JSONException {
        List<OrganizationalGoalBus> goalList = organizationalGoalService.getAllGoals();

        for (int i = 0; i < goalList.size(); i++) {
            deleteOrgGoalFromESB(goalList.get(i));
            System.out.print("DEL Eliminato dal Bus Goal con id" + goalList.get(i).getId());


            ObjectMapper mapper = new ObjectMapper();
            try {
                String obj = mapper.writeValueAsString(goalList.get(i));
                JSONObject jsonObject1 = new JSONObject(obj);
                pushGoals(jsonObject1.toString(), goalList.get(i).getTitle(), goalList.get(i).getId());
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void pushGoals(String payload, String title, String id) throws JSONException {
        String read = null;
        JSONObject jsonRead = new JSONObject();
        jsonRead.put("objIdLocalToPhase", id);
        jsonRead.put("typeObj", "base64-OrganizationalGoal");
        jsonRead.put("instance", title);
        jsonRead.put("busVersion", "");
        jsonRead.put("tags", "[]" );
        jsonRead.put("payload", payload);
        try {
            BusMessage readBus = new BusMessage(BusMessage.OPERATION_CREATE,"phase6",jsonRead.toString());
            read = readBus.send(currentBusVersion2);
            System.out.println("Create Message: " + read);
        } catch(MalformedURLException ex) {
            ex.printStackTrace();
        } catch(IOException ioex) {
            ioex.printStackTrace();
        }catch(BusException e){
            e.printStackTrace();
        }
        if(read.contains("ERR4")){
            try {
                BusMessage updateBus = new BusMessage(BusMessage.OPERATION_UPDATE,"phase6",jsonRead.toString());
                read = updateBus.send(currentBusVersion2);
                System.out.println("UPDATE MESSAGE " + read);
            } catch(MalformedURLException ex) {
                ex.printStackTrace();
            } catch(IOException ioex) {
                ioex.printStackTrace();
            }catch(BusException e){
                e.printStackTrace();
            }
        }

        DTOresponse dtoresponse = new DTOresponse();
        dtoresponse.setMessage(read);
        ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse, HttpStatus.OK);
    }

    private void deleteOrgGoalFromESB(OrganizationalGoalBus tempGoal) throws JSONException {
        JSONObject deleteRequest = new JSONObject();
        deleteRequest.put("objIdLocalToPhase", tempGoal.getId());
        deleteRequest.put("typeObj", "base64-OrganizationalGoal");
        deleteRequest.put("instance", tempGoal.getTitle());
        deleteRequest.put("busVersion", "");
        getJSONStringFromBus(BusMessage.OPERATION_DELETE, deleteRequest, "phase5");
    }

    @Override
    public String getJSONStringFromBus(String operation, JSONObject obj, String phase) throws JSONException {
        BusMessage message = null;
        String response = null;
        try {
            System.out.printf("Bus Message: operation=%s, phase=%s, obj=%s\n", operation, phase, obj.toString());
            message = new BusMessage(operation, phase, obj.toString());
            System.out.println("[getJSONStringFromBus] message = " + message);
            response = message.send(currentBusVersion2);
            System.out.println("[getJSONStringFromBus] Response in getJSONStringFromBus:" + response);
        } catch (BusException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }





    private void pullStrategies() throws BusException {
        JSONArray results;
        JSONObject busRequest;
        JSONObject strategiesJSON;
        ObjectMapper mapper = new ObjectMapper();
        busRequest = new JSONObject();

        busRequest.put("objIdLocalToPhase", "");
        busRequest.put("typeObj", "base64-Strategy"); //todo
        busRequest.put("instance", "");
        busRequest.put("busVersion", "");
        busRequest.put("tags", "[]");


        String stringtoParse = getJSONStringFromBusFromPhase2(BusMessage.OPERATION_READ, busRequest, PHASE_02);


        if (stringtoParse.startsWith("{"))//è un oggetto (messaggio di errore)
        {
            return;
        } else {
            results = new JSONArray(stringtoParse);
        }
        try {
            for (int index = 0; index < results.length(); index++) {
                strategiesJSON = new JSONObject(((JSONObject) results.get(index)).getString("payload"));
                System.out.println("strategiesJSON = " + strategiesJSON.toString());
                StrategyBus strategyBus = mapper.readValue(strategiesJSON.toString(), StrategyBus.class);
                strategyBusService.createStrategy(strategyBus);

            }
        } catch (IOException e) {
            throw new BusException("ESB is not working fine: error when fetching Strategies "+ e);
        }

    }

    private void pullTerminalStrategies() throws BusException {
        JSONArray results;
        JSONObject busRequest;
        JSONObject strategiesJSON;
        ObjectMapper mapper = new ObjectMapper();
        busRequest = new JSONObject();

        busRequest.put("objIdLocalToPhase", "");
        busRequest.put("typeObj", "base64-TerminalStrategy"); //todo
        busRequest.put("instance", "");
        busRequest.put("busVersion", "");
        busRequest.put("tags", "[]");


        String stringtoParse = getJSONStringFromBusFromPhase2(BusMessage.OPERATION_READ, busRequest, PHASE_02);


        if (stringtoParse.startsWith("{"))//è un oggetto (messaggio di errore)
        {
            return;
        } else {
            results = new JSONArray(stringtoParse);
        }
        try {
            for (int index = 0; index < results.length(); index++) {
                strategiesJSON = new JSONObject(((JSONObject) results.get(index)).getString("payload"));
                System.out.println("strategiesJSON = " + strategiesJSON.toString());
                StrategyBus strategyBus = mapper.readValue(strategiesJSON.toString(), StrategyBus.class);
                strategyBusService.createStrategy(strategyBus);

            }
        } catch (IOException e) {
            throw new BusException("ESB is not working fine: error when fetching Strategies "+ e);
        }

    }

    private void pullProject() throws BusException {
        JSONArray results;
        JSONObject busRequest;
        JSONObject instanceProjectJSON;
        ObjectMapper mapper = new ObjectMapper();
        busRequest = new JSONObject();

        busRequest.put("objIdLocalToPhase", "");
        busRequest.put("typeObj", "base64-InstanceProject");
        busRequest.put("instance", "");
        busRequest.put("busVersion", "");
        busRequest.put("tags", "[]");


        String stringtoParse = getJSONStringFromBusFromPhase2(BusMessage.OPERATION_READ, busRequest, PHASE_0);


        if (stringtoParse.startsWith("{"))//è un oggetto (messaggio di errore)
        {
            return;
        } else {
            results = new JSONArray(stringtoParse);
        }
        try {
            for (int index = 0; index < results.length(); index++) {
                instanceProjectJSON = new JSONObject(((JSONObject) results.get(index)).getString("payload"));
                InstanceProject instanceProject = mapper.readValue(instanceProjectJSON.toString(), InstanceProject.class);
                instanceProjectService.createInstanceProject(instanceProject);
            }
        } catch (IOException e) {
            throw new BusException("ESB is not working fine: error when fetching Measurement Goal");
        }


    }

    public void pullMeasurementGoals() throws BusException {
        JSONArray results;
        JSONObject busRequest;
        JSONObject measurementGoalJSON;
        ObjectMapper mapper = new ObjectMapper();
        busRequest = new JSONObject();

        busRequest.put("objIdLocalToPhase", "");
        busRequest.put("typeObj", "base64-MeasurementGoal");
        busRequest.put("instance", "");
        busRequest.put("busVersion", "");
        busRequest.put("tags", "[]");


        String stringtoParse = getJSONStringFromBusFromPhase2(BusMessage.OPERATION_READ, busRequest, PHASE_02);


        if (stringtoParse.startsWith("{"))//è un oggetto (messaggio di errore)
        {
            return;
        } else {
            results = new JSONArray(stringtoParse);
        }
        try {
            for (int index = 0; index < results.length(); index++) {
                measurementGoalJSON = new JSONObject(((JSONObject) results.get(index)).getString("payload"));
                MeasurementGoalBus measurementGoalBus = mapper.readValue(measurementGoalJSON.toString(), MeasurementGoalBus.class);
               measurementGoalService.createMeasurementGoal(measurementGoalBus);
                List<PointerBus> goalOntologies = measurementGoalBus.getMetrics();
                for (int i = 0; i < goalOntologies.size(); i++){
                    pullOntologies(goalOntologies.get(i));
                }
                List<PointerBus> goalQuestions = measurementGoalBus.getQuestions();
                for (int i = 0; i < goalQuestions.size(); i++){
                    pullQuestions(goalQuestions.get(i));
                }
                PointerBus goalOrgGoal = measurementGoalBus.getOrganizationalGoalId();
                pullOrgGoal(goalOrgGoal);
            }
        } catch (IOException e) {
            throw new BusException("ESB is not working fine: error when fetching Measurement Goal");
        }

    }

    private void pullOrgGoal(PointerBus pb) throws BusException {
        JSONArray results;
        JSONObject busRequest;
        JSONObject orgGoalJson;
        ObjectMapper mapper = new ObjectMapper();
        busRequest = new JSONObject();
        String typeObj = pb.getTypeObj();
        String instance = pb.getInstance();

        busRequest.put("objIdLocalToPhase", "");
        busRequest.put("typeObj", typeObj);
        busRequest.put("instance", instance);
        busRequest.put("busVersion", "");
        busRequest.put("tags", "[]");


        String stringtoParse = getJSONStringFromBusFromPhase2(BusMessage.OPERATION_READ, busRequest, PHASE_02);


        if (stringtoParse.startsWith("{"))//è un oggetto (messaggio di errore)
        {
            return;
        } else {
            results = new JSONArray(stringtoParse);
        }
        try {
            for (int index = 0; index < results.length(); index++) {
                orgGoalJson = new JSONObject(((JSONObject) results.get(index)).getString("payload"));
                System.out.println("STAMPA OGGETTO ORGGOAL: " + orgGoalJson.toString());
                Long projectId = orgGoalJson.getLong("projectId");
                InstanceProject instanceProject = instanceProjectService.getProjectById(String.valueOf(projectId));
                System.out.println("Instance projectId " + instanceProject);
                OrganizationalGoalBus orgGoalBus = mapper.readValue(orgGoalJson.toString(), OrganizationalGoalBus.class);
                orgGoalBus.setProjectId(instanceProject);

                //ASCENDANT STRATEGY
                StrategyBus orgGoalAscendantStrategy = null;
                if(orgGoalJson.has("ascendantStrategy")){
                    System.out.println("orgGoalJson.getAscendantStrategy= " + orgGoalJson.get("ascendantStrategy"));
                    Long ascendantStrategyId = orgGoalJson.getLong("ascendantStrategyId");
                    orgGoalAscendantStrategy = strategyBusService.getStrategyById(ascendantStrategyId);
                    orgGoalBus.setAscendantStrategy(orgGoalAscendantStrategy);
                }

                //DESCENDANT STRATEGY
                List<StrategyBus> orgGoalDescendantStrategies = new ArrayList<StrategyBus>();
                if(orgGoalJson.has("descendantStrategies")) {
                    JSONArray strategyArray = new JSONArray(orgGoalJson.get("descendantStrategies").toString());
                    for (int i = 0; i < strategyArray.length(); i++) {
                        Long descendantStrategyId = ((JSONObject) strategyArray.get(i)).getLong("id");
                        StrategyBus descendantStrategyBus = strategyBusService.getStrategyById(descendantStrategyId);
                        System.out.println("strategyBus = " + descendantStrategyBus);
                        orgGoalDescendantStrategies.add(descendantStrategyBus);
                    }
                    orgGoalBus.setDescendantStrategies(orgGoalDescendantStrategies);
                }
                if (orgGoalJson.has("mainGoal")){

                }
                if (orgGoalJson.has("subGoals")){

                }



                organizationalGoalService.createOrganizationalGoal(orgGoalBus);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new BusException("ESB is not working fine: error when fetching OrgGoals "+e);
        }

    }
    

    private void pullQuestions(PointerBus pb) throws BusException {
        JSONArray results;
        JSONObject busRequest;
        JSONObject questionBusJSON;
        ObjectMapper mapper = new ObjectMapper();
        busRequest = new JSONObject();
        String typeObj = pb.getTypeObj();
        String instance = pb.getInstance();

        busRequest.put("objIdLocalToPhase", "");
        busRequest.put("typeObj", "base64-Question");
        busRequest.put("instance", instance);
        busRequest.put("busVersion", "");
        busRequest.put("tags", "[]");


        String stringtoParse = getJSONStringFromBusFromPhase2(BusMessage.OPERATION_READ, busRequest, PHASE_02);


        if (stringtoParse.startsWith("{"))//è un oggetto (messaggio di errore)
        {
            return;
        } else {
            results = new JSONArray(stringtoParse);
        }
        try {
            for (int index = 0; index < results.length(); index++) {
                questionBusJSON = new JSONObject(((JSONObject) results.get(index)).getString("payload"));
                QuestionBus questionBus = mapper.readValue(questionBusJSON.toString(), QuestionBus.class);
                questionBusService.createQuestion(questionBus);
            }
        } catch (IOException e) {
            System.err.println("QUESTION EXCEPTION = " + e);
            e.printStackTrace();
            throw new BusException("ESB is not working fine: error when fetching Questions");
        }

    }

    public void pullOntologiesPhase2() throws BusException, JSONException {
        JSONArray results;
        JSONObject busRequest;
        JSONObject ontologyBusJSON;
        ObjectMapper mapper = new ObjectMapper();
        busRequest = new JSONObject();

        busRequest.put("objIdLocalToPhase", "");
        busRequest.put("typeObj", "base64-Ontology");
        busRequest.put("instance", "");
        busRequest.put("busVersion", "");
        busRequest.put("tags", "[]");


        String stringtoParse = getJSONStringFromBusFromPhase2(BusMessage.OPERATION_READ, busRequest, PHASE_02);
        System.out.println("ONTOLOGY stringtoParse = " + stringtoParse);

        if (stringtoParse.startsWith("{"))//è un oggetto (messaggio di errore)
        {
            return;
        } else {
            results = new JSONArray(stringtoParse);
        }
        try {
            for (int index = 0; index < results.length(); index++) {
                ontologyBusJSON = new JSONObject(((JSONObject) results.get(index)).getString("payload"));
                //ontologyBusJSON =  (JSONObject) validatedDataJSON.get("ontology");
                System.out.println("ontologyBusJSON = " + ontologyBusJSON);
                Ontology ontologyBus = mapper.readValue(ontologyBusJSON.toString(), Ontology.class);
                if (!ontologyService.findIfExists(ontologyBus.getId())){
                    ontologyService.createOntology(ontologyBus);
                }
            }
        } catch (IOException e) {
            throw new BusException("ESB is not working fine: error when fetching Ontology " + e);
        }

    }

    public void pullOntologies(PointerBus pb) throws BusException {
        JSONArray results;
        JSONObject busRequest;
        JSONObject ontologyBusJSON;
        ObjectMapper mapper = new ObjectMapper();
        busRequest = new JSONObject();
        String typeObj = pb.getTypeObj();
        String instance = pb.getInstance();

        busRequest.put("objIdLocalToPhase", "");
        busRequest.put("typeObj", "base64-validatedData");
        busRequest.put("instance", instance);
        busRequest.put("busVersion", "");
        busRequest.put("tags", "[]");


        String stringtoParse = getJSONStringFromBusFromPhase2(BusMessage.OPERATION_READ, busRequest, PHASE_04);
        System.out.println("ONTOLOGY stringtoParse = " + stringtoParse);

        if (stringtoParse.startsWith("{"))//è un oggetto (messaggio di errore)
        {
            return;
        } else {
            results = new JSONArray(stringtoParse);
        }
        try {
            for (int index = 0; index < results.length(); index++) {
                 JSONObject validatedDataJSON = new JSONObject(((JSONObject) results.get(index)).getString("payload"));
                ontologyBusJSON =  (JSONObject) validatedDataJSON.get("ontology");
                System.out.println("ontologyBusJSON = " + ontologyBusJSON);
                Ontology ontologyBus = mapper.readValue(ontologyBusJSON.toString(), Ontology.class);
                ontologyService.createOntology(ontologyBus);
            }
        } catch (IOException e) {
            System.err.println("EXCEPTION IO GUARD = " + e);
            e.printStackTrace();
            throw new BusException("ESB is not working fine: error when fetching Ontology " + e);
        }catch(Exception e){
            System.err.println("EXCEPTION GUARD = " + e);
            e.printStackTrace();
        }



    }

    public DTOFinalReport getFinalReportFromESB() throws JSONException {
        JSONArray results;
        JSONObject finalReportRequest;
        DTOFinalReport finalReportDTO = null;
        JSONObject finalReportJSON;
        ObjectMapper mapper = new ObjectMapper();

        finalReportRequest = new JSONObject();
        finalReportRequest.put("objIdLocalToPhase", "");
        finalReportRequest.put("typeObj", "base64-FinalReport");
        finalReportRequest.put("instance", "");
        finalReportRequest.put("busVersion", "");
        finalReportRequest.put("tags", "[]");

        String stringtoParse = getJSONStringFromBusFromPhase(BusMessage.OPERATION_READ, finalReportRequest, PHASE_06);

        if (stringtoParse.startsWith("{"))//è un oggetto (messaggio di errore)
        {
            System.err.println("NO FINAL REPORT EXIST");
            return null;
        } else {
            results = new JSONArray(stringtoParse);
        }
        try {
            for (int index = 0; index < results.length(); index++) {
                if (finalReportDTO == null){
                    finalReportDTO = new DTOFinalReport();
                }
                finalReportJSON = new JSONObject(((JSONObject) results.get(index)).getString("payload"));
                finalReportDTO = mapper.readValue(finalReportJSON.toString(), DTOFinalReport.class);
                System.out.println("finalReporDTO " + finalReportDTO);
                break;
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("ESB is not working fine: error when fecthing Final Report");
        }

        System.out.println("finalReportGUARDA" + finalReportDTO);

        return finalReportDTO;

    }


    private String getJSONStringFromBusFromPhase(String operation, JSONObject obj, String phase) throws JSONException {
        BusMessage message = null;
        String response = null;
        try {
            System.out.printf("Bus Message: operation=%s, phase=%s, obj=%s\n", operation, phase, obj.toString());
            message = new BusMessage(operation, phase, obj.toString());
            response = message.send(currentBusVersion);
        } catch (BusException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }



    private String getJSONStringFromBusFromPhase2(String operation, JSONObject obj, String phase) throws JSONException {
        BusMessage message = null;
        String response = null;
        try {
            System.out.printf("Bus Message: operation=%s, phase=%s, obj=%s\n", operation, phase, obj.toString());
            message = new BusMessage(operation, phase, obj.toString());
            response = message.send(currentBusVersion2);
        } catch (BusException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

}
