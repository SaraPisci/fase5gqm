package it.uniroma2.fase5.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class RetrospectiveReport {

	@Id
	private String retrospectiveReportId;
	private String conclusion;
	private String lastModified;
	private List<Integer> ids;
	private List<String> descriptionList;
	private List<String> priorityList;
	@DBRef
	private List<Feedback> feedbackList;

	public RetrospectiveReport() {
		this.ids=new ArrayList<Integer>();
		this.descriptionList=new ArrayList<String>();
		this.priorityList=new ArrayList<String>();
	}

	public RetrospectiveReport(String retrospectiveReportId, String conclusion, String lastModified, List<Integer> ids, List<String> descriptionList, List<String> priorityList) {
		this.retrospectiveReportId = retrospectiveReportId;
		this.conclusion = conclusion;
		this.lastModified = lastModified;
		this.ids = ids;
		this.descriptionList = descriptionList;
		this.priorityList = priorityList;
	}

	public RetrospectiveReport(String retrospectiveReportId, String conclusion, String lastModified, List<Feedback> feedbackList) {
		this.retrospectiveReportId = retrospectiveReportId;
		this.conclusion = conclusion;
		this.lastModified = lastModified;
		this.feedbackList = feedbackList;
	}

	public String getRetrospectiveReportId() {
		return retrospectiveReportId;
	}
	public void setRetrospectiveReportId(String retrospectiveReportId) {
		this.retrospectiveReportId = retrospectiveReportId;
	}
	public String getConclusion() {
		return conclusion;
	}
	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}
	public String getLastModified() {
		return lastModified;
	}
	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}


	public List<Integer> getIds() {
		return ids;
	}

	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}

	public List<String> getDescriptionList() {
		return descriptionList;
	}

	public void setDescriptionList(List<String> descriptionList) {
		this.descriptionList = descriptionList;
	}

	public List<String> getPriorityList() {
		return priorityList;
	}

	public void setPriorityList(List<String> priorityList) {
		this.priorityList = priorityList;
	}

	public List<Feedback> getFeedbackList() {
		return feedbackList;
	}

	public void setFeedbackList(List<Feedback> feedbackList) {
		this.feedbackList = feedbackList;
	}

//	public static void main(String[] args) {
//		RetrospectiveReport ret= new RetrospectiveReport();
//		ret.addRow("prova", "alta");
//		ret.addRow("prova", "bassa");
//		ret.addRow("prova", "media");
//		System.out.println(ret.getIds().size());
//		for(int i :ret.getIds()){
//			System.out.println(ret.getIds().get(i)+","+ret.getPriorityList().get(i) +","+ ret.getDescription().get(i) );
//		}
//	}
}
