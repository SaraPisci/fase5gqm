package it.uniroma2.fase5.model;

import org.springframework.data.annotation.Id;

public class ReportFase6 {

    @Id
    String reportId;

    public String getReportId() {
        return reportId;
    }

    public ReportFase6(String reportId, String json) {
        this.reportId = reportId;
        this.json = json;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    String json;
}
