package it.uniroma2.fase5.model;

import it.uniroma2.fase5.model.classesFromBus.external.OrganizationalGoalBus;
import it.uniroma2.fase5.model.classesFromBus.metric.Ontology;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ImprovementMessage {



    @Id
    private String improvementMessageId;
    private String message;
    private boolean active;


    @DBRef
    private OrganizationalGoalBus goal;

    public ImprovementMessage(String message, OrganizationalGoalBus goal, boolean active) {
        this.message = message;
        this.goal = goal;
        this.active=active;
    }

    public ImprovementMessage(String improvementMessageId, String message, OrganizationalGoalBus goal, boolean active) {
        this.improvementMessageId=improvementMessageId;
        this.message = message;
        this.goal = goal;
        this.active=active;
    }

    public ImprovementMessage() {
    }


    public String getImprovementMessageId() {
        return improvementMessageId;
    }

    public String getMessage() {
        return message;
    }

    public OrganizationalGoalBus getGoal() {
        return goal;
    }

    public boolean isActive() { return active; }

}
