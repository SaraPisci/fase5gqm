package it.uniroma2.fase5.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class IssueMessageResource {
	
	@Id
	private String name;
	private String url;
	private String description;
	
	public IssueMessageResource(){}
	
	public IssueMessageResource(String name, String url, String description) {
		super();
		this.name = name;
		this.url = url;
		this.description = description;
	}	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
