package it.uniroma2.fase5.model.rest;

import it.uniroma2.fase5.model.*;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import it.uniroma2.fase5.model.classesFromBus.external.InstanceProject;
import it.uniroma2.fase5.model.classesFromBus.external.OrganizationalGoalBus;
import it.uniroma2.fase5.model.classesFromBus.measurementGoal.MeasurementGoalBus;
import it.uniroma2.fase5.model.classesFromBus.metric.Ontology;
import it.uniroma2.fase5.model.classesFromBus.question.QuestionBus;
import it.uniroma2.fase5.utility.Threshold;

public class DTOresponse extends DTO {

	public DTOresponse(DTO dto, String string) {

		this.error = dto.getError();
		this.message = string;

	}

	public DTOresponse() {

	}

	public void setOntologies(List<Ontology> ontologies) {
		this.ontologies = ontologies;
	}
	

	public List<Ontology> getOntologies() {
		return ontologies;
	}

	public List<QuestionBus> getQuestions() {
		return questions;
	}

	public void setQuestions(List<QuestionBus> questions) {
		this.questions = questions;
	}

	public List<MeasurementGoalBus> getMeasurementGoals() {
		return measurementGoals;
	}

	public void setMeasurementGoals(List<MeasurementGoalBus> measurementGoals) {
		this.measurementGoals = measurementGoals;
	}

	public List<OrganizationalGoalBus> getGoals() {
		return goals;
	}

	public void setGoals(List<OrganizationalGoalBus> goals) {
		this.goals = goals;
	}

	/**

	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonInclude(Include.NON_NULL)
	private List<Ontology> ontologies;

	@JsonInclude(Include.NON_NULL)
	private List<QuestionBus> questions;

	@JsonInclude(Include.NON_NULL)
	private List<OrganizationalGoalBus> goals;

	@JsonInclude(Include.NON_NULL)
	private List<MeasurementGoalBus> measurementGoals;

	@JsonInclude(Include.NON_NULL)
	private List<Strategy> strategies;
	
	@JsonInclude(Include.NON_NULL)
	private List<RetrospectiveReport> retrospectiveReports;

	@JsonInclude(Include.NON_NULL)
	private List<InstanceProject> project;
	
	@JsonInclude(Include.NON_NULL)
	private List<ValidateData> validateData;
	
	@JsonInclude(Include.NON_NULL)
	private List<ScheduleChart> scheduleChart;
	
	@JsonInclude(Include.NON_NULL)
	private List<SendingOutComesFase6> sendingOutcomesFase6;
	@JsonInclude(Include.NON_NULL)
	private List<BusinessWorkflowIssueMessage> businessWorkflowIssueMessage;

	@JsonInclude(Include.NON_NULL)
	private FinalReport finalReport;

	@JsonInclude(Include.NON_NULL)
	private List<IssueMessageResource> issueMessageResource;


	@JsonInclude(Include.NON_NULL)
	private List<ImprovementMessage> improvementMessageList;

	@JsonInclude(Include.NON_NULL)
	private List<Threshold> thresholdList;
	

	public List<BusinessWorkflowIssueMessage> getBusinessWorkflowIssueMessage() {
		return businessWorkflowIssueMessage;
	}

	public void setBusinessWorkflowIssueMessage(
			List<BusinessWorkflowIssueMessage> businessWorkflowIssueMessage) {
		this.businessWorkflowIssueMessage = businessWorkflowIssueMessage;
	}

	public List<IssueMessageResource> getIssueMessageResource() {
		return issueMessageResource;
	}

	public void setIssueMessageResource(
			List<IssueMessageResource> issueMessageResource) {
		this.issueMessageResource = issueMessageResource;
	}

	public List<SendingOutComesFase6> getSendingOutcomesFase6() {
    	
		return sendingOutcomesFase6;
	}

	public void setSendingOutcomesFase6(
			List<SendingOutComesFase6> sendingOutcomesFase6) {
		this.sendingOutcomesFase6 = sendingOutcomesFase6;
	}

	public List<ValidateData> getValidateData() {
		return validateData;
	}

	public void setValidateData(List<ValidateData> validateData) {
		this.validateData = validateData;
	}

	@JsonInclude(Include.NON_NULL)
	private Bus busResponse;

	public Bus getBusResponse() {
		return busResponse;
	}

	public void setBusResponse(Bus busResponse) {
		this.busResponse = busResponse;
	}


	public List<Strategy> getStrategies() {
		return strategies;
	}

	public void setStrategies(List<Strategy> strategies) {
		this.strategies = strategies;
	}

	public List<InstanceProject> getProject() {
		return project;
	}

	public void setProject(List<InstanceProject> project) {
		this.project = project;
	}
	public List<RetrospectiveReport> getRetrospectiveReports() {
		return retrospectiveReports;
	}

	public void setRetrospectiveReports(List<RetrospectiveReport> retrospectiveReports) {
		this.retrospectiveReports = retrospectiveReports;
	}


	public void setImprovementMessageList(List<ImprovementMessage> improvementMessageList) {
		this.improvementMessageList = improvementMessageList;
	}

	public List<ScheduleChart> getScheduleChart() {
		return scheduleChart;
	}

	public List<ImprovementMessage> getImprovementMessageList() {
		return improvementMessageList;
	}


	public void setScheduleChart(List<ScheduleChart> scheduleChart) {
		this.scheduleChart = scheduleChart;
	}

	public FinalReport getFinalReport() {
		return finalReport;
	}

	public void setFinalReport(FinalReport finalReport) {
		this.finalReport = finalReport;
	}

	public List<Threshold> getThresholdList() {
		return thresholdList;
	}

	public void setThresholdList(List<Threshold> thresholdList) {
		this.thresholdList = thresholdList;
	}
}
