package it.uniroma2.fase5.model.rest;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class DTOBusinessWorkflowIssueMessage  extends DTO{

	
	private static final long serialVersionUID = 1L;
	@JsonInclude(Include.NON_NULL)
	private String businessWorkflowInstanceId;
	@JsonInclude(Include.NON_NULL)
	private String issueMessage;
	@JsonInclude(Include.NON_NULL)
	public List<String> issueMessageResources;
	
	public String getBusinessWorkflowInstanceId() {
		return businessWorkflowInstanceId;
	}
	public void setBusinessWorkflowInstanceId(String businessWorkflowInstanceId) {
		this.businessWorkflowInstanceId = businessWorkflowInstanceId;
	}
	public String getIssueMessage() {
		return issueMessage;
	}
	public void setIssueMessage(String issueMessage) {
		this.issueMessage = issueMessage;
	}
	public List<String> getIssueMessageResources() {
		return issueMessageResources;
	}
	public void setIssueMessageResources(List<String> issueMessageResources) {
		this.issueMessageResources = issueMessageResources;
	}
}
