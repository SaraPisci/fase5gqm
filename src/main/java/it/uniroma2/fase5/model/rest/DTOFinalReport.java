package it.uniroma2.fase5.model.rest;

import com.fasterxml.jackson.annotation.JsonInclude;

public class DTOFinalReport extends DTO {

    private static final long serialVersionUID = 1L;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String id;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String dataModified;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String newProposals;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String projectRef;



    @JsonInclude(JsonInclude.Include.NON_NULL)
    private boolean active;

    public DTOFinalReport(String finalReportId, String dataModified, String newProposals, String projectRef, boolean active) {
        this.id = finalReportId;
        this.dataModified = dataModified;
        this.newProposals = newProposals;
        this.projectRef = projectRef;
        this.active = active;
    }

    public DTOFinalReport() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDataModified() {
        return dataModified;
    }

    public void setDataModified(String dataModified) {
        this.dataModified = dataModified;
    }

    public String getNewProposals() {
        return newProposals;
    }

    public void setNewProposals(String newProposals) {
        this.newProposals = newProposals;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "DTOFinalReport{" +
                "id='" + id + '\'' +
                ", dataModified='" + dataModified + '\'' +
                ", newProposals='" + newProposals + '\'' +
                ", projectRef='" + projectRef + '\'' +
                ", active=" + active +
                '}';
    }
}
