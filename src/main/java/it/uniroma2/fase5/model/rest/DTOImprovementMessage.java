package it.uniroma2.fase5.model.rest;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

public class DTOImprovementMessage extends DTO {

    private static final long serialVersionUID = 1L;
    //@JsonInclude(JsonInclude.Include.NON_NULL)
    private String improvementMessageId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String goalId;
    private boolean active;


    public String getImprovementMessageId() {
        return improvementMessageId;
    }

    public void setImprovementMessageId(String improvementMessageId) {
        this.improvementMessageId = improvementMessageId;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    public String getGoalId() {
        return goalId;
    }

    public void setGoalId(String goalId) {
        this.goalId = goalId;
    }


    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
