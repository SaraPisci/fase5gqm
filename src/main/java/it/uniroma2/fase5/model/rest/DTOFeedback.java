package it.uniroma2.fase5.model.rest;

import com.fasterxml.jackson.annotation.JsonInclude;

public class DTOFeedback extends DTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String feedbackID;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer phase;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String priority;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String description;

    public String getFeedbackID() {
        return feedbackID;
    }

    public void setFeedbackID(String feedbackID) {
        this.feedbackID = feedbackID;
    }

    public Integer getPhase() {
        return phase;
    }

    public void setPhase(Integer phase) {
        this.phase = phase;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
