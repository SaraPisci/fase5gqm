package it.uniroma2.fase5.model.rest;

import com.fasterxml.jackson.annotation.JsonInclude;

public class DTOReportFase6 extends DTO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    String reportId;

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    String json;
}
