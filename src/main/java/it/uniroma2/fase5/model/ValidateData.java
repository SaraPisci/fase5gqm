package it.uniroma2.fase5.model;

import java.util.List;

import it.uniroma2.fase5.model.classesFromBus.metric.Ontology;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ValidateData {
	@Id
	private String dataId;

	private List<String> data;
	private String businessWorkflowInstanceId;
	@DBRef
	private Ontology ontologyRef;

	

	public ValidateData(String dataId, List<String> data,
			String businessWorkflowInstanceId,
						Ontology ontologyRef) {
		super();
		this.dataId = dataId;
		this.data = data;
		this.businessWorkflowInstanceId = businessWorkflowInstanceId;
		this.ontologyRef = ontologyRef;
	}

	

	public String getBusinessWorkflowInstanceId() {
		return businessWorkflowInstanceId;
	}



	public void setBusinessWorkflowInstanceId(String businessWorkflowInstanceId) {
		this.businessWorkflowInstanceId = businessWorkflowInstanceId;
	}



	public Ontology getOntologyRef() {
		return ontologyRef;
	}

	public void setOntologyRef(Ontology ontologyRef) {
		this.ontologyRef = ontologyRef;
	}

	public String getDataId() {
		return dataId;
	}

	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

	public List<String> getData() {
		return data;
	}

	public void setData(List<String> data) {
		this.data = data;
	}


	@Override
	public String toString() {

		return "{" +
				"\"dataId\":\"" + dataId + '\"' +
				",\"data\":" + data +
				",\"businessWorkflowInstanceId\":\"" + businessWorkflowInstanceId + '\"' +
				",\"ontologyRef\":\"" + ontologyRef + '\"' +
				"}";
	}

}
