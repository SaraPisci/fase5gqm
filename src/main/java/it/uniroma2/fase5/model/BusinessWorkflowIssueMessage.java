package it.uniroma2.fase5.model;

//import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class BusinessWorkflowIssueMessage {

	@Id
	private String businessWorkflowInstanceId;
	private String issueMessage;

	@DBRef
	private List<IssueMessageResource> issueMessageResources;

	public BusinessWorkflowIssueMessage() {
		super();
	}
	
	public BusinessWorkflowIssueMessage(String businessWorkflowInstanceId, String issueMessage) {
		super();
		this.businessWorkflowInstanceId = businessWorkflowInstanceId;
		this.issueMessage = issueMessage;
	}

	public BusinessWorkflowIssueMessage(String businessWorkflowInstanceId, String issueMessage, List<IssueMessageResource> issueMessageResources) {
		super();
		this.businessWorkflowInstanceId = businessWorkflowInstanceId;
		this.issueMessage = issueMessage;
		this.issueMessageResources = issueMessageResources;
	}

	public BusinessWorkflowIssueMessage(String businessWorkflowIssueMessageId) {

		this.businessWorkflowInstanceId = businessWorkflowIssueMessageId;
	}

	public String getBusinessWorkflowInstanceId() {
		return businessWorkflowInstanceId;
	}
	public void setBusinessWorkflowInstanceId(String businessWorkflowInstanceId) {
		this.businessWorkflowInstanceId = businessWorkflowInstanceId;
	}
	public String getIssueMessage() {
		return issueMessage;
	}
	public void setIssueMessage(String issueMessage) {
		this.issueMessage = issueMessage;
	}
	public List<IssueMessageResource> getIssueMessageResources() {
		return issueMessageResources;
	}
	public void setIssueMessageResources(List<IssueMessageResource> issueMessageResources) {
		this.issueMessageResources = issueMessageResources;
	}

}
