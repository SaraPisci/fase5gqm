package it.uniroma2.fase5.model;

import org.springframework.data.annotation.Id;

public class Feedback {

    @Id
    private String feedbackID;
    private Integer phase;
    private String priority;
    private String description;

    public Feedback() {
    }

    public Feedback(String feedbackID,Integer phase, String priority, String description)
    {
        this.feedbackID=feedbackID;
        this.phase=phase;
        this.priority = priority;
        this.description = description;
    }

    public String getFeedbackID() {
        return feedbackID;
    }

    public void setFeedbackID(String feedbackID) {
        this.feedbackID = feedbackID;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPhase() {
        return phase;
    }

    public void setPhase(Integer phase) {
        this.phase = phase;
    }


    @Override
    public String toString() {
        return "{" +
                "\"feedbackID\":'" + feedbackID + '\'' +
                ", \"phase\":" + phase +
                ", \"priority\":'" + priority + '\'' +
                ", \"description\":'" + description + '\'' +
                '}';
    }
}
