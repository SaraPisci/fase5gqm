package it.uniroma2.fase5.model;


import it.uniroma2.fase5.model.classesFromBus.external.InstanceProject;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class FinalReport {

    @Id
    private String finalReportId;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    private boolean active;

    public FinalReport(String finalReportId, String dataModified, String newProposals, InstanceProject projectRef, boolean active) {
        super();
        this.finalReportId = finalReportId;
        this.dataModified = dataModified;
        this.newProposals = newProposals;
        this.projectRef = projectRef;
        this.active = active;
    }

    public FinalReport(String finalReportId, String dataModified) {
        super();
        this.finalReportId = finalReportId;
        this.dataModified = dataModified;
    }

    public FinalReport(String finalReportId) {
        super();
        this.finalReportId = finalReportId;
    }


    public FinalReport(){

    }

    public FinalReport(String dataModified, String newProposals, InstanceProject projectRef) {
        super();
        this.dataModified = dataModified;
        this.newProposals = newProposals;
        this.projectRef = projectRef;
    }

    private String dataModified;

    private String newProposals;

    @DBRef
    private InstanceProject projectRef;

    public String getFinalReportId() {
        return finalReportId;
    }

    public void setFinalReportId(String finalReportId) {
        this.finalReportId = finalReportId;
    }

    public String getDataModified() {
        return dataModified;
    }

    public void setDataModified(String dataModified) {
        this.dataModified = dataModified;
    }

    public String getNewProposals() {
        return newProposals;
    }

    public void setNewProposals(String newProposals) {
        this.newProposals = newProposals;
    }

    public InstanceProject getProjectRef() {
        return projectRef;
    }

    public String getProjectIdRef(){
        return this.projectRef.getId();
    }


    public void setProjectRef(InstanceProject projectRef) {
        this.projectRef = projectRef;
    }
}
