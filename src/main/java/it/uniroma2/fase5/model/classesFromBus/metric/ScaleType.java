package it.uniroma2.fase5.model.classesFromBus.metric;

public enum ScaleType {

	nominalScale,
	ordinalScale,
	intervalScale,
	ratioScale,
	absoluteScale,
	
}
