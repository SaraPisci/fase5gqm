package it.uniroma2.fase5.model.classesFromBus.metric.attribute;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by MacH2o on 12/07/17.
 */
@Getter
@Setter
@AllArgsConstructor
public class External extends Attribute {
    private String from;

    public External() {
        super();
    }
}
