package it.uniroma2.fase5.model.classesFromBus.measurementGoal;


import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.uniroma2.fase5.model.classesFromBus.Element;
import it.uniroma2.fase5.model.classesFromBus.external.PointerBus;

import org.springframework.data.mongodb.core.mapping.Document;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Document
@Data
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
public class MeasurementGoalBus extends Element {
	
	public MeasurementGoalBus(){
		this.interpretationModel = new InterpretationModel();
		this.assumptions = new ArrayList<PointerBus>();
		this.contextFactors= new ArrayList<PointerBus>();
		this.questionersId= new ArrayList<String>();
		this.questions= new ArrayList<PointerBus>();
		this.metrics= new ArrayList<PointerBus>();
		
		//this.contexts = new ArrayList<Context>();
		//this.assumptions = new ArrayList<Assumption>();
		//this.metrics = new ArrayList<Metric>();
		//this.questioners = new ArrayList<Questioner>();
		//this.metricator = new Metricator();
	}

	@JsonIgnore
	private String goal;

	private PointerBus organizationalGoalId;
	
	private String object;
	
	private String purpose;
	
	private String qualityFocus;
	
	private String viewPoint;
	
	private List<PointerBus> contextFactors;
	
	private List<PointerBus> assumptions;
	
	private InterpretationModel interpretationModel;


	private List<PointerBus> metrics;


	private List<PointerBus> questions;
	
    private String metricatorId;

	private List<String> questionersId;

	public List<PointerBus> getMetrics() {
		return metrics;
	}

	public List<PointerBus> getQuestions() {
		return questions;
	}

	public PointerBus getOrganizationalGoalId() {
		return organizationalGoalId;
	}




}
