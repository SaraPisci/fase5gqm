package it.uniroma2.fase5.model.classesFromBus.service;

import it.uniroma2.fase5.model.classesFromBus.external.StrategyBus;

public interface StrategyBusService {
    void createStrategy(StrategyBus strategyBus);

    StrategyBus getStrategyById(Long strategyId);
}
