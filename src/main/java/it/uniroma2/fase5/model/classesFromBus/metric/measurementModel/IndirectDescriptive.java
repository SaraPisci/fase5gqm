package it.uniroma2.fase5.model.classesFromBus.metric.measurementModel;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by MacH2o on 12/07/17.
 */
@Getter
@Setter
@AllArgsConstructor
@JsonTypeName("IndirectDescriptive")
public class IndirectDescriptive extends IndirectMeasurementModel {
    private String derivedMeas;
    public IndirectDescriptive() {
        super();

    }
}
