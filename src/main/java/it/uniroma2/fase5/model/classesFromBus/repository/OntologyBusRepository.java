package it.uniroma2.fase5.model.classesFromBus.repository;

import it.uniroma2.fase5.model.classesFromBus.metric.Ontology;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface OntologyBusRepository extends MongoRepository<Ontology, String> {

    List<Ontology> findAllByGoal_Id(String goalId);
    List<Ontology> findAllById(String id);

}
