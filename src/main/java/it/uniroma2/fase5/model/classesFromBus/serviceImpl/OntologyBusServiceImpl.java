package it.uniroma2.fase5.model.classesFromBus.serviceImpl;

import it.uniroma2.fase5.model.classesFromBus.measurementGoal.MeasurementGoalBus;
import it.uniroma2.fase5.model.classesFromBus.metric.Ontology;
import it.uniroma2.fase5.model.classesFromBus.repository.OntologyBusRepository;
import it.uniroma2.fase5.model.classesFromBus.service.OntologyBusService;
import it.uniroma2.fase5.model.rest.DTOresponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OntologyBusServiceImpl implements OntologyBusService{
    @Autowired
    OntologyBusRepository ontologyBusRepository;
    public void createOntology(Ontology ontology){

        Ontology oldOntology = ontologyBusRepository.findOne(ontology.getId());
        if (oldOntology != null) {
            ontologyBusRepository.delete(ontology.getId());

        }
        try {
            ontologyBusRepository.insert(ontology);
        }
        catch(org.springframework.dao.DuplicateKeyException e){
            //d.printStackTrace();
            System.out.println("Caught Exception " + e);
        }
    }

    @Override
    public ResponseEntity<DTOresponse> getAllOntologies() {
        List<Ontology> ontologyList = ontologyBusRepository.findAll();
        DTOresponse dtoresponse = new DTOresponse();
        dtoresponse.setOntologies(ontologyList);
        ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse, HttpStatus.OK);
        return  response;
    }

    @Override
    public boolean findIfExists(String id) {
        Ontology ontology = ontologyBusRepository.findOne(id);
        if (ontology == null){
            return false;
        }else{
            return true;
        }
    }
}
