package it.uniroma2.fase5.model.classesFromBus.repository;

import it.uniroma2.fase5.model.classesFromBus.question.QuestionBus;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface QuestionBusRepository extends MongoRepository<QuestionBus, String> {
}
