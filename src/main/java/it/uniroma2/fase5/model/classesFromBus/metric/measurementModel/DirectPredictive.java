package it.uniroma2.fase5.model.classesFromBus.metric.measurementModel;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by MacH2o on 12/07/17.
 */
@Getter
@Setter
@JsonTypeName("DirectPredictive")
public class DirectPredictive extends DirectMeasurementModel {
    private String baseMeas;
    private String expectedBehavior;

    public DirectPredictive() {
        super();

    }

    public DirectPredictive(String baseMeas, String expectedBehavior) {
        super();
        this.baseMeas = baseMeas;
        this.expectedBehavior = expectedBehavior;
    }
}
