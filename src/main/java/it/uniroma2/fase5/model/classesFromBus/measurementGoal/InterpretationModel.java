package it.uniroma2.fase5.model.classesFromBus.measurementGoal;


import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper=true)
public class InterpretationModel {
	
	private String functionJavascript;
	private String queryNoSQL;
}
