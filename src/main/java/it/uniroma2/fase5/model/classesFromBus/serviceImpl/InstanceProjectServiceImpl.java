package it.uniroma2.fase5.model.classesFromBus.serviceImpl;

import it.uniroma2.fase5.model.classesFromBus.external.InstanceProject;
import it.uniroma2.fase5.model.classesFromBus.measurementGoal.MeasurementGoalBus;
import it.uniroma2.fase5.model.classesFromBus.repository.InstanceProjectRepository;
import it.uniroma2.fase5.model.classesFromBus.service.InstanceProjectService;
import it.uniroma2.fase5.model.rest.DTOresponse;
import org.hibernate.validator.constraints.EAN;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InstanceProjectServiceImpl implements InstanceProjectService{
    @Autowired
    InstanceProjectRepository instanceProjectRepository;
    @Override
    public void createInstanceProject(InstanceProject instanceProject) {
        InstanceProject oldInstanceProject = instanceProjectRepository.findOne(instanceProject.getId());
        System.out.println("oldMGoal:   " + oldInstanceProject);
        if (oldInstanceProject != null) {
            instanceProjectRepository.delete(instanceProject.getId());

        }
        try {
            instanceProjectRepository.insert(instanceProject);
        }
        catch(org.springframework.dao.DuplicateKeyException e){
            //d.printStackTrace();
            System.out.println("Caught Exception " + e);
        }
    }

    @Override
    public ResponseEntity<DTOresponse> getProject() {
        List<InstanceProject> project = instanceProjectRepository.findAll();
        if (project.size() == 0){
            project = new ArrayList<InstanceProject>();
        }
        DTOresponse dtoresponse = new DTOresponse();
        dtoresponse.setProject(project);
        ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse, HttpStatus.OK);
        return response;
    }

    @Override
    public InstanceProject getProjectById(String id) {
        return instanceProjectRepository.findOne(id);
    }
}
