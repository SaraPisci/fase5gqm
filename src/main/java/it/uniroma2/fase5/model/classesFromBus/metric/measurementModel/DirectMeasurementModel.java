package it.uniroma2.fase5.model.classesFromBus.metric.measurementModel;


import lombok.*;


@Setter
@Getter


public abstract class DirectMeasurementModel extends MeasurementModel {

    public DirectMeasurementModel() {
        super();
    }
}
