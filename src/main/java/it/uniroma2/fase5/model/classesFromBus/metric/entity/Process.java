package it.uniroma2.fase5.model.classesFromBus.metric.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by MacH2o on 11/07/17.
 */
@Getter
@Setter
@AllArgsConstructor
public class Process extends GenericEntity {
    String serial;

    public Process() {
        super();
    }
}
