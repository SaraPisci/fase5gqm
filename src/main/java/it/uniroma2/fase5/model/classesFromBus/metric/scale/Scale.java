package it.uniroma2.fase5.model.classesFromBus.metric.scale;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.*;

/**
 * Created by MacH2o on 10/07/17.
 */
@Setter
@Getter
@AllArgsConstructor
@RequiredArgsConstructor

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Interval.class, name = "Interval"),
        @JsonSubTypes.Type(value = Nominal.class, name = "Nominal"),
        @JsonSubTypes.Type(value = Ordinal.class, name = "Ordinal"),
        @JsonSubTypes.Type(value = Ratio.class, name = "Ratio"),
})
public abstract class Scale {
    protected String name;
    protected String description;
    protected String type;
}
