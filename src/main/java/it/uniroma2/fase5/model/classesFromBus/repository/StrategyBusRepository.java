package it.uniroma2.fase5.model.classesFromBus.repository;

import it.uniroma2.fase5.model.classesFromBus.external.StrategyBus;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StrategyBusRepository  extends MongoRepository<StrategyBus, String> {
    StrategyBus findById(Long strategyId);
}
