package it.uniroma2.fase5.model.classesFromBus.service;

import it.uniroma2.fase5.model.classesFromBus.question.QuestionBus;
import it.uniroma2.fase5.model.rest.DTOresponse;
import org.springframework.http.ResponseEntity;

public interface QuestionBusService {
    void createQuestion(QuestionBus question);
    ResponseEntity<DTOresponse> getAllQuestions();
}
