package it.uniroma2.fase5.model.classesFromBus.question;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.uniroma2.fase5.model.classesFromBus.Element;
import it.uniroma2.fase5.model.classesFromBus.external.PointerBus;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@RequiredArgsConstructor
@ToString
@Document
public class QuestionBus extends Element {
	private String focus;
	private String subject;
	private String description;
	private String questionerId;

	@JsonIgnore
	private PointerBus ontologies;

}



