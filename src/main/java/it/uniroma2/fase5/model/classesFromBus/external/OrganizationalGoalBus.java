package it.uniroma2.fase5.model.classesFromBus.external;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@ToString
@AllArgsConstructor
public class OrganizationalGoalBus {

	@Id
	private String id;

	private String focus;
	private String magnitude;
	private String object;
	private String timeFrame;
	private String title;
	private String information;
	private String state; // {APPROVED, PROPOSED, REFUSED}
	private String type;

	@DBRef
	private StrategyBus ascendantStrategy;

	private Long ascendantStrategyId;

	@DBRef
	private OrganizationalGoalBus maingoal;

	public List<OrganizationalGoalBus> getSubGoals() {
		return subGoals;
	}

	@DBRef
	private List<OrganizationalGoalBus> subGoals;
	@DBRef
	private List<StrategyBus> descendantStrategies;
	@DBRef
    @JsonIgnore
	private InstanceProject projectId;
	private List<String> constraints;
	@DBRef
	private List<Assumption> assumptions;
	@DBRef
	private List<ContextFactor> contextFactors;
	@JsonIgnore
	private String scope;

    public OrganizationalGoalBus() {
    }




    public void AddSubgoal(OrganizationalGoalBus goal){
		subGoals.add(goal);
	}
	public void removeSubGoal(OrganizationalGoalBus subGoal) {
		this.subGoals.remove(subGoal);

	}
	public boolean hasDescendantStrategies(){
		return !descendantStrategies.isEmpty();
	}
	public boolean hasSubgoal(OrganizationalGoalBus subGoal){
		return this.subGoals.contains(subGoal);
	}
	public void addAssumption(Assumption a){
		this.assumptions.add(a);
	}
	public void addContextFactor(ContextFactor c){
		this.contextFactors.add(c);
	}
	public boolean hasAssumption(Assumption assumption) {
		return assumptions.contains(assumption);
	}
	public boolean hasContextFactor(ContextFactor conFact) {

		return contextFactors.contains(conFact);
	}
	public void removeAssumption(Assumption assumption) {
		assumptions.remove(assumption);

	}
	public void removeContextFactor(ContextFactor conFact) {
		contextFactors.remove(conFact);
	}
	public void addConstraint(String c){
		constraints.add(c);
	}
	public boolean hasConstraint(String c){
		return constraints.contains(c);
	}
	public void removeConstraint(String c){
		constraints.remove(c);
	}

	public void addDescendantStrategy(StrategyBus toLinkStrategy) {
		descendantStrategies.add(toLinkStrategy);
	}
	public void addAscendentGoal(OrganizationalGoalBus goal) {
		// TODO Auto-generated method stub
		maingoal=goal;
	}
	public void removeAscendentGoal() {
		maingoal=null;

	}
	public boolean hasSubgoals() {
		// TODO Auto-generated method stub
		return !subGoals.isEmpty();
	}
	public void addAscendantStrategy(StrategyBus strategy) {
		ascendantStrategy=strategy;
	}
	public boolean hasAscendantStrategy() {
		// TODO Auto-generated method stub
		return ascendantStrategy!=null;
	}
	public boolean hasAscendantGoal() {
		// TODO Auto-generated method stub
		return maingoal!=null;
	}
	/*public boolean descendFromGoal(OrganizationalGoalBus g) {
		if(this.maingoal!=null)
			if(this.maingoal.id==g.id)
				return true;
			else return this.maingoal.descendFromGoal(g);
		else
			if(this.ascendantStrategy!=null)
				return this.ascendantStrategy.descendFromGoal(g);
			else return false;
	}*/
	public boolean hasAscendantStrategy(StrategyBus strategy) {

		return ascendantStrategy.getId()==strategy.getId();
	}
	public void removeAscendantStrategy() {
		ascendantStrategy=null;

	}
	/*
	public boolean descendFromStrategy(StrategyBus s) {
		if (this.ascendantStrategy!=null)
			if(this.ascendantStrategy.getId()==s.getId())
				return true;
			else
				return this.ascendantStrategy.descendFromStrategy(s);
		else
			if(this.maingoal!=null)
				return maingoal.descendFromStrategy(s);
			else return false;
			
	}*/
	public void removeDescendantStrategy(StrategyBus strategy) {
		this.descendantStrategies.remove(strategy);
		
	}

	public String getId(){
		return this.id;
	}

	public String getTitle(){
		return this.title;
	}
}
