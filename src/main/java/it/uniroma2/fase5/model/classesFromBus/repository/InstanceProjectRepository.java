package it.uniroma2.fase5.model.classesFromBus.repository;

import it.uniroma2.fase5.model.classesFromBus.external.InstanceProject;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface InstanceProjectRepository  extends MongoRepository<InstanceProject, String> {
}
