package it.uniroma2.fase5.model.classesFromBus.service;

import it.uniroma2.fase5.model.classesFromBus.external.OrganizationalGoalBus;
import it.uniroma2.fase5.model.rest.DTOresponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface OrganizationalGoalService {
    void createOrganizationalGoal(OrganizationalGoalBus organizationalGoal);

    ResponseEntity<DTOresponse> getGoals();

    List<OrganizationalGoalBus> getAllGoals();
}
