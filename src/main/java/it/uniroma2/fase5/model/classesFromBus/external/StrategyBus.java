package it.uniroma2.fase5.model.classesFromBus.external;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@ToString
public class StrategyBus {
	
	@Id
	private Long id;
	//private OrganizationalGoalBus ascendantGoal;
	private Long ascendantGoalId;
	@DBRef
	private StrategyBus ascendantStrategy;
	//private List<OrganizationalGoalBus> descendantGoals;
	@DBRef
	private List<StrategyBus> subStrategies;
	private Long projectId;
	@JsonIgnore
	private String state;
	private void setStrategyState(String s){
		this.state = s;
	}
	private List<ContextFactor> linkedContextFactors;
	private List<Assumption> linkedAssumptions;

	public void addSubStrategy(StrategyBus childStrategy){
		this.subStrategies.add(childStrategy);
	}
	public boolean hasStrategy(StrategyBus strategy){
		return subStrategies.contains(strategy);
	}

	private String title;
	private String description;
	/*public void addAscendantGoal(OrganizationalGoalBus goal) {
		ascendantGoal=goal;
	}*/
	/*
	public boolean hasDescendantGoal(){
		return !descendantGoals.isEmpty();
	}
	*/
	public void linkAscendantStrategy(StrategyBus childStrategy) {
		ascendantStrategy=childStrategy;
	}

	/*
	public void AddDescendantGoal(OrganizationalGoalBus goal) {
		descendantGoals.add(goal);
	}*/
	public void removeSubStrategy(StrategyBus childStrategy) {
		this.subStrategies.remove(childStrategy);

	}
	
	public void addContextFactor(ContextFactor factor){
		this.linkedContextFactors.add(factor);
	}
	public void removeContextFactor(ContextFactor factor){
		this.linkedContextFactors.remove(factor);
	}
	public void addAssumption(Assumption assumption){
		this.linkedAssumptions.add(assumption);
	}
	public void removeAssumption(Assumption assumption){
		this.linkedAssumptions.remove(assumption);
	}

	public boolean hasAscendantStrategy() {
		return this.ascendantStrategy!=null;
	}

	public Long getId(){
		return this.id;
	}

	public String getTitle(){
		return this.title;
	}


}



