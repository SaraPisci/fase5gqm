package it.uniroma2.fase5.model.classesFromBus.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import it.uniroma2.fase5.model.classesFromBus.Entity;
import it.uniroma2.fase5.model.classesFromBus.State;
import lombok.Data;

@Data
public class MetadataDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6227428458702853303L;
	public String id;
	public String version;
	public List<String> tags;
	public String creationDate;
	public String lastVersionDate;
	public String creatorId;
	public State state;
	public String releaseNote;
	public Entity entityType;
	public String versionBus;
	

	@JsonProperty("creationDate")
	public void setCreationDate(String date){
		this.creationDate = date;
	}
	
	@JsonProperty("lastVersionDate")
	public void setLastVersionDate(String date){
		this.lastVersionDate = date;
	}
	@JsonIgnore
	public void setCreationDate(LocalDate date){
		if(date!=null){
			this.creationDate = date.toString();
			}
	}
	@JsonIgnore
	public void setLastVersionDate(LocalDate date){
		if(date != null){
			this.lastVersionDate = date.toString();
			}
	}


}
