package it.uniroma2.fase5.model.classesFromBus.metric;

import it.uniroma2.fase5.model.classesFromBus.Element;
import it.uniroma2.fase5.model.classesFromBus.measurementGoal.MeasurementGoalBus;
import it.uniroma2.fase5.model.classesFromBus.metric.attribute.External;
import it.uniroma2.fase5.model.classesFromBus.metric.attribute.Internal;
import it.uniroma2.fase5.model.classesFromBus.metric.measurementModel.MeasurementModel;
import it.uniroma2.fase5.model.classesFromBus.metric.scale.Scale;
import lombok.*;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;


@Setter
@Getter
@AllArgsConstructor
@RequiredArgsConstructor
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper=true)
@Document(collection="ontology")
public class Ontology<X extends MeasurementModel, T extends Scale>  extends Element {

    private X measurementModel;
    private ArrayList<Internal> internals;
    private ArrayList<External>  externals;
    private T scale;
    private Unit unit;
    private MeasurementGoalBus goal;
    private ArrayList<MeasurementResult> measurements;
    private String metricatorId;



}
