package it.uniroma2.fase5.model.classesFromBus.metric.measurementModel;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.*;

import java.util.ArrayList;

/**
 * Created by MacH2o on 10/07/17.
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "compositeType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = DirectDescriptive.class, name = "DirectDescriptive"),
        @JsonSubTypes.Type(value = DirectDescriptive.class, name = "DirectPrescriptive"),
        @JsonSubTypes.Type(value = DirectPredictive.class, name = "DirectPredictive"),
        @JsonSubTypes.Type(value = IndirectDescriptive.class, name = "IndirectDescriptive"),
        @JsonSubTypes.Type(value = IndirectPrescriptive.class, name = "IndirectPrescriptive"),
        @JsonSubTypes.Type(value = IndirectPredictive.class, name = "IndirectPredictive")
})
public abstract class MeasurementModel {
    private String name;
    private String description;
    private String metricType; // Generic or Base or CoCoMo
    private String modelType; // Descriptive, Prescriptive or Predictive
    private String type; //Direct, Indirect


}
