package it.uniroma2.fase5.model.classesFromBus.repository;

import it.uniroma2.fase5.model.classesFromBus.external.OrganizationalGoalBus;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface OrganizationalGoalRepository  extends MongoRepository<OrganizationalGoalBus, String> {

    List<OrganizationalGoalBus> findAllByTitle(String title);
}
