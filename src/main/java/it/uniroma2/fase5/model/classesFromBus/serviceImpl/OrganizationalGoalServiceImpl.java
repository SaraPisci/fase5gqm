package it.uniroma2.fase5.model.classesFromBus.serviceImpl;


import it.uniroma2.fase5.model.classesFromBus.external.OrganizationalGoalBus;
import it.uniroma2.fase5.model.classesFromBus.repository.OrganizationalGoalRepository;
import it.uniroma2.fase5.model.classesFromBus.service.OrganizationalGoalService;
import it.uniroma2.fase5.model.rest.DTOresponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrganizationalGoalServiceImpl implements OrganizationalGoalService{
    @Autowired
    OrganizationalGoalRepository organizationalGoalRepository;

    @Override
    public void createOrganizationalGoal(OrganizationalGoalBus organizationalGoal) {
        OrganizationalGoalBus oldOGoal = organizationalGoalRepository.findOne(organizationalGoal.getId());
        if (oldOGoal != null) {
            organizationalGoalRepository.delete(organizationalGoal.getId());

        }
        try {
            organizationalGoalRepository.insert(organizationalGoal);
        }
        catch(org.springframework.dao.DuplicateKeyException e){
            //d.printStackTrace();
            System.out.println("Caught Exception " + e);
        }
    }

    @Override
    public ResponseEntity<DTOresponse> getGoals() {
        List<OrganizationalGoalBus> goals = organizationalGoalRepository.findAll();
        if (goals.size() == 0){
            goals = new ArrayList<OrganizationalGoalBus>();
        }
        DTOresponse dtoresponse = new DTOresponse();
        dtoresponse.setGoals(goals);
        ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse, HttpStatus.OK);
        return response;
    }

    @Override
    public List<OrganizationalGoalBus> getAllGoals() {
        return organizationalGoalRepository.findAll();
    }
}
