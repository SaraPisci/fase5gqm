package it.uniroma2.fase5.model.classesFromBus;


public enum Entity {
	MeasurementGoal ("phase2"),
	Question("phase2"),
	Metric("phase2"),
	ContextFactor("phase1"),
	Assumption("phase1"),
	InstanceProject("phase0"),
	User("phase2"),
	OrganizationalGoal("phase2"),
	CoCoMoMetric("phase2"),
	Ontology("phase2");
	
	private final String phase;
	
	private Entity(String phase){
		this.phase=phase;
	}
	
	public String getPhase(){
		return this.phase;
	}
}
