package it.uniroma2.fase5.model.classesFromBus.metric.measurementModel;


import lombok.*;

import java.util.ArrayList;

@Getter
@Setter
@AllArgsConstructor


public abstract class IndirectMeasurementModel extends MeasurementModel {
    private ArrayList<MeasurementModel> derivedMeasurementModels;
    private String expression;
    public IndirectMeasurementModel() {
        super();
    }

    public void append(MeasurementModel model) {
        derivedMeasurementModels.add(model);
    }
}
