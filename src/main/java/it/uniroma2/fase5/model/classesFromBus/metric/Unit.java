package it.uniroma2.fase5.model.classesFromBus.metric;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * Created by MacH2o on 10/07/17.
 */
@Setter
@Getter
@AllArgsConstructor
@RequiredArgsConstructor

public class Unit {
    private String name;
    private String description;
}
