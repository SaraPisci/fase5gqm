package it.uniroma2.fase5.model.classesFromBus;

public enum State {
	Approved,
	ApprovedInternalApprobation,
	Created,
	Rejected,
	External,
	Suspended,
	OnUpdate,
	OnUpdateWaitingQuestions,
	OnUpdateQuestionerEndpoint,
	OnUpdateInternalRefinement,
	Pending,
	APPROVED;
	
}
