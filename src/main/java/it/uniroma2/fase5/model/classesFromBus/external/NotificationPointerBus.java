package it.uniroma2.fase5.model.classesFromBus.external;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationPointerBus extends PointerBus{

	public NotificationPointerBus() {
		super();
	}
	
	public String operation;
	public String phase;

}
