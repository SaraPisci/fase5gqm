package it.uniroma2.fase5.model.classesFromBus.external;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.uniroma2.fase5.model.classesFromBus.Element;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


import java.util.Map;

@Data
@EqualsAndHashCode(callSuper=true)
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class InstanceProject extends Element {

	private String title;
	private String description;
	private String status;
	private String iteration;
	private String projectManagerId;
	private String rootElementId;

	public InstanceProject() {
		super();
	}

	public InstanceProject(String id) {
		super(id);
	}


	@JsonProperty("projectManager")
	public void setFunction(Map<String, Object> function) {
		projectManagerId = ((Integer) function.get("id")).toString();
	}

}
