package it.uniroma2.fase5.model.classesFromBus.metric.attribute;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@RequiredArgsConstructor
public class Attribute<T>{
    private String name;
    private String description;
    private String type; //Internal or External
    private T entity;
}
