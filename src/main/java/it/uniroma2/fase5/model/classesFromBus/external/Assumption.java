package it.uniroma2.fase5.model.classesFromBus.external;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.uniroma2.fase5.model.classesFromBus.Element;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
@JsonIgnoreProperties(ignoreUnknown = true)

public class Assumption extends Element {

	private String title;
	private String description;

	@JsonProperty("criticalValue")
	private String criticallyValue;

	private String validationState;
	private String invalidationReason;

	@JsonProperty("projectId")
	private String instanceProjectId;
	
	//creatorId e creationDate is in Element
	
}