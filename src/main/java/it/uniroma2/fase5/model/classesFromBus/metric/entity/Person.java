package it.uniroma2.fase5.model.classesFromBus.metric.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by MacH2o on 11/07/17.
 */
@Getter
@Setter
@AllArgsConstructor
public class Person extends GenericEntity {
    String surname;

    public Person() {
        super();
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
