package it.uniroma2.fase5.model.classesFromBus.repository;

import it.uniroma2.fase5.model.classesFromBus.external.PointerBus;
import it.uniroma2.fase5.model.classesFromBus.measurementGoal.MeasurementGoalBus;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MeasurementGoalBusRepository extends MongoRepository<MeasurementGoalBus, String>{
    MeasurementGoalBus findAllByOrganizationalGoalId_InstanceContaining(String instance);


}
