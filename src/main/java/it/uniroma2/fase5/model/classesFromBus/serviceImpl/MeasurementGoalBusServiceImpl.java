package it.uniroma2.fase5.model.classesFromBus.serviceImpl;

import com.mongodb.DuplicateKeyException;
import it.uniroma2.fase5.model.classesFromBus.measurementGoal.MeasurementGoalBus;
import it.uniroma2.fase5.model.classesFromBus.repository.MeasurementGoalBusRepository;
import it.uniroma2.fase5.model.classesFromBus.service.MeasurementGoalBusService;
import it.uniroma2.fase5.model.rest.DTOresponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("MeasurementGoalBusService")
public class MeasurementGoalBusServiceImpl implements MeasurementGoalBusService {
    @Autowired
    MeasurementGoalBusRepository measurementGoalBusRepository;
    @Override
    public void createMeasurementGoal(MeasurementGoalBus measurementGoalBus) {
        MeasurementGoalBus oldMGoal = measurementGoalBusRepository.findOne(measurementGoalBus.getId());
        if (oldMGoal != null) {
            measurementGoalBusRepository.delete(measurementGoalBus.getId());

        }
        try {
            measurementGoalBusRepository.insert(measurementGoalBus);
        }
        catch(org.springframework.dao.DuplicateKeyException e){
            //d.printStackTrace();
            System.out.println("Caught Exception " + e);
        }
    }

    @Override
    public ResponseEntity<DTOresponse> getMeasurementGoalByOrgGoalId(String title) {
        System.out.println("title = " + title);
        System.out.println("Query assurda " + measurementGoalBusRepository.findAllByOrganizationalGoalId_InstanceContaining(title));
        MeasurementGoalBus measurementGoalBus = measurementGoalBusRepository.findAllByOrganizationalGoalId_InstanceContaining(title);
        System.out.println("measurementGoalBus = " + measurementGoalBus);

        List<MeasurementGoalBus> measurementGoalBusArray = new ArrayList<MeasurementGoalBus>();
        measurementGoalBusArray.add(measurementGoalBus);
        DTOresponse dtoresponse = new DTOresponse();
        dtoresponse.setMeasurementGoals(measurementGoalBusArray);
        ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse, HttpStatus.OK);
        return  response;
    }

    @Override
    public ResponseEntity<DTOresponse> getAllMeasurementGoals() {
        List<MeasurementGoalBus> measurementGoalBusList = measurementGoalBusRepository.findAll();
        DTOresponse dtoresponse = new DTOresponse();
        dtoresponse.setMeasurementGoals(measurementGoalBusList);
        ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse, HttpStatus.OK);
        return  response;
    }
}
