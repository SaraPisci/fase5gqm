package it.uniroma2.fase5.model.classesFromBus.service;

import it.uniroma2.fase5.model.classesFromBus.metric.Ontology;
import it.uniroma2.fase5.model.classesFromBus.repository.OntologyBusRepository;
import it.uniroma2.fase5.model.rest.DTOresponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

public interface OntologyBusService {
    void createOntology(Ontology ontology);

    ResponseEntity<DTOresponse> getAllOntologies();

    boolean findIfExists(String id);
}
