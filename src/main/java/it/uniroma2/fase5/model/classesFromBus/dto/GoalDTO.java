package it.uniroma2.fase5.model.classesFromBus.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public abstract class GoalDTO extends DTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
}	
	
