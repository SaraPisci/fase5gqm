package it.uniroma2.fase5.model.classesFromBus.metric.scale;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by MacH2o on 12/07/17.
 */
@Getter
@Setter
@AllArgsConstructor
@JsonTypeName("Indirect")
public class Interval extends Ordinal {
    private String range;

    public Interval() {
        super();
    }
}
