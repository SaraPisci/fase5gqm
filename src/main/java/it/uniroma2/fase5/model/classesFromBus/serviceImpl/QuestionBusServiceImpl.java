package it.uniroma2.fase5.model.classesFromBus.serviceImpl;

import it.uniroma2.fase5.model.classesFromBus.metric.Ontology;
import it.uniroma2.fase5.model.classesFromBus.question.QuestionBus;
import it.uniroma2.fase5.model.classesFromBus.repository.OntologyBusRepository;
import it.uniroma2.fase5.model.classesFromBus.repository.QuestionBusRepository;
import it.uniroma2.fase5.model.classesFromBus.service.QuestionBusService;
import it.uniroma2.fase5.model.rest.DTOresponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionBusServiceImpl implements QuestionBusService{
    @Autowired
    QuestionBusRepository questionBusRepository;

    @Override
    public void createQuestion(QuestionBus question) {

        QuestionBus oldQuestion = questionBusRepository.findOne(question.getId());
        if (oldQuestion != null) {
            questionBusRepository.delete(question.getId());

        }
        try {
            questionBusRepository.insert(question);
        }
        catch(org.springframework.dao.DuplicateKeyException e){
            //d.printStackTrace();
            System.out.println("Caught Exception " + e);
        }
    }

    @Override
    public ResponseEntity<DTOresponse> getAllQuestions() {
        List<QuestionBus> questionBusList = questionBusRepository.findAll();
        DTOresponse dtoresponse = new DTOresponse();
        dtoresponse.setQuestions(questionBusList);
        ResponseEntity<DTOresponse> response = new ResponseEntity<DTOresponse>(dtoresponse, HttpStatus.OK);
        return  response;
    }
}
