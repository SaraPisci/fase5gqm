package it.uniroma2.fase5.model.classesFromBus.external;

import lombok.AllArgsConstructor;
import lombok.Data;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@Document
public class PointerBus {

	public String objIdLocalToPhase;
	public String typeObj;
	public String instance;

	
	public String busVersion;
	@JsonProperty("tags")
	public List<KeyValue> busTags;


	
	public PointerBus(){
		this.busTags = new ArrayList<KeyValue>();
		this.busVersion= "";
		this.instance= "";
		this.objIdLocalToPhase= "";
		this.typeObj = "";
	}


	public String getTypeObj() {
		return typeObj;
	}

	public String getInstance() {
		return instance;
	}

}