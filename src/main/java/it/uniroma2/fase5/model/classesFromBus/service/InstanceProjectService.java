package it.uniroma2.fase5.model.classesFromBus.service;

import it.uniroma2.fase5.model.classesFromBus.external.InstanceProject;
import it.uniroma2.fase5.model.rest.DTOresponse;
import org.springframework.http.ResponseEntity;

public interface InstanceProjectService {
    void createInstanceProject(InstanceProject instanceProject);
    ResponseEntity<DTOresponse> getProject();
    InstanceProject getProjectById(String id);
}
