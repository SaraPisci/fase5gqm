package it.uniroma2.fase5.model.classesFromBus.metric;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by MacH2o on 10/07/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MeasurementResult {
    private String value;
    private String timestamp;

}
