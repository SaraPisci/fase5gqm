package it.uniroma2.fase5.model.classesFromBus.serviceImpl;

import it.uniroma2.fase5.model.classesFromBus.external.InstanceProject;
import it.uniroma2.fase5.model.classesFromBus.external.StrategyBus;
import it.uniroma2.fase5.model.classesFromBus.repository.StrategyBusRepository;
import it.uniroma2.fase5.model.classesFromBus.service.StrategyBusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StrategyBusServiceImpl implements StrategyBusService{
    @Autowired
    StrategyBusRepository strategyBusRepository;
    @Override
    public void createStrategy(StrategyBus strategyBus) {

        StrategyBus oldStrategyBus = strategyBusRepository.findOne(String.valueOf(strategyBus.getId()));
        System.out.println("oldMGoal:   " + oldStrategyBus);
        if (oldStrategyBus != null) {
            strategyBusRepository.delete(String.valueOf(strategyBus.getId()));

        }
        try {
            strategyBusRepository.insert(strategyBus);
        }
        catch(org.springframework.dao.DuplicateKeyException e){
            //d.printStackTrace();
            System.out.println("Caught Exception " + e);
        }
    }

    @Override
    public StrategyBus getStrategyById(Long strategyId) {
        return strategyBusRepository.findById(strategyId);
    }
}
