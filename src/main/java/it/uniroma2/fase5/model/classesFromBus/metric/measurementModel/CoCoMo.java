package it.uniroma2.fase5.model.classesFromBus.metric.measurementModel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by MacH2o on 21/07/17.
 */
@Getter
@Setter

public class CoCoMo extends MeasurementModel {

    public CoCoMo() {
        super();
    }
}
