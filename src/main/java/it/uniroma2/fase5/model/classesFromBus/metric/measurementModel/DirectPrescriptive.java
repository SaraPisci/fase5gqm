package it.uniroma2.fase5.model.classesFromBus.metric.measurementModel;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by MacH2o on 12/07/17.
 */
@Getter
@Setter
@JsonTypeName("DirectPrescriptive")
public class DirectPrescriptive extends DirectMeasurementModel {
    private String baseMeas;
    private String expected;

    public DirectPrescriptive() {
        super();

    }

    public DirectPrescriptive(String baseMeas, String expected) {
        super();
        this.baseMeas = baseMeas;
        this.expected = expected;
    }
}
