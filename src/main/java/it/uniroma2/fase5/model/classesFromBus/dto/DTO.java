package it.uniroma2.fase5.model.classesFromBus.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class DTO implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = -5876831761186189543L;

	/**
	 * 
	 */
	
	
	public MetadataDTO metadata;

	
	public DTO(){
		this.setMetadata(new MetadataDTO());
	}
	
	

	public MetadataDTO getMetadata() {
		return metadata;
	}
}
