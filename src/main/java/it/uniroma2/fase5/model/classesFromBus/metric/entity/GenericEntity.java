package it.uniroma2.fase5.model.classesFromBus.metric.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * Created by MacH2o on 11/07/17.
 */
@Setter
@Getter
@AllArgsConstructor
@RequiredArgsConstructor
public abstract class GenericEntity {
    String name;
    String description;
    private String type;

}
