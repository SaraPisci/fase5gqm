package it.uniroma2.fase5.model.classesFromBus.service;

import it.uniroma2.fase5.model.classesFromBus.measurementGoal.MeasurementGoalBus;
import it.uniroma2.fase5.model.rest.DTOresponse;
import org.springframework.http.ResponseEntity;

public interface MeasurementGoalBusService {
    void createMeasurementGoal(MeasurementGoalBus measurementGoalBus);

    ResponseEntity<DTOresponse> getMeasurementGoalByOrgGoalId(String title);

    ResponseEntity<DTOresponse> getAllMeasurementGoals();
}
