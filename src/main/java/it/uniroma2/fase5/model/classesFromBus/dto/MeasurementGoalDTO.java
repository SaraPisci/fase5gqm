package it.uniroma2.fase5.model.classesFromBus.dto;


import java.util.List;


import it.uniroma2.fase5.model.classesFromBus.external.PointerBus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper=true)
@ToString(callSuper=true)
public class MeasurementGoalDTO extends GoalDTO{
	/**
	 *  */
	private static final long serialVersionUID = 1L;
	public String object;
	public String viewPoint;
	public String qualityFocus;
	public String purpose;
	public PointerBus OrganizationalGoalId;
	public List<PointerBus> metrics;
	public List<PointerBus> questions;
	public String metricatorId;
	public List<String> questionersId;
	public List<PointerBus> contextFactors;
	public List<PointerBus> assumptions;
	public InterpretationModelDTO interpretationModel;
	
}

