package it.uniroma2.fase5.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "reportFase6")
public class SendingOutComesFase6 {
@Id
String fileId;

String json;

public SendingOutComesFase6() {
	super();
}
public SendingOutComesFase6(String fileId, String json) {
	this.fileId = fileId;
	this.json = json;
}
public String getFileId() {
	return fileId;
}
public void setFileId(String fileId) {
	this.fileId = fileId;
}
public String getjson() {
	return json;
}
public void setjson(String json) {
	this.json = json;
}

}
