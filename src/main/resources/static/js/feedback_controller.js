(function() {
	var feedCtrl = angular.module('feedCtrl', []);

feedCtrl.directive('productReadfeedback', function($rootScope, $http, $window){
	return {
		restrict: 'E',
		templateUrl: '../product-readfeedback.html',
		controller: function(){
			$(document).ready(function() {
				console.log("ready");						    
			});	
		}
	}
});

feedCtrl.controller('readFeedbackCtrl', function ($scope, $http, $rootScope, $window) {
	console.log("readFeedbackCtrl");
	
	$scope.extendPanels = function(event)
	{
			//Toggle di High, Medium e Low
			 console.log("Toggle di high, medium e low");
			 var id = $(event.target).attr("id");
			 console.log("id per estensione: "+id);
			 if(id != undefined)
			 {
				 var lastChar = id.substr(id.length - 1);
				 var headStr = id.substr(0,id.length-1);
				 console.log("headStr: "+headStr);
				 if(headStr == "headingOne1")
				 {
					 $('#collapseOne1'+lastChar).toggle('slow');
				 }
				 else if(headStr == "headingTwo1")
				 {
					 $('#collapseTwo1'+lastChar).toggle('slow');
				 }
				 else if(headStr == "headingThree1")
				 {
					 $('#collapseThree1'+lastChar).toggle('slow');
				 }
			 }
			 else console.log("Clicca bene!");
	}
	
	$scope.deleteRetRep = function(event)
	{
		 //Eliminazione feedback cliccando sulla x.
			/*var id= event.target.id;
			var lastChar = id.substr(id.length - 1);*/
			var lastChar = event;
			console.log($rootScope.arrComplete[lastChar]);
			var r = confirm("Sicuro di voler eliminare il feedback del "+$rootScope.arrComplete[lastChar].lastModified +" ?");
			if (r == true) {
				 $http({
				        url: 'http://localhost:8082/deleteRetrospectiveReport/',
				        method: "POST",
				        data: { 'retrospectiveReportId' : $rootScope.arrComplete[lastChar].retrospectiveReportId }
				    })
				    .then(function(response) {
				            console.log("Success!");
				            $window.location.reload();
				    }, 
				    function(response) { // optional
				            console.log("Failed!");
				    });
		    }
			else {
		    	console.log("NON hai eliminato "+$rootScope.arrComplete[lastChar].retrospectiveReportId);
		    }

	}
	
	//Riempie le tabelle in base alle priorità dei feedback.
	$scope.setTables = function()
	{
		for(var k=0; k<$rootScope.arrComplete.length; k++)
		{
			//elimina righe table+k
			var tab1 = "table1"+k;
			var len1 = document.getElementById("table1"+k).rows.length;
//			console.log("len1 "+len1);
			if(len1>2)
			{
				console.log("Lunghezza table1"+k+": "+len1);
				for(var i=len1-1; i>0; i--)
				{
					document.getElementById(tab1).deleteRow(i);
				}
			}
			var tab2 = "table2"+k;
			var len2 = document.getElementById("table2"+k).rows.length;
			if(len2>2)
			{
				for(var i=len2-1; i>0; i--)
				{
					document.getElementById(tab2).deleteRow(i);
				}
			}
			var tab3 = "table3"+k;
			var len3 = document.getElementById("table3"+k).rows.length;
			if(len3>2)
			{
				for(var i=len3-1; i>0; i--)
				{
					document.getElementById(tab3).deleteRow(i);
				}
			}
		}
		for(var j=0; j<$rootScope.arrComplete.length; j++)
		{
			for(var i=0; i<$rootScope.arr.length; i++)
			{
				if($rootScope.arr[i].idRetRep == $rootScope.arrComplete[j].retrospectiveReportId && $rootScope.arr[i].priority == "Alta")
				{
					var tab = "table1"+j;
					createRow($rootScope.arr[i].phase, $rootScope.arr[i].description, tab);
				}
				else if($rootScope.arr[i].idRetRep == $rootScope.arrComplete[j].retrospectiveReportId && $rootScope.arr[i].priority == "Media")
				{
					var tab = "table2"+j;
					createRow($rootScope.arr[i].phase, $rootScope.arr[i].description, tab);
				}
				else if($rootScope.arr[i].idRetRep == $rootScope.arrComplete[j].retrospectiveReportId && $rootScope.arr[i].priority == "Bassa")
				{
					var tab = "table3"+j;
					createRow($rootScope.arr[i].phase, $rootScope.arr[i].description, tab);
				}
			}
		}
	}
	
	$scope.expandDate = function(event)
	{
		console.log("Voglio espandere la data");
		var id = $(event.target).attr("id");

		$('#content'+id).toggle('slow');
	}



	$("#ViewAll").click(function(){
		//$("#contentReadFeedback").show();
        $("#NewFeedback").hide();
        $("#riepilogo").hide();
        $("#id_div").hide();
        $("#send_out").hide();
        $("#viewSendingO").hide();
    	console.log("viewallclick");

    	$("#readFeedbackContainer").show();
    	console.log("ArrComplete size ", $rootScope.arrComplete.length);
		for(var i=0; i<$rootScope.arrComplete.length; i++)
		{
			console.log("arrcomplete ", $rootScope.arrComplete[i]);
	//		console.log("Dentro il for");
			var x = $rootScope.arrComplete[i].retrospectiveReportId;
//			console.log("idRetRep: "+x);
                $('#content' + i).hide();
                $('#' + x).toggle('slow');

		}
		 
		//Riempimento table in base alle priorità. 
		$scope.setTables();
		
		//visualizzazione delle conclusioni
		var str = "Conclusion: ";
		var result = str.bold();

		$('#divConcl'+i).empty();
		
		for(var i=0; i<$rootScope.arrComplete.length; i++)
		{
	//		$('#divConcl'+i).prepend("<b>Conclusion: </b>");
			$('#conclusion'+i).val($rootScope.arrComplete[i].conclusion);
		}

	});

	$http.get("http://localhost:8082/getRetrospectiveReports/").then(function(response)
	{
		$scope.arrDim = [];
		$rootScope.arr = [];
		$scope.arrDate = [];
		$rootScope.arrComplete = [];

		//$rootScope.arrComplete = response.data.retrospectiveReports;
		console.log("resp", response.data);

		for (var i = 0; i<response.data.retrospectiveReports.length; i++)
		{
			for(var j=0; j < response.data.retrospectiveReports[i].feedbackList.length; j++)
			{
				$rootScope.arr.push(
					{
							date: response.data.retrospectiveReports[i].lastModified,
							idRetRep: response.data.retrospectiveReports[i].retrospectiveReportId,
							phase: response.data.retrospectiveReports[i].feedbackList[j].phase,
							description: response.data.retrospectiveReports[i].feedbackList[j].description,
							priority: response.data.retrospectiveReports[i].feedbackList[j].priority
					}
				);
			}
			$rootScope.arrComplete.push(response.data.retrospectiveReports[i]);
			$scope.arrDim.push(response.data.retrospectiveReports[i].feedbackList.length);
		}
		
		for(var i=0; i<$rootScope.arrComplete.length; i++)
		{
			console.log("Id: "+$rootScope.arrComplete[i].retrospectiveReportId);
			console.log("Version: "+$rootScope.arrComplete[i].version);
		}
		
			//Creazione array contenente le date di tutti i feedback
		var i=0;
		var j=0;
		while(i<$rootScope.arr.length && j<$scope.arrDim.length)
		{
			j++;
			$scope.arrDate.push($rootScope.arr[i].date);
			i += $scope.arrDim[j];
		}
	});	
});	

//Aggiunge una riga alla tabella 'tab' contenente i valori c1 e c2.
function createRow(c1, c2, tab)
{
	var table = document.getElementById(tab);
	var row = table.insertRow(1);
	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	cell1.innerHTML = c1;
	cell2.innerHTML = c2;
}

})();	