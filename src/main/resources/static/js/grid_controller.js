(function () {

    var app = angular.module('MyApp', ['feedCtrl', 'MyAppFeed', 'controllerBus', 'diagCtrl', 'MyAppSending', 'repCtrl']);
//	app.run(function ($rootScope, $http) {
//
////		$http.get("http://localhost:8080/getProject/").then(function (response) {
////			$rootScope.myProject = response.data.projectId;
////		});
//	});
//	app.controller('ProjectCtrl', function ($scope, $http, $rootScope) {
//
//		$rootScope.selectProject = function (list, projId) {
//			console.log("projId: " + projId);
//			$rootScope.myGoal = list;
//			localStorage["ProjectID"] = projId;
//			if ($scope.showDiv == true) {
//				$rootScope.showDiv = !$rootScope.showDiv;
//			}
//
//			$rootScope.selected_grid = true;
//			$http.get("http://localhost:8080/getStatusByProjectId/"+projId).then(function (response) {
//				$rootScope.myStatus = response.data.status;
//
//			});
//
//		}
//
//		$scope.myProjectImport = function () {
//
//			if ($scope.hideRow == true) {
//				$scope.showRow = !$scope.showRow;
//				$scope.hideRow = !$scope.hideRow;
//			} else if ($scope.showRow == true && $scope.hideRow == false) {
//				$scope.hideRow = false;
//			}
//		}
//	});
    app.controller('GridCtrl', function ($scope, $http, $sce, $compile, $rootElement, $rootScope, MeasurementGoalService) {


        console.log("Secondo2");
        var mg = [];
        $rootScope.expandGoal = function (id, x) {
            var ul = document.getElementById(id);

            var temp = ul.getElementsByTagName("li");
            var temp2 = ul.getElementsByTagName("ul");
            for (var i = 0; ul.children[i] != null; i++) {

                for (var i = 0; ul.children[i] != null; i++) {
                    if (ul.children[i] != null) {

                        if (ul.children[i].style.display != "none")
                            ul.children[i].style.display = "none";
                        else
                            ul.children[i].style.display = "block";
                    }
                }
            }
        };
        $rootScope.expandStrategy = function (id, x) {
            var ul = document.getElementById(id);
            var temp = ul.getElementsByTagName("li");
            var temp2 = ul.getElementsByTagName("ul");
            console.log("strategy count:" + temp.length + "," + temp2.length + "," + ul.length);
            for (var i = 0; i < (temp.length + temp2.length); i++) {
                if (ul.children[i] != null) {

                    if (ul.children[i].style.display != "none") {
                        ul.children[i].style.display = "none";
                    } else {
                        ul.children[i].style.display = "block";
                    }
                }
            }
            console.log("strategy count:" + temp.length + "," + temp2.length);
        };

        $scope.append_metrics = function (parentData, parentId) {

            if (parentData == null)
                return null;

            var mtrx = {};

            for (i = 0; i < parentData.length; i++) {
                mtrx = parentData[i];
                var mtr_html = '<div><div class="panel">'
                    + '<a class="panel-heading" role="tab" id="heading' + mtrx.id + '" onclick="changeLabel(this)" data-toggle="collapse" data-parent="#accordion' + parentId + '" href="#collapse' + mtrx.id + '" aria-expanded="true" aria-controls="collapse' + mtrx.id + '">'
                    + '<label id="label_id" href="#/fa-plus-square-o"><i class="fa fa-plus-square-o fa-2x"></i>&nbsp;&nbsp;  Metrica: ' + mtrx.measurementModel.description + '&nbsp;&nbsp;</label> </a>'
                    + '<div id="collapse' + mtrx.id + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + mtrx.id + '">'
                    + ' <div class="panel-body"><table id=' + mtrx.id + ' class="table table-striped">'
                    + '	<tr><th scope="row">DESCRIPTION:</th><td>' + mtrx.measurementModel.description + '</td></tr>'
                    + '	<tr><th scope="row">MEASUREMENT MODEL TYPE:</th><td>' + mtrx.measurementModel.modelType + '</td></tr>'
                    + '	<tr><th scope="row">STATE:</th><td>' + mtrx.state + '</td></tr>'
                    + '	<tr><th scope="row">SCALE TYPE:</th><td>' + mtrx.scale.type + '</td></tr>'
                    + '	<tr><th scope="row">CREATION DATE:</th><td>' + mtrx.creationDate + '</td></tr>'
                    + '	<tr><th scope="row">METRIC UNIT:</th><td>' + mtrx.unit.description + '</li></td></tr>'
                mtr_html += '</ul></td></tr>'
                    + '</table></div></div></div></div>';

                var linkingFunction = $compile(mtr_html);
                var elem = linkingFunction($scope);
                $('#collapse' + parentId).append(elem);

            }
            //console.log("disableImportGrid prima dell'if= " + $scope.disableImportGrid);
            if ($scope.disableImportGrid == false)
                $scope.disableImportGrid = !$scope.disableImportGrid;
            //console.log("disableImportGrid dopo if= " + $scope.disableImportGrid);
        };

        $scope.append_questions = function (parentData, parentId) {
            console.log("I'M IN APPEND QUESTIONS", parentData, parentId);

            if (parentData == null)
                return null;

            var quest = {};

            for (i = 0; i < parentData.length; i++) {
                quest = parentData[i];


                var quest_html = '<div><div class="panel">'
                    + '<a class="panel-heading" role="tab" id="heading' + quest.id + '" onclick="changeLabel(this)" data-toggle="collapse" data-parent="#collapse' + parentId + '" href="#collapse' + quest.id + '" aria-expanded="true" aria-controls="collapse' + quest.id + '">'
                    + '<label id="label_id" href="#/fa-plus-square-o"><i class="fa fa-plus-square-o fa-2x"></i>&nbsp;&nbsp;  Question: ' + quest.description + '&nbsp;&nbsp;</label></a>'
                    + '<div id="collapse' + quest.id + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + quest.id + '">'
                    + ' <div class="panel-body"><table id=' + quest.id + ' class="table table-striped">'
                    + '	<tr><th scope="row">DESCRIPTION:</th><td>' + quest.description + '</td></tr>'
                    + '	<tr><th scope="row">FOCUS:</th><td>' + quest.focus + '</td></tr>'
                    + '	<tr><th scope="row">CREATION DATE:</th><td>' + quest.creationDate + '</td></tr>'
                    + '	<tr><th scope="row">LAST MODIFIED:</th><td>' + quest.lastVersionDate + '</td></tr>'
                    + '	<tr><th scope="row">SUBJECT:</th><td>' + quest.subject + '</td></tr>'
                    + '	<tr><th scope="row">STATE:</th><td>' + quest.state + '</td></tr>'
                    + '</table></div></div></div></div>';

                var linkingFunction = $compile(quest_html);
                var elem = linkingFunction($scope);
                $('#collapse' + parentId).append(elem);

            }
            return null;
        };
        $scope.append_measurements = function (parentData, parentId) {
            if (parentData.object == "Aule") {
                console.log("I'M IN APPEND MEASUREMENT", parentData);
            }

            // var msgoals_id=[];
            if (parentData == null)
                return null;

            var mg = parentData;


            var msGoal_html = '<div><div class="panel">'
                + '<a class="panel-heading" role="tab" id="heading' + mg.id + '" onclick="changeLabel(this)" data-toggle="collapse" data-parent="#collapse' + parentId + '" href="#collapse' + mg.id + '" aria-expanded="true" aria-controls="collapse' + mg.id + '">'
                + '<label id="label_id" href="#/fa-plus-square-o"><i class="fa fa-plus-square-o fa-2x"></i>&nbsp;&nbsp;  Measurement Goal: ' + mg.object + '&nbsp;&nbsp;</label></a>'
                + '<div id="collapse' + mg.id + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + mg.id + '">'
                + ' <div class="panel-body"><table id=' + mg.id + ' class="table table-striped">'
                + '	<tr><th scope="row">OBJECT:</th><td>' + mg.object + '</td></tr>'
                + '	<tr><th scope="row">PURPOSE:</th><td>' + mg.purpose + '</td></tr>'
                + '	<tr><th scope="row">QUALITY FOCUS:</th><td>' + mg.qualityFocus + '</td></tr>'
                + '	<tr><th scope="row">CREATION DATE:</th><td>' + mg.creationDate + '</td></tr>'
                + '	<tr><th scope="row">LAST VERSION DATE:</th><td>' + mg.lastVersionDate + '</td></tr>';

                if (mg.interpretationModel.functionJavascript != ""){
                    msGoal_html += '<tr><th scope="row">INTERPRETATIONAL MODEL:</th><td>' + mg.interpretationModel.functionJavascript + '</td></tr>'
                };
                msGoal_html +='<tr><th scope="row">STATE:</th><td>' + mg.state + '</td></tr>';
/*            if (mg.contextFactors != null) {
                msGoal_html += '	<tr><th scope="row">' + ' 		CONTEXT:</th><td><ul>';
                for (var j = 0; j < mg.contextFactors.length; j++)
                    msGoal_html += '<li>' + mg.contextFactors[j] + '</li>';
                msGoal_html += '</ul></td></tr>';
            } */
            msGoal_html += '</table></div></div></div></div>';
            // ng-click="append_subGoal("'+s.goalRef+'","'+s.strategyId+'")">expand
            // subgoal</p>';
            var linkingFunction = $compile(msGoal_html);
            var elem = linkingFunction($scope);
            $('#collapse' + parentId).append(elem);

            var questionList = [];
            var sQuestion = 0;
            console.log("questions", mg.questions, $rootScope.questionsList);
            for (var i = 0; i < mg.questions.length; i++) {
                for (var j = 0; j < $rootScope.questionsList.length; j++) {
                    if ($rootScope.questionsList[j].id == mg.questions[i].instance) {
                        questionList[sQuestion] = $rootScope.questionsList[j];
                        sQuestion++;
                    }
                }
            }
            $scope.append_questions(questionList, mg.id);

            console.log("ONTOLOGYROOTLIST", $rootScope.ontologiesList);
            var ontologyList = [];
            for (var i = 0; i < mg.metrics.length; i++) {
                for (var j = 0; j < $rootScope.ontologiesList.length; j++) {
                    if ($rootScope.ontologiesList[j].id == mg.metrics[i].instance) {
                        ontologyList.push($rootScope.ontologiesList[j]);
                    }
                }
            }

            console.log("OntologyList ", ontologyList);
            $scope.append_metrics(ontologyList, mg.id);

            // msgoals_id.push(mg);
  /*          for (var k = 0; k < parentData.length; k++) {
                mg = parentData[k];
                var questionList = [];
                var sQuestion = 0;
                for (var i = 0; i < mg.questions.length; i++) {
                    for (var j = 0; j < $rootScope.questionsList.length; j++) {
                        if ($rootScope.questionsList[j].id === mg.questions[i].instance) {
                            questionList[sQuestion] = $rootScope.questionsList[j];
                            sQuestion++;
                        }
                    }
                }
                //console.log("QuestionList ", questionList);
                $scope.append_questions(questionList, mg.id);
                var ontologyList = [];
                var sOntology = 0;
                for (var i = 0; i < mg.metrics.length; i++) {
                    for (var j = 0; j < $rootScope.ontologiesList.length; j++) {
                        if ($rootScope.ontologiesList[j].id === mg.metrics[i].instance) {
                            ontologyList[sOntology] = $rootScope.ontologiesList[j];
                            sOntology++;
                        }
                    }
                }
                //console.log("OntologyList ", ontologyList)
                $scope.append_metrics(ontologyList, mg.id);
            } */
        };

        $scope.append_subGoal = function (parentData, parentId) {
            // var subgoals_id=[];
            console.log("Parent SUBGOAL", parentData);
            if (parentData == null)
                return null;

            var sbgoal = {};

            for (i = 0; i < parentData.length; i++) {
                sbgoal = parentData[i];
                console.log("SUBGOAL", sbgoal);
                var x = JSON.stringify(parentData[i], null, 2);
                localStorage.setItem("subgoal" + i, x);

                var subGoal_html = '<div><div class="panel">'
                    + ' <a class="panel-heading" role="tab" id="heading' + sbgoal.goalId + '" onclick="changeLabel(this)" data-toggle="collapse" data-parent="#collapse' + parentId + '" href="#collapse' + sbgoal.id + '" aria-expanded="true" aria-controls="collapse' + sbgoal.id + '">'
                    + '<label id="label_id" href="#/fa-plus-square-o"><i class="fa fa-plus-square-o fa-2x"></i>&nbsp;&nbsp;  Goal: ' + sbgoal.title + '&nbsp;&nbsp;</label><!--<button id="btn' + sbgoal.id + '" class="btn btn-default btn-sm" data-toggle="modal"  data-target=".bs-example-modal-lg" ng-click=" plotDiagram(myGoal,' + sbgoal.id + ')" ><font>Unchecked</font></button>--></a>'
                    + '<div id="collapse' + sbgoal.id + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + sbgoal.goalId + '">'
                    + ' <div class="panel-body"><table id=' + sbgoal.id + ' class="table table-striped">'
                    + '	<tr><th scope="row">DESCRIPTION:</th><td>' + sbgoal.information + '</td></tr>'
                    + '	<tr><th scope="row">TIMEFRAME:</th><td>' + sbgoal.timeFrame + '</td></tr>'
                    + '	<tr><th scope="row">OBJECT: </th><td>' + sbgoal.object + '</td></tr>'
                    + '	<tr><th scope="row"><p>MAGNITUDE:</th><td>' + sbgoal.magnitude + '</td></tr>'
                    + '	<tr><th scope="row">FOCUS:</th><td>' + sbgoal.focus + '</td></tr>'
                    + '	<tr><th scope="row">ORGANIZATIONAL SCOPE:</th><td>' + sbgoal.organizationalScopeIds + '</td></tr>'
                    // +'<p
                    // ng-click="append_strategy('+sbgoal.strategyRef+','+sbgoal.goalId+')">expand
                    // strategy</p>'
                    // +'<p
                    // ng-click="append_measurements('+sbgoal.measurementRef+','+sbgoal.goalId+')">expand
                    // measure</p>'
                    + '</div></div>';

                var linkingFunction = $compile(subGoal_html);
                var elem = linkingFunction($scope);
                $('#collapse' + parentId).append(elem);
                $scope.setValidation(sbgoal.goalId);

                // subgoals_id.push(sbgoal);
            }
            for (var i = 0; i < parentData.length; i++) {
                sbgoal = parentData[i];
                $scope.append_strategy(sbgoal.strategyRef, sbgoal.goalId);
                $scope.append_measurements(sbgoal.measurementRef, sbgoal.goalId);
            }
        };

        $scope.append_strategy = function (parentData, parentId) {
            //console.log("I'M IN APPEND STRATEGY");
            if (parentData == null)
                return null;
            var s = {};
            for (i = 0; i < parentData.length; i++) {
                //console.log("I'M IN APPEND STRATEGY LENGTH ------>" + parentData.length);
                s = parentData[i];
                //console.log("STRATEGY", s);

                var stg_html = '<div><div class="panel">'
                    + '<a class="panel-heading" role="tab" id="heading' + s.id + '" onclick="changeLabel(this)" data-toggle="collapse" data-parent="#collapse' + parentId + '" href="#collapse' + s.id + '" aria-expanded="true" aria-controls="collapse' + s.id + '">'
                    + '<label id="label_id" href="#/fa-plus-square-o"><i id="label_id" class="fa fa-plus-square-o fa-2x" "></i>&nbsp;&nbsp;Strategia: ' + s.title + '&nbsp;&nbsp;</label></a>'
                    + '<div id="collapse' + s.id + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + s.id + '">'
                    + ' <div class="panel-body"><table id=' + s.id + ' class="table table-striped">'
                    + '	<tr><th scope="row">TITLE:</th><td>' + s.title + '</td></tr>'
                    + '	<tr><th scope="row">DESCRIPTION:</th><td>' + s.description + '</td></tr>'
/*
                if (s.linkedContextFactors != null) {
                    stg_html += '	<tr><th scope="row">'
                        + ' 		CONTEXT:</th><td><ul>';
                    for (var j = 0; j < s.linkedContextFactors.length; j++)
                        stg_html += '<li>' + s.linkedContextFactors[j].title
                            + '</li>';
                    stg_html += '</ul></td></tr>';
                }
                if (s.linkedAssumptions != null) {
                    stg_html += '<tr><th scope="row">'
                        + ' 		ASSUMPTION: </th><td><ul>';
                    for (var j = 0; j < s.linkedAssumptions.length; j++)
                        stg_html += '<li>' + s.linkedAssumptions[j].title
                            + '</li>';
                    stg_html += '</ul></td></tr>';
                } */
                stg_html += '</table></div></div></div></div>';
                // +'<p
                // ng-click="append_subGoal("'+s.goalRef+'","'+s.strategyId+'")">expand
                // subgoal</p>';

                var linkingFunction = $compile(stg_html);
                var elem = linkingFunction($scope);
                $('#collapse' + parentId).append(elem);

                // strategies_id.push(s);
            }
            //  for (var i = 0; i < parentData.length; i++) {
            //    s = parentData[i];
            //  $scope.append_subGoal(s.goalRef, s.strategyId);
            //}
        };

        $scope.append_goal = function (parentId, parentData) {
            console.log("APPEND_GOAL ", parentId, parentData);

            if (parentData == null) {
                console.log("parentData == null")
                return null;
            }

            var measurementGoalLinked = {};

            for(var t=0; t<parentData.length; t++){
                for (var m=0; m<$rootScope.measurementGoalList.length; m++){
                    if ($rootScope.measurementGoalList[m].organizationalGoalId.instance == parentData[t].title){
                        measurementGoalLinked[parentData[t].id] = $rootScope.measurementGoalList[m];
                    }
                }
            }

            console.log("MEASGOALLINK", measurementGoalLinked);


            var goal = {};

            for (var i = 0; i < parentData.length; i++) {
                goal = parentData[i];
                var goal_html = '<div><div class="panel">'
                    + ' <a class="panel-heading" role="tab" id="heading' + goal.id + '" onclick="changeLabel(this)" data-toggle="collapse" data-parent="#accordion" href="#collapse' + goal.id + '" aria-expanded="true" aria-controls="collapse' + goal.id + '">'
                    + '<label id="label_id" href="#/fa-plus-square-o"><i  class="fa fa-plus-square-o fa-2x" ></i>&nbsp;&nbsp;  Goal: ' + goal.title + ' &nbsp;&nbsp;</label><!--<button id="btn' + goal.id + '" class=" btn btn-default btn-sm" data-toggle="modal"   data-target=".bs-example-modal-lg" ng-click=" plotDiagram(myGoal,' + goal.id + ' )" ><font>Unchecked</font></button>--></a>'
                    + '<div id="collapse' + goal.id + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + goal.id + '">'
                    + ' <div class="panel-body"><table id=' + goal.id + ' class="table table-striped">'
                    + '<tr ><th scope="row">NAME:</th><td>' + goal.title + '</td></tr>'
                    + '<tr ><th scope="row">INFORMATION:</th><td>' + goal.information + '</td></tr>'
                    + '<tr><th scope="row">TIMEFRAME:</th><td>' + goal.timeFrame + '</td></tr>'
                    + '<tr><th scope="row">OBJECT:</th><td>' + goal.object + '</td></tr>'
                    + '<tr><th scope="row">MAGNITUDE:</th><td>' + goal.magnitude + '</td></tr>'
                    + '<tr><th scope="row">FOCUS:</th><td>' + goal.focus + '</td></tr>'
//                    + '<tr><th scope="row">ORGANIZATIONAL SCOPE:</th><td>' + goal.organizationalScopeIds + '</td></tr>'
//                    + '<tr><th scope="row">VERSIONE:</th><td>' + goal.version + '</td></tr>'
                    + '<tr><th scope="row">STATE:</th><td>' + goal.state + '</td></tr>'
                // +'<label
                // ng-click="append_strategy('+goal.strategyRef+','+goal.goalId+')">expand
                // strategy</label>'
                // +'<label
                // ng-click="append_measurements('+goal.measurementRef+','+goal.goalId+')">expand
                // measure</label>'

                if (goal.subGoals != null) {
                    for (var w = 0; w < goal.subGoals.length; w++) {
                        goal_html += '<tr><th scope="row">SUBGOAL:</th><td>' + goal.subGoals[w].title + '</td></tr>'
                    }
                }
                goal_html +=  '</table></div></div></div></div>';

                var linkingFunction = $compile(goal_html);
                var elem = linkingFunction($scope);
                $('#' + parentId).append(elem);

                var strategyListGoalSel = [];
                var sizeStrList = 0;

                //console.log("goal: ", JSON.stringify(goal));


                if (goal.descendantStrategies != null) {
                    for (var j = 0; j < goal.descendantStrategies.length; j++) {
                        strategyListGoalSel[sizeStrList] = goal.descendantStrategies[j];
                        sizeStrList++;
                    }
                }
                if (goal.ascendantStrategy != null) {
                    strategyListGoalSel[sizeStrList] = goal.ascendantStrategy;
                    sizeStrList++;
                }

                $scope.append_strategy(strategyListGoalSel, goal.id);

                $scope.append_measurements(measurementGoalLinked[goal.id], goal.id);


                //$scope.setValidation(goal.goalId);


            }




        };
        //funzione angularJs che  mostra la griglia validata quindi con gli status prelevati con l'ultimo aggiornamento.
        $scope.setValidation = function (goalid) {
            console.log("INTO SETVALIDATION");

            var Status = $rootScope.myStatus;
            //console.log("JSON STATUS : " + JSON.stringify(Status));
            //Status = JSON.stringify(Status);
            //console.log("JSON STATUUUUS: " + Status);
            //console.log("JSON STATUUUUS goalRef: " + Status.status);
            //console.log("GOAL ID DALLO STATUS = " + Status.goalRef);
            //console.log("SECOND INTO SETVALIDATION");
            //var statusColor = {};
            console.log("lenght : " + Status.length);
            var buttonValidation = document.getElementById("btn" + goalid);
            //console.log("THIRD INTO SETVALIDATION");
            //con questo for confronto un JSON object con l'id del gol del bottone selezione
            for (var i = 0; i < Status.length; i++) {
                console.log("INTO STATUS.GOALREF.GOALID = " + Status[i].goalRef.goalId);
                //console.log("ciao");
                if (goalid == Status[i].goalRef.goalId) {
                    //if( goalid == Status.goalRef.goalId){
                    console.log("INTO STATUS.GOALREF.GOALID");
                    statusColor = Status[i].goalRef.state;
                    //statusColor = Status.status;
                    console.log("state---------->" + Status[i].goalRef.goalId);

                    console.log("colore---------->" + statusColor);
                    //confronto gli status e cambio la classe e il testo del bottone
                    if (statusColor == "accepted") { //status1 = accepted
                        console.log("INTO status1");
                        buttonValidation.className = "btn btn-succes";
                        buttonValidation.setAttribute("style", "color:#fff; background-color: #26B99A; border-color: #169F85; ");
                        buttonValidation.children[0].textContent = "Accepted";
                    }
                    else if (statusColor == "rejected") {
                        buttonValidation.className = "btn btn-danger";
                        buttonValidation.setAttribute("style", "color:#fff; background-color: #d9534f; border-color: #d43f3a;");
                        buttonValidation.children[0].textContent = "Rejected";
                    }
                    else if (statusColor == "unchecked") {
                        buttonValidation.className = "btn btn-default";
                        buttonValidation.setAttribute("style", "background-color: #fff; border-color: #ccc;");

                        buttonValidation.children[0].textContent = "Unchecked";
                    }
                }
            }
            console.log("FINE SETVALIDATION");
        };


        $scope.plotDiagram = function (listaGoal, id) {
            console.log("PLOT", listaGoal, id);
            //va gestita l'apertura o chiusura del menu
            $('#menu_toggle').click();

            Chart.defaults.global.legend = {
                enabled: false
            };

            var metricsRef = []

            var strategyRef = [];
            var sizeStrList = 0;

            for (var i = 0; i < listaGoal.length; i++) {
                var goal = listaGoal[i];

                if (goal.descendantStrategies != null) {
                    for (var i = 0; i < goal.descendantStrategies.length; i++) {
                        strategyRef[sizeStrList] = goal.descendantStrategies[i];
                        sizeStrList++;
                    }
                }
                if (goal.ascendantStrategy != null) {
                    strategyRef[sizeStrList] = goal.ascendantStrategy;
                    sizeStrList++;
                }
                //console.log("STRATEGY LIST PLOT", strategyRef);
            }


            var title;
            for (i = 0; i < listaGoal.length; i++) {
                if (listaGoal[i].id == id) {
                    title = listaGoal[i].title;
                }
            }
            console.log("TITLE ", title);
            MeasurementGoalService.getMeasurementGoalByOrgGoalId({title: title}).$promise.then(function (data) {
                console.log("Dati mg presi ", data);
                metricsRef = data.measurementGoals[0];
                // parsing per il timeframe del goal
                var timeframeGoal = parseTimeframe(listaGoal, id, metricsRef);
                console.log("timeFrameGoal: " + JSON.stringify(timeframeGoal));
                //prendo l'ascissa corretta per il graph
                var ascissa = setLabel(timeframeGoal);

            });

            var dataJson = {};
            var i, j, k, count = 0;
            var dataSet = new Array();
            var strategyRef = {};
            var metricsRef = {};
            var htmlSelMetric;
            var selectedMetric = new Array();
            var organizationaleScope = {};
            var lineChart;
            var barChart;
            var countLine = 0;
            // parsing per l'organizationalScope del goal = al titolo del grafico

            //organizationalScope = parseOrganizationalScope(listaGoal, id);

            // definizione titolo diagramma sul modal
            $('#graphTitle').html("Goal " + id);

            // parsing per il riferimento alle strategie d'interesse associate al goal selezionato
            //strategyRef = parseStrategy(listaGoal, id);
            //console.log("strategyRef: "+JSON.stringify(strategyRef));
            // parsing per il riferimento alle metriche d'interesse associate al goal selezionato
            //metricsRef = parseMetric(listaGoal, id);
            //console.log("metricsRef: "+JSON.stringify(metricsRef));
            //stampa l'errore sul modale
            if (strategyRef == null || metricsRef == null) {
                var htmlStr = '  <div id="errorId" class="x_content"><div class="bs-example" data-example-id="simple-jumbotron"><div class="jumbotron"><h1><b>Error</b></h1>'
                    + '<p>No strategy for the selected goals</p>'
                    + '</div></div></div>';

                if ($('#lineChart').length || $('#barChart').length) {
                    $("#canvasContent").prepend(htmlStr);
                    $('#lineChart').remove();
                    $('#barChart').remove();
                    $('#chooseChart').hide();

                    console.log("prima volta nell'erase " + $('#chooseChart').length);
                }
                else {
                    if ($('#errorId').length || $('#dataError').length) {
                        $('#chooseChart').hide();
                        $('#errorId').remove();
                        $("#canvasContent").prepend(htmlStr);
                    }
                    else {
                        $("#canvasContent").prepend(htmlStr);
                    }

                }
            }

            else {


                //verifico l'esistenza o meno del div dell'errore ed in caso lo cancello
                if ($('#errorId').length) {
                    $('#errorId').remove();
                    $('#chooseChart').show();
                }
                //	$http.post("http://localhost:8080/receiveRealBusMessage/", {typeObject:"validatedData"}).then(
                //	function (response) {

                if ($rootScope.myData != null) {

                    //$rootScope.myData = response.data.validateData;
                    // prelevo i dati validati dal JSON object con una get
                    dataJson = $rootScope.myData;
                    // con questi cicli confronto le metriche parsate con quelle associate nel Json Object per
                    // ottenere i corrispettivi dati
                    for (i = 0; i < strategyRef.length; i++) {
                        for (j = 0; j < metricsRef.length; j++) {
                            for (k = 0; k < dataJson.length; k++) {

                                if (dataJson[k].strategyRef != null && JSON.stringify(dataJson[k].strategyRef) == JSON.stringify(strategyRef[i])) {

                                    if (dataJson[k].metricRef != null && JSON.stringify(dataJson[k].metricRef) == JSON.stringify(metricsRef[j])) {
                                        var dataList = [];
                                        dataList = dataJson[k].data;
                                        dataSet[count] = {
                                            metric: metricsRef[j].metricId,
                                            strategy: strategyRef[i].strategyId,
                                            data: dataList
                                        };


                                        count++;
                                        if (selectedMetric.length == 0) {
                                            selectedMetric.push({
                                                id: metricsRef[j].metricId,
                                                description: metricsRef[j].description
                                            });
                                        } else {
                                            var p;
                                            for (p = 0; p < selectedMetric.length; p++) {
                                                if (metricsRef[j].metricId == selectedMetric[p].id) {
                                                    break;
                                                }

                                            }
                                            //per salvare le metriche nuove
                                            if (p == selectedMetric.length)
                                                selectedMetric.push({
                                                    id: metricsRef[j].metricId,
                                                    description: metricsRef[j].description
                                                });
                                        }
                                    } else
                                        console.log("Metric null o nessun matching");
                                } else
                                    console.log("Strategy null o nessun matching");
                            }
                        }
                    }

                    if (selectedMetric.length == 0) {
                        var htmlDataError = '  <div id="dataError" class="x_content"><div class="bs-example" data-example-id="simple-jumbotron"><div class="jumbotron"><h1><b>Error</b></h1>'
                            + '<p>No Data for the selected goals</p>'
                            + '</div></div></div>';
                        if ($('#dataError').length) {
                            $('#lineChart').remove();
                            $('#barChart').remove();
                            $('#dataError').remove();
                            $("#canvasContent").prepend(htmlDataError);
                        } else {
                            $("#canvasContent").prepend(htmlDataError);
                            $('#barChart').remove();
                            $('#lineChart').remove();
                        }

                    }
                    for (var s = 0; s < selectedMetric.length; s++) {
                        //popola i dropdown per selezionare le metriche da plottare
                        if ($('#metrica' + selectedMetric[s].id).length == 0) {
                            htmlSelMetric = '<li><a href="#" id= "metrica' + selectedMetric[s].id + '" >' + selectedMetric[s].description + '</a></li>';
                            $("#dropdownMetric").prepend(htmlSelMetric);
                        }

                        document.getElementById('metrica' + selectedMetric[s].id).onclick = (function (selMetr) {
                            return function () {
                                console.log("selMetr:" + selMetr);
                                $('#canvasContent').empty();
                                // funzione per la generazione di colori rgba in modo random
                                var dynamicColors = function () {
                                    var r = Math.floor(Math.random() * 255);
                                    var g = Math.floor(Math.random() * 255);
                                    var b = Math.floor(Math.random() * 255);
                                    return "rgba(" + r + "," + g + "," + b + "," + 0.8 + ")";
                                }

                                if ($('#lineChart').length == 0) {

                                    var canvas = '<canvas id="lineChart"></canvas>';
                                    $("#canvasContent").prepend(canvas);
                                }
                                var line = new Array();
                                var ctx = document.getElementById("lineChart").getContext('2d');
                                // definizione oggetto per il
                                // dataset per il diagramma ovvero la linea da tracciare

                                for (i = 0; i < dataSet.length; i++) {
                                    if (dataSet[i].metric == selMetr)
                                        line.push({
                                            label: "Strategy " + dataSet[i].strategy, //strategyRef[i].strategyId ,
                                            backgroundColor: dynamicColors(),
                                            borderColor: dynamicColors(),
                                            pointBorderColor: "rgba(38, 185, 154, 0.7)",
                                            pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
                                            pointHoverBackgroundColor: "#fff",
                                            pointHoverBorderColor: "rgba(220,220,220,1)",
                                            pointBorderWidth: 1,
                                            data: dataSet[i].data
                                        });
                                }

                                // creazione diagramma passando l'id del canvas, type di
                                // diagramma , dataset generato prima;
                                lineChart = new Chart(ctx, {
                                    type: 'line',

                                    data: {
                                        labels: ascissa,
                                        datasets: line
                                    }
                                });

                                if ($('#barChart').length == 0) {

                                    var canvas = '<canvas id="barChart" style="display : none"></canvas>';
                                    $("#canvasContent").prepend(canvas);

                                }


                                var context = document.getElementById("barChart").getContext('2d');

                                barChart = new Chart(context, {
                                    type: 'bar',

                                    data: {
                                        labels: ascissa,
                                        datasets: line
                                    }
                                });

                            }
                        })(selectedMetric[s].id);//fine addEventAtach
                    }//fine for  selected metric

                }   //);
            }

        }
        //funzione che mostra il bar diagram
        $('#barType').click(function () {
            $('#lineChart').hide();
            $('#barChart').show();
        });
        //funzione che mostra il line diagram
        $('#lineType').click(function () {

            $('#barChart').hide();
            $('#lineChart').show();
        });
        //funzione che gestitsce la chiusura del modale
        $('#closeDiagram').click(function () {
            $('#barChart').remove();
            $('#lineChart').remove();
            $('#errorId').remove();
            $('#dataError').remove();

            for (var s = 0; s < $('#dropdownMetric').children().length; s++) {
                //popola i dropdown per selezionare le metriche da plottare

                $('#dropdownMetric').children(s).remove();

            }

            $('#menu_toggle').click();
        });


//funzione JQuery che permette il salvataggio con una post dello status della griglia.

        $('#saveStatus').click(function () {
            var prj = localStorage.getItem("ProjectID");
            var goalid = parseTitle($('#graphTitle').html());
            var statusid = goalid;
            var status = {};
            var creationDate = getDate();
            var lastModified = getDate();

            if ($('#btn' + goalid).attr("class") == "btn btn-succes") {
                status = "accepted";
            } else if ($('#btn' + goalid).attr("class") == "btn btn-danger") {
                status = "rejected";
            } else if ($('#btn' + goalid).attr("class") == "btn btn-default") {
                status = "unchecked";
            } else
                alert("Scegliere lo status");
            console.log("status: " + status);
            var data = {};
            data = {
                statusId: statusid,
                status: status,
                creationDate: creationDate,
                lastModified: lastModified,
                goalRef: goalid,
                projectRef: prj
            };
            $http.post('http://localhost:8082/createStatus/', data).then(function (response) {
                alert("Risposta affermativa: " + JSON.stringify(data.status));
                $('#closeDiagram').click();
            }, function (response) {
                alert("Errore : " + JSON.stringify(response));
            });

        });

    }); //ADD


})();


//funzione javascript che cambia il colore del bottone di analisi real time durante la selezione nel modale
function validation(id) {
    // bottoni per la validazione
    var unchecked = document.getElementById("unchecked");
    var accepted = document.getElementById("accepted");
    var rejected = document.getElementById("rejected");

    // variabile per prendere l'Id del goal dal title del graph
    var title_change = document.getElementById("graphTitle");

    var app = {};
    app = parseTitle(title_change.innerHTML);

    var buttonValidation = document.getElementById("btn" + app);

    if (id == unchecked) {
        buttonValidation.className = "btn btn-default";
        buttonValidation.setAttribute("style", "background-color: #fff; border-color: #ccc;");
        buttonValidation.children[0].textContent = "Unchecked";

    } else if (id == accepted) {
        buttonValidation.className = "btn btn-succes";
        buttonValidation.setAttribute("style", "color:#fff; background-color: #26B99A; border-color: #169F85; ");
        buttonValidation.children[0].textContent = "Accepted";

    } else if (id == rejected) {
        buttonValidation.className = "btn btn-danger";
        buttonValidation.setAttribute("style", "color:#fff; background-color: #d9534f; border-color: #d43f3a;");
        buttonValidation.children[0].textContent = "Rejected";

    }

}

//funzione javascript che cambia il label + in - o viceversa quando si usa l'accordian
function changeLabel(elem) {

    var label = elem.getElementsByTagName("label")[0];
    if (label.href == "#/fa-minus-square-o") {
        label.href = "#/fa-plus-square-o"

        label.children[0].setAttribute("class", "fa fa-plus-square-o fa-2x");
    } else {
        label.href = "#/fa-minus-square-o";
        label.children[0].setAttribute("class", "fa fa-minus-square-o fa-2x");
    }
}

// funzione javascript per ottenere i label da associare all'asse x dei diagrammi prodotti
function setLabel(timeframe) {
    var result = new Array();
    var value_limit = parseInt(timeframe.value);
    var months = new Array(12);
    months[0] = "January";
    months[1] = "February";
    months[2] = "March";
    months[3] = "April";
    months[4] = "May";
    months[5] = "June";
    months[6] = "July";
    months[7] = "August";
    months[8] = "September";
    months[9] = "October";
    months[10] = "November";
    months[11] = "December";

    if (timeframe.label == "anno" || timeframe.label == "anni") {
        for (var i = 0; i <= value_limit; i++) {
            result.push(parseInt(timeframe.period) + i);
        }
    }
    else {
        if (timeframe.label == "mese" || timeframe.label == "mesi") {
            var j = 0;
            for (var i = 0; i < value_limit; i++) {
                result.push(months[(parseInt(timeframe.period) + i - 1) % 12]);

            }
        }

    }

    return result;
}

//funzione javascript per parsare le strategie associate alla lista di gol
function parseStrategy(lista, Id) {
    var strategyRef = {};
    var x = {};

    if (lista[0].goalId == Id) {

        if (lista[0].strategyRef != null) {

            strategyRef = lista[0].strategyRef;
            return strategyRef;
        } else
            console.log("Nessuna strategia nel goal Organizzativo");
    } else {
        for (var i = 0; i < localStorage.length; i++) {
            x = JSON.parse(localStorage.getItem("subgoal" + i));
            if (x != null && x.goalId == Id) {
                if (x.strategyRef != null) {
                    strategyRef = x.strategyRef;
                    return strategyRef;
                }

                else
                    console.log("Nessuna strategia nel subgoal in parse strategy");

            } else
                console.log("Nessun matching tra id e del sub goal in parse strategy");
        }
    }

    return strategyRef = null;
}

//funzione javascript per parsare le metriche associate alla lista di gol
function parseMetric(lista, Id) {
    var metricRef = {};
    var x = {};

    if (lista[0].goalId == Id) {
        if (lista[0].measurementRef[0] != null
            && lista[0].measurementRef[0].metricsRef != null) {
            metricRef = lista[0].measurementRef[0].metricsRef;
            return metricRef;
        } else
            console.log("Nessuna metrica di Riferimento per il Goal organizzativo in  parse metric");
    } else {
        for (var i = 0; i < localStorage.length; i++) {
            x = JSON.parse(localStorage.getItem("subgoal" + i));
            if (x != null && x.goalId == Id) {
                if (x.measurementRef != null && x.measurementRef[0] != null && x.measurementRef[0].metricsRef != null) {
                    metricRef = x.measurementRef[0].metricsRef != null
                    return metricRef;
                } else
                    console.log("Nessuna metrica di riferimento per il subgoal in parse metric");
            } else
                console.log("Nessun matching tra id e subgoal in parse metric");
        }

    }

    return metricRef = null;
}

//funzione per parsare dal JSOn della lista di goal l'organizational scope per generare il titolo del diagramma
function parseOrganizationalScope(lista, id) {
    var organizationalScope = {};
    var x = {};

    if (lista[0].goalId == id) {
        organizationalScope = lista[0].organizationalScope;
        return organizationalScope;

    } else {

        for (var i = 0; i < localStorage.length; i++) {
            x = JSON.parse(localStorage.getItem("subgoal" + i));
            if (x != null && x.goalId == id) {
                organizationalScope = x.organizationalScope;
                return organizationalScope;
            }
        }
    }
    return organizationalScope = null;
}

//funzione per parsare il titolo
function parseTitle(title) {

    var parsed = title.split(" ");

    return parsed[1];
}

//funzione per parsare il Timeframe  dalla lista del goal
function parseTimeframe(lista, id, mg) {
    console.log("PARSE TIME FRAME");
    var timeframe = {};
    var time = {};
    var start = {};
    var x = {};

    if (lista[0].id == id) {
        time = lista[0].timeFrame;
        start = mg.creationDate;
    } else {

        for (var i = 0; i < localStorage.length; i++) {
            x = JSON.parse(localStorage.getItem("subgoal" + i));
            console.log("x.goalId:" + x.id);
            if (x.id == id) {
                time = x.timeFrame;
                start = mg.creationDate;
                break;
            } else {
                time = null;
                start = null;
            }
        }
    }

    if (time != null && start != null) {
        var parsed = time.split(" ");
        var dateParsed = start.split("/")
        if (parsed[1] == "mesi" || parsed[1] == "mese") {
            timeframe = {
                value: parsed[0],
                label: parsed[1],
                period: dateParsed[1]
            };
        }
        else timeframe = {
            value: parsed[0],
            label: parsed[1],
            period: dateParsed[2]
        };
    } else
        timeframe = null;


    return timeframe;
}

//funzione per la generazione della data corrente utilizzata nella post per il cambio di Status
function getDate() {
    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = d.getFullYear() + '/' + (('' + month).length < 2 ? '0' : '')
        + month + '/' + (('' + day).length < 2 ? '0' : '') + day;
    return output;
}
