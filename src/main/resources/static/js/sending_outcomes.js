(function() {
      var app = angular.module('MyAppSending',[]);


		app.controller('sendingOutcomesCtrl', function($scope, $rootScope, $http){
			 $("#error_out").hide();
			 $("#no_status").hide();
			 var report = "";
			 var project = "";

			 var proj,rep;


	        var status="";
	        var count=0;
	        var metric ="";
	        //Click Send

			$rootScope.Send_Out=function(){



		           jQuery("input[name=iCheck1]").each(function () {
		               var project = $(this).is(":checked");
		               if (project) {
		            	   proj = $(this).val();   
		               }
		               else{
		            	   $("#error_out").css("display","block");
		            	   
		               }	  
		           });


                var improvement = {
                    goal: '',
                    metric: '',
                    //TODO aggingere goal ecc
                    message: '',
                };


                var report_fase6DB = {

                    projectRef: '',
                    //TODO aggingere goal ecc
                    improvementList: [improvement],
                    comment: '',

                };
		           /*
		           jQuery("input[name=iCheck2]").each(function () {
		               var report = $(this).is(":checked");
		               if (report) {
		            	   rep = $(this).val();  
		               }
		               else{
		            	   $("#error_out").css("display","block");
		               }   
		           });*/


				// if( rep && proj && count==0){
					// count++;




					 $("#error_out").css("display","none");
					 $("#ok_out").show('slow');

                            $http.get("http://localhost:8082/getDataImprovement/").then(function successCallback(response) {




                     		$rootScope.myimprovement = response.data.improvementMessageList;
                            report_fase6DB.improvementList = response.data.improvementMessageList;



                               /* var data_report ={
                                    reportID :Math.round(100*Math.random()),
                                    json : ReportFase6DB
                                }*/

                                var data_report ={
                                    reportId :Math.round(100*Math.random()),
                                    json : ""
                                }

                                 $http({
                                    url: 'http://localhost:8082/getProject/',
                                    method: "GET"

                                }).then(function(response) {
                                		console.log("ARRIVA RESPONSE ", response);
                                        $rootScope.myProject=response.data.project;
                                        for(i=0;i<$rootScope.myProject.length;i++){
                                            var temp= $rootScope.myProject[i];
                                            console.log("project  ",temp);
                                            report_fase6DB.projectRef=temp.id;
                                        }
                                        var text_comment = $('textarea#textareaComments').val();
                                        report_fase6DB.comment=text_comment;
                                        var ReportFase6DB = JSON.stringify(report_fase6DB);
                                        data_report.json=ReportFase6DB;
                                        console.log("INJSON"+data_report.json);


                                         $http({
                                             url: 'http://localhost:8082/createReportFase6/',
                                             method: "POST",
                                             data: data_report,

                                         }).then(function(response) {

                                                 console.log("Success!");
                                             },
                                             function(response) { // optional
                                                 console.log("Failed!");
                                             })

                                        console.log("Success!");
                                    },
                                    function(response) { // optional
                                        console.log("Failed!");
                                    });
                            //}


                                //console.log("OUTJSON"+data_report.json);
/*
                            $http({
                                url: 'http://localhost:8082/createReportFase6/',
                                method: "POST",
                                data: data_report,

                            }).then(function(response) {

                                    console.log("Success!");
                                },
                                function(response) { // optional
                                    console.log("Failed!");
                                })*/
                     });


			 $("#close_out").click(function(){
				 $("#send_out").hide('slow');
				 $("#error_out").css("display","none");
				 $("#ok_out").hide();
			 });
	 
			 $rootScope.deleteModalStatus=function(){
				 $("#BodyStatus").empty();
				 
			 }
	 
			 $rootScope.deleteModalFeedback=function(){
				 $("#BodyRetrospective").empty();
			 }

			 }

			 



    $rootScope.viewDetails=function(comment){

        	$("#BodyRetrospective").empty();
        	$('<p> '+comment+'</p>'
        	).appendTo('#BodyRetrospective');
        }

});
		
		app.controller('viewSending', function($scope, $http,$rootScope) {
			$rootScope.deletesend=false;
			 $("#ok_delete").css("display","none");
			 $("#error_delete").css("display","none");
				
			$rootScope.getAllsend=function(){
                $http.get("http://localhost:8082/getSendingOutComeFase6/").then(function(response) {
                    $rootScope.mySending= response.data.sendingOutcomesFase6;

                    $rootScope.mySending.forEach(function (my) {
						my.json = JSON.parse(my.json);
                    });
					}, function(error){
                	console.log("Error in getSendingOutComeFase6");
				});
				 
			};
			
			$rootScope.deleteSendOut=function(){
				$rootScope.deletesend=true;
//			    $(".bo").css("display","block !important");
//			    $("#radio_send").css("display","block !important");
				$("#delete_def").css("display","block");
				$("#delete_SendOut").css("display","none");
//				$("#col").css("display","block");
			};
			
			$rootScope.deleteSendDef=function(){
				 $("#error_delete").css("display","none");
				 $("#ok_delete").css("display","none");
				var deleteSend="";
		         var sending_outcomes= {
		        		 sendingOutcomesID:'',
		        		 retrospectiveReportId:'',
		 				 projectRef:'',	 				 
		 				 statusRef:[],
		 				 comment:'',
		 				// feedback:''
		        };
		           jQuery("input[name=iCheck3]").each(function () {
		               var delet_send = $(this).is(":checked");
		               if (delet_send ) {
		            	   deleteSend = $(this).val();   
		               }
		               else{
		            	  
		               }  
		           });
		           
		           if(deleteSend != ""){
		        	    
							$http({
						        url: 'http://localhost:8082/deleteSendingOutcomes/',
						        method: "POST",
						        data: { 'sendingOutcomesID' : deleteSend}
						    })
						    .then(function(response) {
						            console.log("Success!"); 
						            $("#ok_delete").css("display","block");
						            
						           location.reload();
						    }, 
						    function(response) { // optional
					            console.log("Failed!");
						    });
		           } 
		           else{
		        	   $("#error_delete").css("display","block");
		           }
			}	
			
		});
	})();	