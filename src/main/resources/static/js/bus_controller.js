(function () {
    var app = angular.module('controllerBus', ['services']);
    app.controller('busCtrl', function ($rootScope, $scope, $http, $compile, requestService, BusIntegration, FinalReportService, ProjectService, GoalService, MeasurementGoalService, OntologyService, QuestionService) {
        var cc = this;
        var phase = 6;

        BusIntegration.pullFromPhase2({}, function (data) {

        }, function (err) {
            vm.msg = err.statusText;
            $("#bell").notify(
                vm.msg,
                {position: "bottom right"}
            );

        });


        $scope.pullData = function () {
            cc.ProjectService = new ProjectService();
            var project = [];
            ProjectService.getProject().$promise.then(function (data) {
                console.log("Stampo projectId", data.project[0]);
                $rootScope.myProject = data.project[0];
            });
            cc.GoalService = new GoalService();

            var goals = [];

            GoalService.getAllGoals().$promise.then(function(data) {
                goals = data.goals;
                console.log("Stampo goals", goals);
                $rootScope.myGoal = goals;
            });

        }

        $rootScope.selectProject = function (list, projId) {

            if ($scope.showDiv == true) {
                $rootScope.showDiv = !$rootScope.showDiv;
            }

            $rootScope.selected_grid = true;

            MeasurementGoalService.getAllMeasurementGoals().$promise.then(function(data) {
                console.log("mg data ", data);
                $rootScope.measurementGoalList = data.measurementGoals;
            });
            OntologyService.getAllOntologies().$promise.then(function(data) {
                console.log("onto data ", data);
                $rootScope.ontologiesList = data.ontologies;
            });
            QuestionService.getAllQuestions().$promise.then(function(data) {
                console.log("question data ", data);
                $rootScope.questionsList = data.questions;
            });

        };

        $scope.myProjectImport = function () {

            //	console.log("import:",JSON.stringify($rootScope.myProject));
            if (
                $scope.hideRow == true) {
                $scope.showRow = !$scope.showRow;
                $scope.hideRow = !$scope.hideRow;
            } else if ($scope.showRow == true && $scope.hideRow == false) {
                $scope.hideRow = false;
            }

        }


        $scope.recBusRequest = function (type) {

            console.log("Inserisco " + type);

            var dataType = JSON.stringify({typeObject: type});
            console.log("HO IL JSON --> " + dataType);
//  	  		    	if(type=="outcomes") 	{
//  	  		    	var promise =requestService.createBusEntity(JSON.stringify({typeObject:type}));
//  	  		    		promise.then(function (){
//  	  		    			console.log("ceateBusEntity");
//  	  		    		});}
            var result = {};

            if (type != null) {

                //ENTRO QUIIIIIIIIIIII!!!!!!!!
                console.log("ENTRO QUIIIIIIIIIIIIIIIIIII");
                var promise = requestService.requestJsonFromBus(dataType);
                promise.then(function (response) {
                    console.log("SECONDA ENTRATA")
                    //console.log("in",response.data.busResponse);
//  	  		                		$rootScope.postDataResponse =response.data.busResponse;
//  	  		                	console.log("response.data"+JSON.stringify(response));
                    //console.log("type",type);

                    if (type == "metric") {
                        //console.log("tag",tag);


                    }
                    if (type == "question") {
                        //console.log("tag",tag);

                    }
                    if (type == "goal") {

                    }
                    if (type == "strategy") {


                    }
                    if (type == "outcomes") {
                        var test = response.data.message;
                        var tt = {};
                        tt = JSON.parse(test);
                        //console.log("test  :"+test);
                        //	  		                    	  	$rootScope.postDataStatus=tt;
                        //	  		                    	  	$rootScope.myStatus = $rootScope.postDataStatus;
                        console.log("outomes list :" + JSON.stringify(tt));
                        //	console.log("tag",tag);
                        var test = response.data.message;
                        var tt = {};
                        tt = JSON.parse(test);
                        console.log("TEST")
                        conosole.log(test);

                        $rootScope.outcomes = tt;
                        //$rootScope.outcomes = $rootScope.postDataProject;
                        console.log("Outcomes list " + tt.retrospectiveReportId + "---<:>----" + JSON.stringify(tt));


                    }
                    if (type == "InstanceProject") {


                        if ($rootScope.postDataProject == null) {


                            var test = response.data.message;


                            for (var i = 0; i < test.length; i++) {
                                test = test.replace("\"[", "[");
                                test = test.replace("]\"", "]");
                                test = test.replace("\"{", "{");
                                test = test.replace("}\"", "}");
                            }
                            //test = test.substring(0, 109)+''+test.substring(110, test.length);
                            //test = test.substring(0, test.length-2) +"}";
                            //console.log("735: " + test.substring(720, 739));
                            //console.log("TEST = " + test);
                            var tt = {}
                            console.log("JSON test test test ---> " + test);
                            console.log("JSON test pezzo " + test.substring(970, 1000));

                            tt = JSON.parse(test);
                            console.log("JSON parsato ---> " + tt.toString());

                            $rootScope.postDataProject = tt;
                            $rootScope.myProject = $rootScope.postDataProject;
                            console.log("projectId list :" + JSON.stringify($rootScope.myProject));
                        }
                    }
                    if (type == "validatedData") {
                        if ($rootScope.myValidatedData == null) {
                            //console.log("Status:"+JSON.stringify(JSON.stringify(response.data.message)));
                            //var test=JSON.stringify(JSON.stringify(response.data.message));
                            /*console.log("rd"+JSON.stringify(response.data));
                            //console.log("test dopo stringhify",test);
                            for(var i=0; i<test.length;i++){
                             //test=	response.message.replace("\\","");
                                test = test.replace("\\","");
                                test = test.replace("\"{","{");
                                test = test.replace("}\"","}");
                            }
                                test = test.replace("\"\"","");
                                test = test.replace("\"\"","");*/
                            var test = response.data.message;
                            for (var i = 0; i < test.length; i++) {
                                test = test.replace("\"[", "[");
                                test = test.replace("]\"", "]");
                                test = test.replace("\"{", "{");
                                test = test.replace("}\"", "}");
                            }
                            //test = test.substring(0, 96)+''+test.substring(97, test.length);
                            //test = test.substring(0, 564)+''+test.substring(565, test.length);
                            //test = test.substring(0, 577)+''+test.substring(578, test.length);
                            //test = test.substring(0, 746)+''+test.substring(747, test.length);
                            //test = test.substring(0, 747)+''+test.substring(748, test.length);

                            console.log("TEST data = " + test);
                            //console.log("pezzo : " + test.substring(560, 590));
                            //console.log("pezzo = " + test.substring(80, 120));
                            var tt = {};
                            tt = JSON.parse(test);
                            //console.log("test  :"+test);
                            $rootScope.postValidatedData = tt;
                            $rootScope.myValidatedData = $rootScope.postValidatedData;
                            $rootScope.myData = $rootScope.postValidatedData;
                            console.log("validatedData list :" + JSON.stringify($rootScope.myValidatedData));
                        }
                    }


                });

            }

        };


        $scope.sendRequest = function (type, tag, origin, resolve, id) {

            //		console.log(type,tag,origin,resolve,id);
            var dataSend = JSON.stringify({typeObject: type, tag: tag});
            var dataType = JSON.stringify({typeObject: type});
            console.log("DATATYPE : " + dataType);
            var dataTag = JSON.stringify({tag: tag});
            var dataTT = JSON.stringify({typeObject: type, tag: tag});

            var result = {};

            $http.post('http://localhost:8082/sendBusMessage/', dataType)
                .success(function (response) {

                    //console.log("payload: "+JSON.stringify(test).payload);
//		            	console.log("send ok: "+JSON.stringify(JSON.stringify(response.message.replace("\\",""))));  
//		            	//var obj = JSON.parse(JSON.stringify(JSON.stringify(response.message)));
//		            	//console.log("obj: "+ JSON.stringify(obj));
//		            	//console.log("REPLACED: "+ JSON.parse());
//		            	console.log("send ok: "+JSON.stringify(JSON.stringify(test.payload)));  

                });


            if (tag != null && type != null) {
                var promise = requestService.getJsonFromBus(dataTT);
                promise.then(function (response) {
                    //console.log("in",response.data.busResponse);
                    $rootScope.postDataResponse = response.data.busResponse;
                    if (type == "metric") {
                        //console.log("tag",tag);
                        var temp = (JSON.parse(response.data.busResponse.content[2]));
                        $rootScope.postDataMetric = temp;

                    }
                    if (type == "question") {
                        //console.log("tag",tag);
                        var temp = (JSON.parse(response.data.busResponse.content[2]));
                        $rootScope.postDataQuestion = temp;

                    }
                    if (type == "goal") {
                        var temp = (JSON.parse(response.data.busResponse.content[2]));
                        for (var tp = 0; tp < temp.goals.length; tp++) {
                            if (temp.goals[tp].strategyRef != null) {
                                for (var ta = 0; ta < temp.goals[tp].strategyRef.length; ta++) {
                                    //temp.goals[tp].strategyRef[ta]={id:temp.goals[tp].strategyRef[ta].strategyId};
                                    temp.goals[tp].strategyRef[ta] = temp.goals[tp].strategyRef[ta].strategyId;
                                }
                            }
                            if (temp.goals[tp].measurementRef != null) {
                                for (var ta = 0; ta < temp.goals[tp].measurementRef.length; ta++) {
                                    //temp.goals[tp].measurementRef[ta]={id:temp.goals[tp].measurementRef[ta].measurementGoalId};
                                    temp.goals[tp].measurementRef[ta] = temp.goals[tp].measurementRef[ta].measurementGoalId;
                                }
                            }
                        }
                        $rootScope.postDataGoal = temp;
                    }
                    if (type == "strategy") {
                        var temp = (JSON.parse(response.data.busResponse.content[2]));
                        for (var tp = 0; tp < temp.strategies.length; tp++) {
                            if (temp.strategies[tp].goalRef != null) {
                                for (var ta = 0; ta < temp.strategies[tp].goalRef.length; ta++) {
                                    //temp.strategies[tp].goalRef[ta]={id:temp.strategies[tp].goalRef[ta].goalId};
                                    temp.strategies[tp].goalRef[ta] = temp.strategies[tp].goalRef[ta].goalId;
                                }
                            }
                        }
                        $rootScope.postDataStrategy = temp;

                    }
                    if (type == "measurementgoal") {
                        //	console.log("tag",tag);
                        var temp = (JSON.parse(response.data.busResponse.content[2]));
                        for (var tp = 0; tp < temp.measurementGoals.length; tp++) {
                            if (temp.measurementGoals[tp].metricsRef != null) {
                                for (var ta = 0; ta < temp.measurementGoals[tp].metricsRef.length; ta++) {
                                    //temp.measurementGoals[tp].metricsRef[ta]={id:temp.measurementGoals[tp].metricsRef[ta].metricId};
                                    temp.measurementGoals[tp].metricsRef[ta] = temp.measurementGoals[tp].metricsRef[ta].metricId;
                                }
                            }
                            if (temp.measurementGoals[tp].questionsRef != null) {
                                for (var ta = 0; ta < temp.measurementGoals[tp].questionsRef.length; ta++) {
                                    //temp.measurementGoals[tp].questionsRef[ta]={id:temp.measurementGoals[tp].questionsRef[ta].questionId};
                                    temp.measurementGoals[tp].questionsRef[ta] = temp.measurementGoals[tp].questionsRef[ta].questionId;
                                }
                            }
                        }
                        $rootScope.postDataMeasurement = temp;


                    }
                    if (type == "projectId") {
                        //	console.log("tag",tag);
                        console.log("JSON.parse(response.data.busResponse.content[2].payload): " + JSON.parse(response.data.busResponse.content[2].payload));
                        var temp = (JSON.parse(response.data.busResponse.content[2].payload));

                        for (var tp = 0; tp < temp.project.length; tp++) {
                            if (temp.project[tp].goalRef != null) {
                                for (var ta = 0; ta < temp.project[tp].goalRef.length; ta++) {
                                    //temp.projectId[tp].goalRef[ta]={id:temp.projectId[tp].goalRef[ta].goalId};
                                    temp.project[tp].goalRef[ta] = temp.project[tp].goalRef[ta].goalId;
                                }
                            }
                        }

                        $rootScope.postDataProject = temp;


                    }
                });


            }

            else {
                if (tag != null) {
                    var promise = requestService.getJsonFromBus(dataTag);
                    promise.then(function (response) {
                        //console.log("in",response.data.busResponse);
                        $rootScope.postDataResponse = response.data.busResponse;
                    });


                }
                else {
                    //  			console.log("type");
                    var promise = requestService.getJsonFromBus(dataType);
                    promise.then(function (response) {
                        //console.log("in",response.data.busResponse);
                        $rootScope.postDataResponse = response.data.busResponse;

                        //console.log("type",type);

                        if (type == "metric") {
                            //console.log("tag",tag);
                            var temp = (JSON.parse(response.data.busResponse.content[2]));
                            $rootScope.postDataMetric = temp;

                        }
                        if (type == "question") {
                            //console.log("tag",tag);
                            var temp = (JSON.parse(response.data.busResponse.content[2]));
                            $rootScope.postDataQuestion = temp;

                        }
                        if (type == "goal") {
                            var temp = (JSON.parse(response.data.busResponse.content[2]));
                            for (var tp = 0; tp < temp.goals.length; tp++) {
                                if (temp.goals[tp].strategyRef != null) {
                                    for (var ta = 0; ta < temp.goals[tp].strategyRef.length; ta++) {
                                        //temp.goals[tp].strategyRef[ta]={id:temp.goals[tp].strategyRef[ta].strategyId};
                                        temp.goals[tp].strategyRef[ta] = temp.goals[tp].strategyRef[ta].strategyId;
                                    }
                                }
                                if (temp.goals[tp].measurementRef != null) {
                                    for (var ta = 0; ta < temp.goals[tp].measurementRef.length; ta++) {
                                        //temp.goals[tp].measurementRef[ta]={id:temp.goals[tp].measurementRef[ta].measurementGoalId};
                                        temp.goals[tp].measurementRef[ta] = temp.goals[tp].measurementRef[ta].measurementGoalId;
                                    }
                                }
                            }
                            $rootScope.postDataGoal = temp;
                        }
                        if (type == "strategy") {
                            var temp = (JSON.parse(response.data.busResponse.content[2]));
                            for (var tp = 0; tp < temp.strategies.length; tp++) {
                                if (temp.strategies[tp].goalRef != null) {
                                    for (var ta = 0; ta < temp.strategies[tp].goalRef.length; ta++) {
                                        //temp.strategies[tp].goalRef[ta]={id:temp.strategies[tp].goalRef[ta].goalId};
                                        temp.strategies[tp].goalRef[ta] = temp.strategies[tp].goalRef[ta].goalId;
                                    }
                                }
                            }
                            $rootScope.postDataStrategy = temp;

                        }
                        if (type == "measurementgoal") {
                            //	console.log("tag",tag);
                            var temp = (JSON.parse(response.data.busResponse.content[2]));
                            for (var tp = 0; tp < temp.measurementGoals.length; tp++) {
                                if (temp.measurementGoals[tp].metricsRef != null) {
                                    for (var ta = 0; ta < temp.measurementGoals[tp].metricsRef.length; ta++) {
                                        //temp.measurementGoals[tp].metricsRef[ta]={id:temp.measurementGoals[tp].metricsRef[ta].metricId};
                                        temp.measurementGoals[tp].metricsRef[ta] = temp.measurementGoals[tp].metricsRef[ta].metricId;
                                    }
                                }
                                if (temp.measurementGoals[tp].questionsRef != null) {
                                    for (var ta = 0; ta < temp.measurementGoals[tp].questionsRef.length; ta++) {
                                        //temp.measurementGoals[tp].questionsRef[ta]={id:temp.measurementGoals[tp].questionsRef[ta].questionId};
                                        temp.measurementGoals[tp].questionsRef[ta] = temp.measurementGoals[tp].questionsRef[ta].questionId;
                                    }
                                }
                            }
                            $rootScope.postDataMeasurement = temp;


                        }
                        if (type == "projectId") {
                            //	console.log("tag",tag);
                            var temp = (JSON.parse(response.data.busResponse.content[2]));
                            for (var tp = 0; tp < temp.project.length; tp++) {
                                if (temp.project[tp].goalRef != null) {
                                    for (var ta = 0; ta < temp.project[tp].goalRef.length; ta++) {
                                        //temp.projectId[tp].goalRef[ta]={id:temp.projectId[tp].goalRef[ta].goalId};
                                        temp.project[tp].goalRef[ta] = temp.project[tp].goalRef[ta].goalId;
                                    }
                                }
                            }

                            $rootScope.postDataProject = temp;

                        }


                    });

                }

            }
            ;

        };


        $scope.insert_question = function (obj) {
            if (jQuery.isPlainObject(obj))
                return obj;
            for (var i = 0; i < $rootScope.postDataQuestion.questions.length; i++) {
                if (obj == $rootScope.postDataQuestion.questions[i].questionId)
                    return $rootScope.postDataQuestion.questions[i];
            }
        };

        $scope.insert_metric = function (obj) {
            if (jQuery.isPlainObject(obj))
                return obj;
            for (var i = 0; i < $rootScope.postDataMetric.metrics.length; i++) {
                if (obj == $rootScope.postDataMetric.metrics[i].metricId)
                    return $rootScope.postDataMetric.metrics[i];
            }

        };
        $scope.insert_measurement = function (obj) {
            if (jQuery.isPlainObject(obj))
                return obj;
            for (var i = 0; i < $rootScope.postDataMeasurement.measurementGoals.length; i++) {
                if (obj == $rootScope.postDataMeasurement.measurementGoals[i].measurementGoalId) {
                    if ($rootScope.postDataMeasurement.measurementGoals[i].questionsRef != null)
                        for (var j = 0; j < $rootScope.postDataMeasurement.measurementGoals[i].questionsRef.length; j++) {
                            $rootScope.postDataMeasurement.measurementGoals[i].questionsRef[j] = $scope.insert_question($rootScope.postDataMeasurement.measurementGoals[i].questionsRef[j]);
                        }
                    if ($rootScope.postDataMeasurement.measurementGoals[i].metricsRef != null)
                        for (var j = 0; j < $rootScope.postDataMeasurement.measurementGoals[i].metricsRef.length; j++) {
                            $rootScope.postDataMeasurement.measurementGoals[i].metricsRef[j] = $scope.insert_metric($rootScope.postDataMeasurement.measurementGoals[i].metricsRef[j]);
                        }

                    return $rootScope.postDataMeasurement.measurementGoals[i];
                }
            }
        };

        $scope.insert_strategy = function (obj) {


            if (jQuery.isPlainObject(obj))
                return obj;
            for (var i = 0; i < $rootScope.postDataStrategy.strategies.length; i++) {

                if (obj == $rootScope.postDataStrategy.strategies[i].strategyId) {
                    if ($rootScope.postDataStrategy.strategies[i].goalRef != null)
                        for (var j = 0; j < $rootScope.postDataStrategy.strategies[i].goalRef.length; j++) {
                            $rootScope.postDataStrategy.strategies[i].goalRef[j] = $scope.insert_goal($rootScope.postDataStrategy.strategies[i].goalRef[j]);
                        }

                    return $rootScope.postDataStrategy.strategies[i];
                }
            }
        };

        $scope.insert_goal = function (obj) {
            //console.log(obj,jQuery.isPlainObject(obj),jQuery.isPlainObject(obj.id));
            //if(obj.id==null)
            if (jQuery.isPlainObject(obj))
                return obj;
            for (var i = 0; i < $rootScope.postDataGoal.goals.length; i++) {
//				if(obj.id == $rootScope.postDataGoal.goals[i].goalId)
                //console.log("goal obj.id",obj);
//				console.log("goal",JSON.stringify($rootScope.postDataGoal.goals[i]));
//				console.log("goal before",JSON.stringify($rootScope.postDataGoal.goals[i]));
                //	if(obj.id == $rootScope.postDataGoal.goals[i].goalId){
                if (obj == $rootScope.postDataGoal.goals[i].goalId) {
                    if ($rootScope.postDataGoal.goals[i].strategyRef != null) {
                        for (var j = 0; j < $rootScope.postDataGoal.goals[i].strategyRef.length; j++)
                            $rootScope.postDataGoal.goals[i].strategyRef[j] = $scope.insert_strategy($rootScope.postDataGoal.goals[i].strategyRef[j]);
                    }
                    if ($rootScope.postDataGoal.goals[i].measurementRef != null) {
                        for (var j = 0; j < $rootScope.postDataGoal.goals[i].measurementRef.length; j++)
                            $rootScope.postDataGoal.goals[i].measurementRef[j] = $scope.insert_measurement($rootScope.postDataGoal.goals[i].measurementRef[j]);
                    }

                    //				console.log("goal after",JSON.stringify($rootScope.postDataGoal.goals[i]));
                    return $rootScope.postDataGoal.goals[i];
                }
            }
        };
        $scope.create_grid = function () {
            for (var i = 0; i < $rootScope.postDataProject.project.length; i++) {
                //	console.log("before",JSON.stringify($rootScope.postDataProject.projectId[i]));
                if ($rootScope.postDataProject.project[i].goalRef != null)
                    for (var j = 0; j < $rootScope.postDataProject.project[i].goalRef.length; j++) {
                        $rootScope.postDataProject.project[i].goalRef[j] = $scope.insert_goal($rootScope.postDataProject.project[i].goalRef[j]);
                    }
                //	console.log("after:"+ i+":",JSON.stringify($rootScope.postDataProject.projectId[i]));

            }
            $rootScope.myProject = $rootScope.postDataProject;
        };

        $scope.completeRequest = function () {//type,tag,origin,resolve,id
//	  	$scope.sendRequest("metric",tag,origin,resolve,id);
//	  	$scope.sendRequest("question",tag,origin,resolve,id);
//	    	$scope.sendRequest("strategy",tag,origin,resolve,id);
//	    	$scope.sendRequest("measurementgoal",tag,origin,resolve,id);
//	    	$scope.sendRequest("goal",tag,origin,resolve,id);	    	
//	    	$scope.sendRequest("projectId",tag,origin,resolve,id);
            if ($rootScope.postDataStatus == null)
            //console.log("HO INSERITO validatedData");
                $scope.recBusRequest("validatedData");
            if ($rootScope.postDataStatus == null)
            //console.log("HO INSERITO status");
                $scope.recBusRequest("status");
            if ($rootScope.postDataProject == null)
            //console.log("HO INSERITO projectId");
                $scope.recBusRequest("InstanceProject");


            //$scope.recBusRequest("outcomes");
        };

        $scope.enableDiagram = function () {
            document.getElementById("Diagram").className = "enable";
//	    	$("#Diagram").className= "enable";
        };


    });
    app.factory('requestService', function ($http) {
        return {
            getJsonFromBus: function (data) {//data è il "filtro" da applicare alla ricerca (usa il fakeBus)
                return $http.post('http://localhost:8082/receiveBusMessage/', data);
            },
            requestJsonFromBus: function (data) {//data è il "filtro" da applicare alla ricerca (usa il Bus)
                return $http.post('http://localhost:8082/receiveRealBusMessage/', data);
            },
            createBusEntity: function (data) {
                return $http.post('http://localhost:8082/createBusEntity/', data);
            }
        }
    });


//  	test.replace("/","");
//  	console.log("response dopo replace"+test);
//  	$rootScope.postDataProject =test;

})();