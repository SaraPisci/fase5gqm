(function () {
    var app = angular.module('MyAppFeed', []);

    app.run(function ($rootScope, $http) {

        //$http.get("http://localhost:8082/getProject/").then(function(response) {
        //	$rootScope.myProject= response.data.projectId;
        //		});
        /*$http.get("http://localhost:8082/getRetrospectiveReports/").then(function(response) {
            $rootScope.myretrospective= response.data.retrospectiveReports;
        }*/

        $http.get("http://localhost:8082/getDataImprovement/").then(function (response) {
                $rootScope.myimprovement = response.data.improvementMessageList;
                console.log("GUARDA", $rootScope.myimprovement);


            }
        );
    });

    var arr = new Array();
    var arr_salva = new Array();

    /* Inizio Feedback*/
    app.controller('createfeedCtrl', function ($rootScope, $scope, $http) {

        $rootScope.init = function () {
            $rootScope.num_feed = 1;
            var phases_check = "";
            var phases_check_arr = [];
            var priority = "";
            var selText = "";
            var phases_checkControl = "";
            var priorityControl = "";
            var selTextControl = "";
            $rootScope.feedback_a = new Array();


            $("#error_conc").hide();
            $("#ok_conc").hide();
            var Date = "";
            var Version = "";
            var concl_salva = {
                conclusion: '',
                version: '',
                dateModify: '',
                feedback: []
            };

            //Modal
            jQuery('#wizard').smartWizard({
                selected: 0,  // Selected Step, 0 = first step
                cycleSteps: true, // cycle step
                noForwardJumping: true,
                contentCache: true,
                contentURL: null, // specifying content url enables ajax content loading
                contentURLData: null,
                enableFinishButton: false, // makes finish button enabled always
                onLeaveStep: validateSteps, // triggers when leaving a step
                ajaxType: 'POST',
                // onShowStep:,  // triggers when showing a step
                onFinish: onFinishCallback,  // triggers when Finish button is clicked
                labelFinish: 'Save',
                includeFinishButton: true, // Add the finish button


            });

            //jQuery('#wizard').smartWizard('goToStep', 1);
            $('#myModal').modal("show");
            jQuery('.buttonNext').addClass('btn btn-success');
            jQuery('.buttonNext').attr('type', "submit");
            jQuery('.buttonNext').attr('value', "Submit");
            jQuery('.buttonPrevious').addClass('btn btn-primary');
            jQuery('.buttonFinish').addClass('btn btn-default');
            jQuery('.buttonFinish').attr("data-container", "body");
            jQuery('.buttonFinish').attr("data-toggle", "popover");
            jQuery('.buttonFinish').attr("data-placement", "left");
            jQuery('.buttonFinish').attr("data-trigger", "focus");
            jQuery('.buttonFinish').attr("title", "Chiudi (X) per continuare la creazione del Report");
            $('.buttonPrevious').addClass("buttonDisabled");
            $('.buttonFinish').addClass("buttonDisabled");

            //$('.buttonPrevious').removeClass("buttonDisabled");

            function onFinishCallback(stepnumber, currentIndex, arr) {
                saveValid = false;
                if (validateAllSteps(stepnumber, currentIndex)) {
                    selTextControl = "";
                    selTextControl = $('textarea#textareaID').val();
                    if (selTextControl == "") {
                        jQuery("#error_feed").css("display", "block");
                        saveValid = false;
                        selTextControl = "";
                        return false;
                    }
                    else {
                        saveValid = true;
                        arr = saveData();
                        jQuery("#error_feed").css("display", "none");
                        $rootScope.num_feed++;
                        priorityControl = "";
                        phases_checkControl_A = [];
                        selTextControl = "";
                        phases_checkControl = "";
                        $('textarea#textareaID').val('');
                        jQuery('.buttonFinish').popover();
                        $("#myModal").modal('hide');
                        jQuery('#successA').css("display", "block");
                        jQuery('#riepilogo').css("display", "block");
                        $("#riepilogo").show();
                        $rootScope.feedback_a = arr;
                        var len = arr.length;
                        var count = 1;
                        for (i = 0; i < len; i++) {

                            $('<ul class="list-unstyled timeline">'
                                + '<li>'
                                + '<div class="block" >'
                                + '<div class="tags">'
                                + '<a href="#" class="tag">'
                                + ' <span>Feedback:' + count + '</span>'
                                + '</a>'
                                + '</div>'
                                + '<div class="block_content">'
                                + '<h2 class="title">'
                                + '<a> Phase Involved :' + arr[i].phases_check + '</a>'
                                + '</h2>'
                                + '<div class="byline">'
                                + '<span> Priority :</span> <a>' + arr[i].priority + '</a>'
                                + '</div>'
                                + '<p class="excerpt">' + arr[i].selText + '</p>'
                                + '</div>'
                                + '</div>'
                                + '</li>'
                                + '</ul> ').appendTo('#riepilogo_f');
                            count++;
                        }
                        //jQuery('#wizard').smartWizard('goToStep', 1);
                        return true;
                    }
                }
            }

            function validateSteps(stepnumber, currentIndex) {
                var isStepValid = true;
                if (currentIndex.fromStep != 4) {
                    $('.buttonFinish').addClass("buttonDisabled");
                }
                else {
                    $('.buttonFinish').removeClass("buttonDisabled");
                }
                if (currentIndex.toStep != 1) {
                    $('.buttonPrevious').removeClass("buttonDisabled");
                }
                else {
                    $('.buttonPrevious').addClass("buttonDisabled");
                }
                if (currentIndex.fromStep == 2) {
                    jQuery("input[name=check]").each(function () {
                        var ischecked = $(this).is(":checked");
                        if (ischecked) {
                            phases_checkControl += $(this).val() + ",";
                        }
                    });
                    if (phases_checkControl == undefined) {
                        phases_checkControl = "";
                        isStepValid = false;
                        return false;
                    }
                    else {
                        isStepValid = true;
                        return isStepValid;
                    }
                }

                if (currentIndex.fromStep == 3) {
                    priorityControl = "";
                    jQuery("input[name=iCheck]").each(function () {
                        var prioritycheck = $(this).is(":checked");
                        if (prioritycheck) {
                            priorityControl = $(this).val();
                        }
                    });
                    if (priorityControl == undefined) {
                        priorityControl = "";
                        isStepValid = false;
                        return false;
                    }
                    else {
                        isStepValid = true;
                        return isStepValid;
                    }
                }

                if (currentIndex.fromStep == 4) {
                    selTextControl = "";
                    selTextControl = $('textarea#textareaID').val();
                    if (selTextControl == "") {
                        jQuery("#error_feed").css("display", "block");
                        isStepValid = false;
                        selTextControl = "";
                        return false;
                    }
                    else {
                        isStepValid = true;
                        saveData();
                        jQuery("#error_feed").css("display", "none");
                        $rootScope.num_feed = $rootScope.num_feed + 1;
                        var phases_checkControl = "";
                        $('textarea#textareaID').val('');
                        var priorityControl = "";
                        var selTextControl = "";
                        return true;
                    }
                }
                return true;
            }

            function validateAllSteps(stepnumber, currentIndex) {
                var isStepValid = false;
                if (currentIndex.fromStep == 4 && $('textarea#textareaID').val() != "") {
                    isStepValid = true;
                    return true;
                }
                else
                    return false;
                return isStepValid;
            }

            function saveData() {
                phases_check_p = new Array();
                phases_check_arr = [];
                phases_check = "";
                jQuery("input[name=check]").each(function () {
                    var ischecked = $(this).is(":checked");
                    if (ischecked) {
                        phases_check += $(this).val() + ",";
                        phases_check_arr.push($(this).val());
                    }
                });
                //Priorità
                priority = "";
                jQuery("input[name=iCheck]").each(function () {
                    var prioritycheck = $(this).is(":checked");
                    if (prioritycheck) {
                        priority = $(this).val();

                    }
                });
                selText = "";
                selText = $('textarea#textareaID').val();
                var feed = {
                    phases_check: '',
                    priority: '',
                    selText: ''
                };

                var feed_salva = {
                    phases_check: [],
                    priority: '',
                    selText: ''
                };

                feed_salva.phases_check = phases_check_arr;
                feed_salva.priority = priority;
                feed_salva.selText = selText;
                feed.phases_check = phases_check;
                feed.priority = priority;
                feed.selText = selText;
                arr.push(feed);
                arr_salva.push(feed_salva);
                $scope.feedback_a.push(feed);
                return arr_salva;
            }

            $("#save").click(function () {
                $('.buttonFinish').addClass("buttonDisabled");
                var textareaConclusion = '';
                var single_cal2 = '';
                var version = '';
                var Conclusion = "";
                var Date = "";
                var Version = "";
                var concl_salva = {
                    retrospectiveReportId: '',
                    conclusion: '',
                    lastModified: '',
                    ids: [],
                    descriptionList: [],
                    priorityList: []

                };
                var retrospectiveReport = concl_salva;
                textareaConclusion = "";
                textareaConclusion = $('#editor').text();
                if (textareaConclusion == "") {
                    jQuery("#error_conc").show();
                    textareaConclusion = "";
                    return false;
                }
                else {
                    Conclusion = textareaConclusion;
                }
                version = "";
                version = $('#version').val();
                if (version == "") {
                    jQuery("#error_conc").show();
                    version = "";
                    return false;
                }
                else {
                    Version = version;
                }
                single_cal2 = "";
                single_cal2 = $('#single_cal2').val();
                if (single_cal2 == "") {
                    jQuery("#error_conc").show();
                    single_cal2 = "";
                    return false;
                }
                else {
                    Date = single_cal2;
                }

                $("#ok_conc").show();
                $("#error_conc").hide();


                concl_salva.conclusion = Conclusion;
                concl_salva.retrospectiveReportId = Version;
                concl_salva.lastModified = Date;

                var len = $rootScope.feedback_a.length;

                for (i = 0; i < len; i++) {
                    var phases = ($rootScope.feedback_a[i]).phases_check;
                    var num = phases.length;
                    for (j = 0; j < num; j++) {
                        concl_salva.ids.push(phases[j]);
                        (concl_salva.priorityList).push(($rootScope.feedback_a[i]).priority);
                        (concl_salva.descriptionList).push(($rootScope.feedback_a[i]).selText);

                    }
                }

                var retrospectiveReport = JSON.stringify(concl_salva);
                $http({
                    url: 'http://localhost:8082/createRetrospectiveReport/',
                    method: "POST",
                    data: retrospectiveReport
                })
                    .then(function (response) {
                            console.log("Success!");
                            $('#version').val('');


                        },
                        function (response) { // optional
                            console.log("Failed!");
                        });

            });
        }
        //init

    });


})();	
