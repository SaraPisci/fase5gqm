$(document).ready(function() {

    var loginPageUri = "http://10.220.32.198:8081/index.html";
    var backendPhase = "http://localhost:8082";

	//var user = getLoggedUsername();
	var user = "Kermit";

    document.getElementById("welcome").innerHTML = user;
    document.getElementById("navbar").innerHTML = user;

    $("#logout").click(function() {
            document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
            document.cookie = "role=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
            document.location = getLoginPageUri();
	});

	$("#NewFeed").click(function(){
		$('#readFeedbackContainer').hide();
		$('#NewFeed').attr("disabled", true);
		$("#successA").hide();
		$("#riepilogo").hide();
		$("#id_div").hide('puff');
		$("#send_out").hide();
		$("#viewSendingO").hide();
		$("#container").hide();

	});

	$("#Diagram").click(function(){
	//	$('#container').show();
		$("#NewFeedback").hide();
		$("#riepilogo").hide();
		$("#id_div").hide();
		$("#send_out").hide();
		$("#viewSendingO").hide();
	});

    $("#Report").click(function(){
        //	$('#container').show();
        $("#NewFeedback").hide();
        $("#riepilogo").hide();
        $("#id_div").hide();
        $("#send_out").hide();
        $("#viewSendingO").hide();
    });

    $('#proLoading').hide();

    $("#seleziona").click(function(){
        $('#proLoading').show();
        setTimeout(function () {
            $('#label_grid').click();
            $('#proLoading').hide();
        }, 1000);
    });

    $('#pro').click(function(){
        /*if($('id_div').css("visibility") == "hidden") {
        	console.log("div", $('id_div'));
            $("#seleziona").click();
        }*/
        $('#id_div').show();
    });

//	$('#myModal').on('shown.bs.modal', function () {
//		  $('#myInput').focus()
//	});

	$("#error_conc").hide();
	$("#ok_conc").hide();
	
	$("#close_riep").click(function(){
		$("#riepilogo").hide();
		$('#NewFeed').show();
	});
	
//	$("#pro").click(function(){
//		$('#NewFeed').show();
//		$("#id_div").show('puff');
//		$("#Newfeedback").hide();
//		$("#riepilogo").hide();
//		$("#send_out").hide();
//		$("#viewSendingO").hide();
//		
//	});
	$("#SendingOutcomes").click(function(){
		$('#readFeedbackContainer').hide();
		$('#NewFeed').show();
		$("#NewFeedback").hide();
		$("#riepilogo").hide();
		$("#id_div").hide();
		$("#send_out").toggle('puff');
		$("#viewSendingO").hide();
	});
	
	$("#viewSendingOutcomes").click(function(){
		$('#readFeedbackContainer').hide();
		$('#NewFeed').show();
		$("#viewSendingO").toggle("puff");
		$("#NewFeedback").hide();
		$("#riepilogo").hide();
		$("#id_div").hide();
		$("#send_out").hide();
	});


	
	$("#close_riep").click(function(){
	        location.reload();
	});
	
	$("#close_out").click(function(){
        location.reload();
});

	function show(toBlock) {
		setDisplay(toBlock, 'block');
	}
	function hide(toNone) {
		setDisplay(toNone, 'none');
	}
	function setDisplay(target, str) {
		document.getElementById(target).style.display = str;
		console.log("oggetto show/hide " + document.getElementById(target));
	}


	    /* bootstrap-wysiwyg */
	   
//	      $(document).ready(function() {
	        function initToolbarBootstrapBindings() {
	          var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
	              'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
	              'Times New Roman', 'Verdana'
	            ],
	            fontTarget = $('[title=Font]').siblings('.dropdown-menu');
	          $.each(fonts, function(idx, fontName) {
	            fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
	          });
	          $('a[title]').tooltip({
	            container: 'body'
	          });
	          $('.dropdown-menu input').click(function() {
	              return false;
	            })
	            .change(function() {
	              $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
	            })
	            .keydown('esc', function() {
	              this.value = '';
	              $(this).change();
	            });

	          $('[data-role=magic-overlay]').each(function() {
	            var overlay = $(this),
	              target = $(overlay.data('target'));
	            overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
	          });

	          if ("onwebkitspeechchange" in document.createElement("input")) {
	            var editorOffset = $('#editor').offset();

	            $('.voiceBtn').css('position', 'absolute').offset({
	              top: editorOffset.top,
	              left: editorOffset.left + $('#editor').innerWidth() - 35
	            });
	          } else {
	            $('.voiceBtn').hide();
	          }
	        }

	        function showErrorAlert(reason, detail) {
	          var msg = '';
	          if (reason === 'unsupported-file-type') {
	            msg = "Unsupported format " + detail;
	          } else {
	            console.log("error uploading file", reason, detail);
	          }
	          $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
	            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
	        }

	        initToolbarBootstrapBindings();

	        $('#editor').wysiwyg({
	          fileUploadError: showErrorAlert
	        });

	        window.prettyPrint;
	        prettyPrint();
//	      });
	   
	    /* /bootstrap-wysiwyg */
	      
	      /*datapicker*/
	      $('#single_cal2').daterangepicker({
	          singleDatePicker: true,
	          calender_style: "picker_2"
	        }, function(start, end, label) {
	        	
	        });

    function getBackendPhase() {
        return backendPhase;
    }

    function getLoggedUsername() {
        var tempAssignee = getCookie("username").replace(/\"/g, "");
        var username = getURLParameter('username');
        var hash = getURLParameter('token');
        if (!tempAssignee || tempAssignee.length === 0) {
            var data = {
                username: username,
                hash: hash
            };
            if (username == '' || hash == '') {
                redirectToLogin();
                return;
            }
            $.post(getBackendPhase() + "/login", data, function (response, status) {
                tempAssignee = username;
                setCookie("username", tempAssignee);
                printCookie();
                console.log("entered");
            }).fail(function (jqXHR, textStatus, errorThrown) {
                redirectToLogin();

            });
        } else {
            tempAssignee = username;
        }
        printCookie();
        return tempAssignee;
    }

    function redirectToLogin() {
        alert("You are not logged in. Please, login first!");
            document.location = getLoginPageUri();
    }

    function getLoginPageUri() {
    	return loginPageUri;
	}

    //Retrive cookie from html page
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //set a cookie without timestamp
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";path=/";
    }

    function printCookie() {
        console.log(document.cookie);
    }

    //Get parameters from link
    function getURLParameter(name) {

        return decodeURIComponent((new RegExp('[?|&]' + name + '='
            + '([^&;]+?)(&|#|;|$)').exec(location.search) || [ null, '' ])[1]
                .replace(/\+/g, '%20'))
            || null;
    }
});	 
	