
var serv = angular.module('services',['ngResource']);

serv.factory('GoalService', function($resource) {
    return $resource('/getGoals/:id', { id: '@id' }, {
        getAllGoals: {
            method: 'GET',
            url:'/getGoals/',
        }
    });
});


serv.factory('ThresholdService', function($resource) {
    return $resource('/getThresholds/:im', { im: '@im'}, {
        getThresholds: {
            method: 'GET',
            url:'/getThresholds/:im',
        }
    });
});


serv.factory('BusIntegration', function($resource) {
    return $resource('/integration/:met/:phase', { id: '@id' }, {
        update: {
            method: 'PUT'
        },
        pushDataonBus: {
            method: 'PUT',
            params:{met:'@met',phase:'@phase'}

        },
        pullDatafromBus: {
            method: 'PUT',
            params:{met:'@met',phase:'@phase'}

        },

        pullFromPhase2: {
            method: 'PUT',
            url:'/pullFromPhase2/',
        }

    });

});

serv.factory('FinalReportService', function($resource) {
    return $resource('/finalReport/:id', { id: '@id' }, {
        getFinalReport: {
            method: 'GET',
            url:'/getFinalReport/',
        }
    });
});


serv.factory('ProjectService', function($resource) {
    return $resource('/getProject/:id', { id: '@id' }, {
        getProject: {
            method: 'GET',
        }
    });
});

serv.factory('OntologyService', function($resource) {
    return $resource('/getOntologies/:id', { id: '@id' }, {
        getAllOntologies: {
            method: 'GET'
        }
    });
});

serv.factory('MeasurementGoalService', function($resource) {
    return $resource('/getMeasurementGoal/:title', { title: '@title' }, {
        getMeasurementGoalByOrgGoalId: {
            method: 'GET',
            params:{title:'title'}
        },
        getAllMeasurementGoals:{
            method: 'GET'
        }

    });
});

serv.factory('QuestionService', function($resource) {
    return $resource('/getQuestions/:id', { id: '@id' }, {
        getAllQuestions: {
            method: 'GET'
        }
    });
});


