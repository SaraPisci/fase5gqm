(function () {

    var diag = angular.module('diagCtrl', ['services']);
    diag.directive('productDiagram', function ($rootScope, $http, $window) {
//	$window.location.reload();


        return {
            restrict: 'E',
            templateUrl: 'product-diagram.html',
            controller: function () {
                $(document).ready(function () {
                    document.getElementById("container").style.display = 'none';
                });
            }
        }
    });


    diag.controller('diagramCtrl', function ($scope, $http, $rootScope, GoalService, ThresholdService) {

            var cc = this;
            var sc = $scope;

            var metricListSel = [];
            $scope.metricListVar = [];
            $rootScope.arrValData = [];
            var validatedDatas = [];
            var mgSel = [];
            var ontologyListSel = [];
            $scope.thresholds = {};
            $scope.myBarChart = {};
            $scope.myLineChart = {};
            $scope.mynewChart = {};
            $scope.finalReport = [];

            $scope.thresholdList = [];
            $scope.validateMetric = [];
            var toClose = [];

            cc.GoalService = new GoalService();

            var goals = [];

            /*GoalService.getAllGoals().$promise.then(function (data) {
                console.log("goals");
                goals = data.goals;
                console.log(goals);
            });*/

            var goal = {
                goalID: ''
            };

            document.getElementById("viewChart").disabled = true;

            $("#Diagram").click(function () {

                GoalService.getAllGoals().$promise.then(function (data) {
                    goals = data.goals;

                    if ($('#container:visible').length == 0) {
                        $('#container').toggle('slow');
                    }
                    else {
                        $('#container').toggle('slow');
                        $('#container').hide();
                    }


                    var el = document.getElementById("container");
                    document.getElementById("chartContainer").style.visibility = "hidden";

                    $rootScope.metricSelGoal = [];
                    $rootScope.goalSelProject = [];
                    $rootScope.goalSelProject = goals;
                    var idGoal = $rootScope.myGoal[0].goalId;
                    for (i = 0; i < goals.length; i++) {
                        if (goals[i].goalId === idGoal) {
                            $rootScope.myGoal[0] = goals[i];
                        }
                    }
                    //$rootScope.append_goal("1", $rootScope.myGoal);
                    goals = null;
                    goals = [];
                    var len = 0;
                    var bool = true;
                    for (i = 0; i < $rootScope.goalSelProject.length; i++) {
                        goalSelId = $rootScope.goalSelProject[i].goalId;
                        bool = true;
                        for (j = 0; j < goals.length; j++) {
                            if (goals[j].goalId === goalSelId) {
                                bool = false;
                            }
                        }
                        if (bool === true) {
                            goals[len] = $rootScope.goalSelProject[i];
                            len = len + 1;
                        }
                    }
                    //$rootScope.goalSelProject = goals;
                    /* var goalToSelect = [];
                     for (var g=0; g<$rootScope.measurementGoalList.length; g++){
                         goalToSelect.push($rootScope.measurementGoalList[g]);
                     }
                     var selIndex = 0;*/

                    var j = 0;
                    for (var i = 0; i < $rootScope.goalSelProject.length; i++) {

                        var select = document.getElementById("goalList");

                        //select.options[i] = new Option($rootScope.goalSelProject[i].id + "." + $rootScope.goalSelProject[i].object, 'Goal' + i);

                        if ($rootScope.goalSelProject[i].subGoals.length === 0) {
                            select.options[j] = new Option($rootScope.goalSelProject[i].title, 'Goal' + j);
                            j++;

                        } else {
                            $rootScope.goalSelProject.splice(i, 1);
                            i--;
                        }

                        /*for (var j=0; j<goalToSelect.length; j++){
                            if (goalToSelect[j].organizationalGoalId.instance == $rootScope.goalSelProject[i].title){

                                if (goalToSelect[j].interpretationModel.functionJavascript != "") {
                                    select.options[selIndex] = new Option($rootScope.goalSelProject[i].title, 'Goal' + i);
                                    selIndex ++;
                                }

                            }
                        }*/
                    }
                    console.log("DOPO SPLICE ", $rootScope.goalSelProject);
                });
            });

            $rootScope.goalSelProject = [];
            $rootScope.metricSelGoal = [];

            //Start append
            $scope.append_metrics = function (parentData, parentId) {
                if (parentData == null)
                    return null;

                var mtrx = {};

                for (i = 0; i < parentData.length; i++) {
                    mtrx = parentData[i];
                    $rootScope.metricSelGoal.push(mtrx);

                }
                if ($scope.disableImportGrid == false)
                    $scope.disableImportGrid = !$scope.disableImportGrid;
            };

            $scope.append_questions = function (parentData, parentId) {

                if (parentData == null)
                    return null;

                var quest = {};

                for (i = 0; i < parentData.length; i++) {
                    quest = parentData[i];

                }
                return null;
            };

            $scope.append_measurements = function (parentData,
                                                   parentId) {
                if (parentData == null)
                    return null;

                var mg = {};

                for (i = 0; i < parentData.length; i++) {
                    mg = parentData[i];

                }
                for (var i = 0; i < parentData.length; i++) {
                    mg = parentData[i];
                    $scope.append_questions(mg.questionsRef, mg.measurementGoalId);
                    $scope.append_metrics(mg.metricsRef, mg.measurementGoalId);
                }
            };

            $scope.append_subGoal = function (parentData, parentId) {

                if (parentData == null)
                    return null;

                var sbgoal = {};

                for (i = 0; i < parentData.length; i++) {
                    sbgoal = parentData[i];

                    $rootScope.goalSelProject.push(sbgoal);

                }
                for (var i = 0; i < parentData.length; i++) {
                    sbgoal = parentData[i];
                    $scope.append_strategy(sbgoal.strategyRef, sbgoal.goalId);
                    $scope.append_measurements(sbgoal.measurementRef, sbgoal.goalId);
                }
            };

            $scope.append_strategy = function (parentData, parentId) {
                if (parentData == null)
                    return null;
                var s = {};

                for (i = 0; i < parentData.length; i++) {
                    s = parentData[i];

                }
                for (var i = 0; i < parentData.length; i++) {
                    s = parentData[i];
                    $scope.append_subGoal(s.goalRef, s.strategyId);
                }
            };

            $rootScope.append_goal = function (parentId, parentData) {

                if (parentData == null)
                    return null;

                var goal = {};

                for (var i = 0; i < parentData.length; i++) {
                    goal = parentData[i];

                    $rootScope.goalSelProject.push(goal);

                }

                for (var i = 0; i < parentData.length; i++) {
                    goal = parentData[i];
                    $scope.append_strategy(goal.strategyRef, goal.goalId);
                    $scope.append_measurements(goal.measurementRef, goal.goalId);
                }
            };
            //End append


            //Dato il goal selezionato, genera la lista di metriche associate.
            $scope.getSelGoalIndex = function () {
                ontologyListSel = [];
                $rootScope.metricSelGoal = [];
                var el = document.getElementById("goalList");
                var selIndex = el.selectedIndex;

                $rootScope.selGoal = $rootScope.goalSelProject[selIndex];


                $rootScope.title = $rootScope.goalSelProject[selIndex].organizationalScope;

                if ($rootScope.measurementGoalList != null) {
                    for (var i = 0; i < $rootScope.measurementGoalList.length; i++) {
                        if ($rootScope.measurementGoalList[i].organizationalGoalId.instance == $rootScope.selGoal.title) {
                            mgSel = $rootScope.measurementGoalList[i];
                        }
                    }

                    cc.thresholdService = new ThresholdService();

                    var titleEl = document.getElementById("title");
                    titleEl.innerHTML = $rootScope.selGoal.title;

                    console.log("interpretationModel: ", mgSel);

                    console.log("interpretationModel: ", mgSel.interpretationModel.functionJavascript);

                    document.getElementById("interpretationModel").innerHTML = "Interpretation Model: " + mgSel.interpretationModel.functionJavascript;

                    if (mgSel.interpretationModel.functionJavascript != "") {

                        ThresholdService.getThresholds({im: btoa(mgSel.interpretationModel.functionJavascript)}).$promise.then(function (data) {
                            console.log("threshold:");
                            console.log(data);
                            $scope.thresholdList = data.thresholdList;
                        }, function (err) {
                            console.log(err);
                            document.getElementById("viewChart").style.visibility = "hidden";
                            alert(err.data.message);
                            console.log("error in get thresholds");
                        });

                    }
                    if (mgSel.metrics != null) {
                        var l = 0;
                        for (var i = 0; i < mgSel.metrics.length; i++) {
                            for (var j = 0; j < $rootScope.ontologiesList.length; j++) {
                                if (mgSel.metrics[i].instance == $rootScope.ontologiesList[j].id) {
                                    ontologyListSel[l] = $rootScope.ontologiesList[j];
                                    l++;
                                }
                            }
                        }
                        console.log("ONTOLOGYLIST", ontologyListSel);
                        for (var k = 0; k < ontologyListSel.length; k++) {
                            $rootScope.metricSelGoal[k] = ontologyListSel[k];
                            $scope.metricListVar[k] = ontologyListSel[k];
                        }

                        document.getElementById("viewChart").disabled = false;
                        $scope.append_metrics(ontologyListSel, mgSel.id);
                        /*var select = document.getElementById("metricList");
                        for (var i = 0; i < select.options.length; i++){
                            select.options[i] = null;
                        }
                        for (var i = 0; i < ontologyListSel.length; i++){
                            var select = document.getElementById("metricList");
                            select.options[i] = new Option(ontologyListSel[i].measurementModel.description, 'Metric'+i);
                        } */
                    }
                    else {
                        document.getElementById("viewChart").disabled = true;
                    }
                }
                else {
                    document.getElementById("viewChart").disabled = true;
                    /* var select = document.getElementById("metricList");
                     for(var i=0; i< select.length; i++)
                     {
                         select.remove(i);
                     }
                     select.options[0] = new Option("", null); */
                }
            };

            //Setta la variabile $selMetric con la metrica selezionata.
            $scope.getSelMetricIndex = function () {
                /* var el = document.getElementById("metricList");
                 var selIndex = el.selectedIndex;
                 if(selIndex != null){
                     $rootScope.selMetric = $rootScope.metricSelGoal[selIndex];
                 }

                 else {
                     console.log("Non hai selezionato nessuna metrica!");
                 } */
            }

            $scope.getData = function () {
//		document.getElementById("dataAdj").disabled = false;
                $scope.getSelMetricIndex();
                //console.log("Metrica selezionata: " + JSON.stringify($rootScope.selMetric));
                $rootScope.allValData = [];
                //	$http.get("http://localhost:8080/getValidateData/").then(function(response)
                /*            var metricSel = [];
                            for (var i = 0; i < $rootScope.selMetric.measurements.length; i++){
                                var dataValue = parseFloat($rootScope.selMetric.measurements[i].value);
                                $rootScope.allValData[i] = dataValue;

                            } */

                //$rootScope.allValData = $rootScope.selMetric.measurements;
                if ($rootScope.allValData != null) {
                    //$rootScope.allValData = response.data.validateData;

                    /*$rootScope.allValData.push(
                        {
                            improvementId: "improvementId",
                            data: $rootScope.allValData.data,
                            dataAdjId: "allValData"

                        });
                    //}*/

                    $('#chartContainer').show('slow');


                    if ($rootScope.allValData.length == 0) {
                        console.log("Non ci sono dati validati.");
                        //document.getElementById("dataAdj").disabled = true;
                    }
                }
                /*       $http.get("http://localhost:8082/getValidateData/").then(function(response) {
                           validatedDatas = response.data.validateData;
                           console.log("validatedDatas");
                           console.log(validatedDatas);

                           for (i = 0; i<validatedDatas.length; i++){
                               metricSel = validatedDatas[i].metricRef;
                               console.log(i);
                               console.log(metricSel);
                               if (metricSel.metricId === $rootScope.selMetric.metricId){
                                   console.log("FIND");
                                   $rootScope.myData = validatedDatas[i];
                                   console.log($rootScope.myData);
                               }
                           }


                           if($rootScope.myData!=null) {
                               //$rootScope.allValData = response.data.validateData;
                               $rootScope.allValData = $rootScope.myData;

                               $rootScope.arrValData.push(
                                   {
                                       improvementId: $rootScope.myData.improvementId,
                                       data: $rootScope.myData.data,
                                       dataAdjId: $rootScope.myData.businessWorkflowInstanceId

                                   });
                               //}
                               $('#chartContainer').show('slow');

                               if ($rootScope.allValData.length == 0) {
                                   console.log("Non ci sono dati validati.");
                                   document.getElementById("dataAdj").disabled = true;
                               }
                           }

                       });  */

//		).then(function(response) {
//            console.log("Success!");
//	    }, 
//	    function(response) { // optional
//	            console.log("Failed!");
//	    });
                $scope.parseX();
            };

            $scope.parseX = function () {
                console.log("--------INIZIO PARSEX---------")
                $scope.arr = [];
                var separator = 0;

                $scope.timeFrame = $rootScope.selGoal.timeFrame;
                console.log("Timeframe preso ", $scope.timeFrame);
                $scope.creationDate = mgSel.creationDate;
                console.log("creationDate presa ", $scope.creationDate);

                var d = new Date();
                for (var i = 0; i < $scope.creationDate.length; i++) {
                    if ($scope.creationDate.charAt(i) == '/' || $scope.creationDate.charAt(i) == '-') {
                        $scope.arr.push($scope.creationDate.substr(separator, i - separator));
                        separator = i + 1;
                    }
                }

                $scope.arr.push($scope.creationDate.substr(separator, $scope.creationDate.length - separator));
                //console.log("arr = " + $scope.arr);
                d.setDate($scope.arr[0]);
                d.setMonth($scope.arr[1] - 1);
                d.setFullYear($scope.arr[2]);
                var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                $scope.arrX = [];
                for (var j = 0; j < $scope.timeFrame.length; j++) {
                    //     	console.log("time frame: "+$scope.timeFrame);
                    var num;
                    if ($scope.timeFrame.substr(j, 4) == "mesi" || $scope.timeFrame.substr(j, 4) == "mese") {
                        for (var i = 0; i < $scope.timeFrame.length; i++) {
                            if ($scope.timeFrame.substr(i, 1) == " ") {
                                num = $scope.timeFrame.substr(0, i);
                                for (var k = 0; k < num; k++) {
                                    $scope.arrX.push(monthNames[d.getMonth() + k]);
                                    if (d.getMonth() + k == monthNames.length - 1) {
                                        for (var z = 0; z < num - k - 1; z++)
                                            $scope.arrX.push(monthNames[z]);
                                        k = num - 1;
                                    }
                                }
                            }
                        }
                    }

                    /*        	//NON NECESSARIO!
                                 else if($scope.timeFrame.substr(j,4) == "mese")
                                {
                                    $scope.arrX.push(d.getDate());
                                    for(var i=0; i<3; i++)
                                    {
                                        d.setDate(d.getDate()+7);
                                        $scope.arrX.push(d.getDate())
                                    }
                                    for(var i=0; i<$scope.arrX.length; i++)
                                        console.log($scope.arrX[i].day+" "+ $scope.arrX[i].month);
                                }*/
                    else if ($scope.timeFrame.substr(j, 4) == "anni" || $scope.timeFrame.substr(j, 4) == "anno") {
                        for (var i = 0; i < $scope.timeFrame.length; i++) {
                            if ($scope.timeFrame.substr(i, 1) == " ") {
                                num = $scope.timeFrame.substr(0, i);
                                for (var k = 0; k < num; k++) {
                                    $scope.arrX.push(d.getFullYear() + k);
                                }
                            }
                        }
                    }
                    /*			NON NECESSARIO!!!
                                else if($scope.timeFrame.substr(j,4) == "anno")
                                {
                                    for(var k=0; k<12; k++)
                                    {
                                        console.log(monthNames[d.getMonth()+k]);
                                        $scope.arrX.push(monthNames[d.getMonth()+k]);
                                        if(d.getMonth()+k == monthNames.length-1)
                                        {
                                            for(var z= 0; z<11-k; z++)
                                                $scope.arrX.push(monthNames[z]);
                                            k=11;
                                        }
                                    }
                                }*/
//        	else console.log("Time frame mal specificato.");
                }
            };

            var validateMetric = function (metric, threshold) {
                switch (threshold.conditionTypes) {
                    case "==": {
                        if ((metric.measurements[metric.measurements.length - 1].value) == threshold.threshold) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    case "<=": {
                        if ((metric.measurements[metric.measurements.length - 1].value) <= threshold.threshold) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    case ">=": {
                        if ((metric.measurements[metric.measurements.length - 1].value) >= threshold.threshold) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    case "<": {
                        if ((metric.measurements[metric.measurements.length - 1].value) < threshold.threshold) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    case ">": {
                        if ((metric.measurements[metric.measurements.length - 1].value) > threshold.threshold) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    case "!=": {
                        if ((metric.measurements[metric.measurements.length - 1].value) != threshold.threshold) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    default: {
                        return false;
                    }
                }
                //if (metric.measurement[metric.measurement.length-1].value)
            }

            var insertThreshold = function (selectedOnt) {
                /*console.log("THRESHOLD");
                var str2 = "inputTreshold"+ id;
                var str3 = "\"" + str2 + "\"";
                var str = "input[name="+ str3+"]";
                console.log(str);
                console.log(document.querySelector(str).value);
                $scope.thresholds[id] = document.querySelector(str).value;
                console.log("treshold");
                console.log($scope.thresholds);*/
                console.log("INSERT THRESHOLD");
                var thr = [];
                if (selectedOnt != null) {
                    for (var i = 0; i < $scope.thresholdList.length; i++) {
                        if ($scope.thresholdList[i].metric == selectedOnt.measurementModel.name) {
                            thr.push($scope.thresholdList[i]);
                        }
                    }
                }
                console.log("THREASHOLDLIST");
                console.log($scope.thresholdList);
                return thr;
            };


            var cross = new Image();
            cross.src = "./image/cross.png";
            var check = new Image();
            check.src = "./image/check.png";


            $scope.createLineChart = function () {
                document.getElementById("chartContainer").style.visibility = "visible";
                /*$("#areaChart").hide();
                $("#barChart").hide();
                $("#lineChart").show();*/
                $scope.line = true;
                $scope.bar = false;
                $scope.area = false;
                console.log("OntoListSel +    ", ontologyListSel);
                console.log("OntoListSel +    ", ontologyListSel);
                /* document.getElementById("title").innerHTML = "";
                 document.getElementById("title").innerHTML = $rootScope.selGoal.title;
 */
                var element = document.getElementById("strategies");

                //Rimuove strategie relative a grafici precedentemente visualizzati.
                /*while (element.firstChild) {
                    element.removeChild(element.firstChild);
                }*/
                if (toClose.length > 0) {
                    toClose.forEach(function (t) {
                        console.log('#' + t);
                        $('#' + t).hide();
                    });
                    toClose = [];
                }

                var timeDataX = [];

                for (var b = 0; b < ontologyListSel.length; b++) {

                    console.log("ONTOLOGY LIST SEL", ontologyListSel);
                    var idCanvas = "lineChart" + ontologyListSel[b].id;
                    toClose.push(idCanvas);
                    var dataToGraph = {};

                    var datas = [];
                    validatedDatas = ontologyListSel[b].measurements;
                    if (validatedDatas == null) continue;
                    toClose.push(idCanvas);
                    $("#" + idCanvas).show();
                    for (var i = 0; i < validatedDatas.length; i++) {
                        var dataValue = parseFloat(validatedDatas[i].value);
                        datas.push(dataValue);
                        var timeValue = validatedDatas[i].timestamp;
                        $rootScope.arrValData[i] = dataValue;
                        dataToGraph[i] = dataValue;
                        timeDataX[i] = timeValue;
                    }
                    console.log("TimeDataX ", timeDataX);


                    var canvas = document.getElementById(idCanvas);
                    var ctx = canvas.getContext("2d");

                    var thr = insertThreshold(ontologyListSel[b]);

                    //document.getElementById("imMetric").innerHTML = "WANTED: "+ thr[0].metric +" "+ thr[0].conditionTypes +" "+ thr[0].threshold;


                    for (var i = 0; i < $scope.thresholdList.length; i++) {
                        if ($scope.thresholdList[i].metric === ontologyListSel[b].measurementModel.name)
                        //$scope.thresholdList[i].validated = validateMetric(ontologyListSel[b], thr[i]);
                            $scope.thresholdList[i].validated = validateMetric(ontologyListSel[b], $scope.thresholdList[i]);
//console.log(thr[i]);
                        /* if (validatedMetric) {
                             document.getElementById("validateMetric").innerHTML = "METRIC VALIDATED";
                             document.getElementById("validationDiv").className = "alert alert-success";
                         } else {
                             document.getElementById("validateMetric").innerHTML = "METRIC NOT VALIDATED";
                             document.getElementById("validationDiv").className = "alert alert-danger";
                         }*/
                    }


                    //var treshold = parseInt($scope.treshold);
                    var horizonalLinePlugin = {
                        afterDraw: function (chartInstance) {
                            var yScale = chartInstance.scales["y-axis-0"];
                            var canvas = chartInstance.chart;
                            var ctx = canvas.ctx;
                            var index;
                            var line;
                            var style;

                            /*console.log(chartInstance.config.data.datasets[0]._meta[0]);

                            if(chartInstance.options.validated!=null)
                            if (chartInstance.options.validated) {
                                chartInstance.config.data.datasets[0]._meta[0].data[len - 1]._model.pointStyle = check;
                            } else {
                                chartInstance.config.data.datasets[0]._meta[0].data[len - 1]._model.pointStyle = cross;
                            }*/

                            if (chartInstance.options.horizontalLine) {
                                for (index = 0; index < chartInstance.options.horizontalLine.length; index++) {
                                    line = chartInstance.options.horizontalLine[index];

                                    if (!line.style) {
                                        style = "rgba(169,169,169, .6)";
                                    } else {
                                        style = line.style;
                                    }

                                    if (line.y) {
                                        yValue = yScale.getPixelForValue(line.y);
                                    } else {
                                        yValue = 0;
                                    }

                                    ctx.lineWidth = 3;

                                    if (yValue) {
                                        ctx.beginPath();
                                        ctx.moveTo(0, yValue);
                                        ctx.lineTo(canvas.width, yValue);
                                        ctx.strokeStyle = style;
                                        ctx.stroke();
                                    }

                                    if (line.text) {
                                        ctx.fillStyle = style;
                                        ctx.fillText(line.text, 0, yValue + ctx.lineWidth);
                                    }
                                }
                                return;
                            }
                            ;
                        }
                    };
                    Chart.pluginService.register(horizonalLinePlugin);

                    /*var validatePlugin = {
                        afterUpdate: function (chart) {

                            var len = datas.length;

                            if (validateMetric(ontologyListSel[b], thr)) {
                                console.log("VALIDATED");
                                chart.config.data.datasets[0]._meta[0].data[len - 1]._model.pointStyle = check;
                            }
                            else {
                                console.log("NOT VALIDATED")
                                chart.config.data.datasets[0]._meta[0].data[len - 1]._model.pointStyle = cross;
                            }
                        }
                    };*/
                    //Chart.pluginService.register(validatePlugin);

                    var randomRgba = function () {
                        var letters = '0123456789ABCDEF'.split('');
                        var color = '#';
                        for (var i = 0; i < 6; i++) {
                            color += letters[Math.floor(Math.random() * 16)];
                        }
                        return color;
                    }


                    var datasetValue = [];

                    if (validatedDatas.length > 0) {
                        console.log("Ci sono dati validati per la metrica selezionata.");
                        /*var titleEl = document.getElementById("title");
                        titleEl.innerHTML = $rootScope.selGoal.title;*/


                        //for(var i =0; i<$rootScope.arrValData.length; i++)
                        // {

                        var rgba = randomRgba();
                        var p = document.createElement("p");
                        var span1 = document.createElement("span");
                        var span2 = document.createElement("span");
                        var span3 = document.createElement("span");
                        span1.style.color = "#000000";
                        span2.style.color = rgba;
                        span3.style.color = "#000000";
                        var text1 = document.createTextNode(" ");
                        span1.appendChild(text1);

                        p.appendChild(span1);
                        span1.appendChild(span2);
                        span2.appendChild(span3);
                        element.appendChild(p);

                        datasetValue[0] = {
                            label: ontologyListSel[b].measurementModel.name + ": " + ontologyListSel[b].measurementModel.description,
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: rgba,
                            borderColor: rgba,
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            pointBorderColor: rgba,
                            pointBackgroundColor: "#fff",
                            pointBorderWidth: 1,
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: rgba,
                            pointHoverBorderColor: rgba,
                            pointHoverBorderWidth: 2,
                            pointRadius: 1,
                            pointHitRadius: 10,
                            data: dataToGraph,
                        }

                        //}
                    }
                    else {
                        console.log("Non ci sono dati validati.");
                        var element = document.getElementById("title");
                        element = "";
                        element.innerHTML = "The metric " + $rootScope.selMetric.id + " doesn't have validated data";
                        element.style.color = "#ff0000";

                        var dat = {
                            labels: timeDataX,
                            datasets: datasetValue,
                            legend: {
                                verticalAlign: "bottom",
                                horizontalAlign: "center"
                            },
                        }
                    }


                    var data = {
                        labels: timeDataX,
                        datasets: datasetValue,
                    };


                    console.log(validatedDatas);
                    console.log(Math.max(...datas) + 1);
                    var thrRows = [];
                    var thrs = [];
                    angular.forEach(thr, function (t) {
                        thrRows.push({
                            "y": t.threshold,
                            "style": "rgb(0, 51, 102)",
                        });
                        thrs.push(t.threshold);
                    });

                    var limits = {
                        min: null,
                        max: null
                    };

                    if (Math.min.apply(null, thrs) < Math.min.apply(null, datas)) {
                        limits.min = Math.min.apply(null, thrs) - 1;
                    } else {
                        limits.min = Math.min.apply(null, datas) - 1;
                    }

                    if (Math.max.apply(null, thrs) > Math.max.apply(null, datas)) {
                        limits.max = Math.max.apply(null, thrs) + 1;
                    } else {
                        limits.max = Math.max.apply(null, datas) + 1;
                    }

                    var myLineChart = new Chart(ctx, {
                        type: 'line',
                        data: data,
                        options: {
                            "horizontalLine": thrRows,
                            scales: {
                                yAxes: [{
                                    ticks: limits
                                }]
                            }
                        }


                    });

                }
            };

            $scope.createAreaChart = function () {


                document.getElementById("chartContainer").style.visibility = "visible";
                /*$("#lineChart").hide();
                $("#barChart").hide();
                $("#areaChart").show();*/
                $scope.line = false;
                $scope.bar = false;
                $scope.area = true;
                //document.getElementById("title").innerHTML = $rootScope.selGoal.title;
                var element = document.getElementById("strategies");

                //Rimuove strategie relative a grafici precedentemente visualizzati.
                /*while (element.firstChild) {
                    element.removeChild(element.firstChild);
                }*/
                if (toClose.length > 0) {
                    toClose.forEach(function (t) {
                        console.log('#' + t);
                        $('#' + t).hide();
                    });
                    toClose = [];
                }

                for (var b = 0; b < ontologyListSel.length; b++) {
                    var idCanvas = "areaChart" + ontologyListSel[b].id;
                    toClose.push(idCanvas);
                    var dataToGraph = {};
                    var timeDataX = [];
                    var datas = [];

                    validatedDatas = ontologyListSel[b].measurements;
                    if (validatedDatas == null) continue;
                    toClose.push(idCanvas);
                    $("#" + idCanvas).show();
                    for (var i = 0; i < validatedDatas.length; i++) {
                        var dataValue = parseFloat(validatedDatas[i].value);
                        var timeValue = validatedDatas[i].timestamp;
                        $rootScope.arrValData[i] = dataValue;
                        dataToGraph[i] = dataValue;
                        datas.push(dataValue);
                        timeDataX[i] = timeValue;
                    }

                    var thr = insertThreshold(ontologyListSel[b]);

                    //document.getElementById("imMetric").innerHTML = "WANTED: "+ thr[0].metric +" "+ thr[0].conditionTypes +" "+ thr[0].threshold;


                    var canvas = document.getElementById(idCanvas);
                    console.log("CANVAS: ", canvas);
                    var ctx = canvas.getContext("2d");
                    // var treshold = parseInt($scope.treshold);

                    for (var i = 0; i < $scope.thresholdList.length; i++) {
                        if ($scope.thresholdList[i].metric === ontologyListSel[b].measurementModel.name)
                        //$scope.thresholdList[i].validated = validateMetric(ontologyListSel[b], thr[i]);
                            $scope.thresholdList[i].validated = validateMetric(ontologyListSel[b], $scope.thresholdList[i]);

                        /* if (validatedMetric) {
                             document.getElementById("validateMetric").innerHTML = "METRIC VALIDATED";
                             document.getElementById("validationDiv").className = "alert alert-success";
                         } else {
                             document.getElementById("validateMetric").innerHTML = "METRIC NOT VALIDATED";
                             document.getElementById("validationDiv").className = "alert alert-danger";
                         }*/
                    }
                    var horizonalLinePlugin = {
                        afterDraw: function (chartInstance) {
                            var yScale = chartInstance.scales["y-axis-0"];
                            var canvas = chartInstance.chart;
                            var ctx = canvas.ctx;
                            var index;
                            var line;
                            var style;

                            /*console.log(chartInstance.config.data.datasets[0]._meta[0]);

                            if(chartInstance.options.validated!=null)
                            if (chartInstance.options.validated) {
                                chartInstance.config.data.datasets[0]._meta[0].data[len - 1]._model.pointStyle = check;
                            } else {
                                chartInstance.config.data.datasets[0]._meta[0].data[len - 1]._model.pointStyle = cross;
                            }*/

                            if (chartInstance.options.horizontalLine) {
                                for (index = 0; index < chartInstance.options.horizontalLine.length; index++) {
                                    line = chartInstance.options.horizontalLine[index];

                                    if (!line.style) {
                                        style = "rgba(169,169,169, .6)";
                                    } else {
                                        style = line.style;
                                    }

                                    if (line.y) {
                                        yValue = yScale.getPixelForValue(line.y);
                                    } else {
                                        yValue = 0;
                                    }

                                    ctx.lineWidth = 3;

                                    if (yValue) {
                                        ctx.beginPath();
                                        ctx.moveTo(0, yValue);
                                        ctx.lineTo(canvas.width, yValue);
                                        ctx.strokeStyle = style;
                                        ctx.stroke();
                                    }

                                    if (line.text) {
                                        ctx.fillStyle = style;
                                        ctx.fillText(line.text, 0, yValue + ctx.lineWidth);
                                    }
                                }
                                return;
                            }
                            ;
                        }
                    };
                    Chart.pluginService.register(horizonalLinePlugin);

                    /*var validatePlugin = {
                        afterUpdate: function (chart) {

                            var len = datas.length;

                            if (validateMetric(ontologyListSel[b], thr)) {
                                console.log("VALIDATED");
                                chart.config.data.datasets[0]._meta[0].data[len - 1]._model.pointStyle = check;
                            }
                            else {
                                console.log("NOT VALIDATED")
                                chart.config.data.datasets[0]._meta[0].data[len - 1]._model.pointStyle = cross;
                            }
                        }
                    };*/
                    //Chart.pluginService.register(validatePlugin);


                    var randomRgba = function () {
                        var letters = '0123456789ABCDEF'.split('');
                        var color = '#';
                        for (var i = 0; i < 6; i++) {
                            color += letters[Math.floor(Math.random() * 16)];
                        }
                        console.log(color);
                        return color;
                    }

                    var datasetValue = [];


                    if (validatedDatas.length > 0) {
                        /*var titleEl = document.getElementById("title");
                        titleEl.innerHTML = $rootScope.selGoal.title;
                        titleEl.style.color = "#000000";*/
                        //Start dynamic area
                        //for (var i = 0; i < $rootScope.arrValData.length; i++) {
                        var rgba = randomRgba();
                        var p = document.createElement("p");
                        var span1 = document.createElement("span");
                        var span2 = document.createElement("span");
                        var span3 = document.createElement("span");
                        span1.style.color = "#000000";
                        span2.style.color = rgba;
                        span3.style.color = "#000000";
                        var text1 = document.createTextNode(" ");
                        span1.appendChild(text1);
                        p.appendChild(span1);
                        span1.appendChild(span2);
                        span2.appendChild(span3);
                        element.appendChild(p);

                        datasetValue[0] = {
                            label: ontologyListSel[b].measurementModel.name + ": " + ontologyListSel[b].measurementModel.description,
                            fillColor: "rgba(255,255,255,1)",
                            strokeColor: "rgba(255,255,255,1)",
                            pointColor: "rgba(255,255,255,1)",
                            backgroundColor: rgba,
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(255,255,255,1)",
                            data: dataToGraph
                        }

                        console.log("TIMEDATA", timeDataX);
                        //}
                        //INSERISCO VALUE X
                        var dat = {
                            labels: timeDataX,
                            datasets: datasetValue,
                            legend: {
                                verticalAlign: "bottom",
                                horizontalAlign: "center"
                            },
                        }
                        console.log("DATA ASSE X");
                        console.log(datasetValue);
                        console.log($scope.arrX);
                        console.log("dat = " + JSON.stringify(dat));
                        //end dynamic area

                    }
                    else {
                        var element = document.getElementById("title");
//			document.getElementById("dataAdj").disabled = true;
                        element.innerHTML = "The metric " + $rootScope.selMetric.id + " doesn't have validated data";
                        element.style.color = "#ff0000";

                        var dat = {
                            labels: timeDataX,
                            datasets: datasetValue,
                            legend: {
                                verticalAlign: "bottom",
                                horizontalAlign: "center"
                            },
                        }
                    }

                    thrRows = [];
                    var thrs = [];
                    angular.forEach(thr, function (t) {
                        thrRows.push({
                            "y": t.threshold,
                            "style": "rgb(0, 51, 102)",
                        });
                        thrs.push(t.threshold);
                    });

                    var limits = {
                        min: null,
                        max: null
                    };

                    if (Math.min.apply(null, thrs) < Math.min.apply(null, datas)) {
                        limits.min = Math.min.apply(null, thrs) - 1;
                    } else {
                        limits.min = Math.min.apply(null, datas) - 1;
                    }

                    if (Math.max.apply(null, thrs) > Math.max.apply(null, datas)) {
                        limits.max = Math.max.apply(null, thrs) + 1;
                    } else {
                        limits.max = Math.max.apply(null, datas) + 1;
                    }

                    var myNewChart = new Chart(ctx, {
                        type: 'line',
                        data: dat,
                        options: {
                            "horizontalLine": thrRows,
                            scales: {
                                yAxes: [{
                                    ticks: limits
                                }]
                            }
                        }

                    });
                }
            }

            $scope.createBarChart = function () {


                document.getElementById("chartContainer").style.visibility = "visible";
                /*$("#lineChart").hide();
                $("#areaChart").hide();
                $("#barChart").show();*/
                $scope.line = false;
                $scope.bar = true;
                $scope.area = false;
                //document.getElementById("title").innerHTML = $rootScope.selGoal.title;
                //document.getElementById("modeltitle").innerHTML = "Modello Interpretativo"; //TODO MODELLO INTERPRETATIVO
                //document.getElementById("intmodel").innerHTML = "if goal reach treshold value, then the goal is achieved!";


                var element = document.getElementById("strategies");
                var randomRgba = function () {
                    var letters = '0123456789ABCDEF'.split('');
                    var color = '#';
                    for (var i = 0; i < 6; i++) {
                        color += letters[Math.floor(Math.random() * 16)];
                    }
                    return color;
                }

                //Rimuove strategie relative a grafici precedentemente visualizzati.

                /*while (element.firstChild) {
                    element.removeChild(element.firstChild);
                }*/
                if (toClose.length > 0) {
                    toClose.forEach(function (t) {
                        console.log('#' + t);
                        $('#' + t).hide();
                    });
                    toClose = [];
                }
                for (var b = 0; b < ontologyListSel.length; b++) {
                    var idCanvas = "barChart" + ontologyListSel[b].id;
                    toClose.push(idCanvas);
                    var dataToGraph = {};
                    var timeDataX = [];

                    console.log("ONTOLOGYLIST", ontologyListSel);
                    validatedDatas = ontologyListSel[b].measurements;
                    console.log("ValidateData", validatedDatas);
                    if (validatedDatas == null) continue;
                    toClose.push(idCanvas);
                    $("#" + idCanvas).show();
                    var datas = [];
                    for (var i = 0; i < validatedDatas.length; i++) {
                        var dataValue = parseFloat(validatedDatas[i].value);
                        var timeDataValue = validatedDatas[i].timestamp;
                        $rootScope.arrValData[i] = dataValue;
                        dataToGraph[i] = dataValue;
                        datas.push(dataValue);
                        timeDataX[i] = timeDataValue;
                    }
                    console.log("TIMEDATAX", timeDataX);

                    var thr = insertThreshold(ontologyListSel[b]);

                    //document.getElementById("imMetric").innerHTML = "WANTED: "+ thr[0].metric +" "+ thr[0].conditionTypes +" "+ thr[0].threshold;

                    for (var i = 0; i < $scope.thresholdList.length; i++) {
                        if ($scope.thresholdList[i].metric === ontologyListSel[b].measurementModel.name)
                        //$scope.thresholdList[i].validated = validateMetric(ontologyListSel[b], thr[i]);
                            $scope.thresholdList[i].validated = validateMetric(ontologyListSel[b], $scope.thresholdList[i]);

                        /* if (validatedMetric) {
                             document.getElementById("validateMetric").innerHTML = "METRIC VALIDATED";
                             document.getElementById("validationDiv").className = "alert alert-success";
                         } else {
                             document.getElementById("validateMetric").innerHTML = "METRIC NOT VALIDATED";
                             document.getElementById("validationDiv").className = "alert alert-danger";
                         }*/
                    }

                    var barChart = document.getElementById(idCanvas).getContext("2d");
                    //var treshold = parseInt($scope.treshold);
                    var horizonalLinePlugin = {
                        afterDraw: function (chartInstance) {
                            var yScale = chartInstance.scales["y-axis-0"];
                            var canvas = chartInstance.chart;
                            var ctx = canvas.ctx;
                            var index;
                            var line;
                            var style;

                            /*console.log(chartInstance.config.data.datasets[0]._meta[0]);


                            if(chartInstance.options.validated!=null)
                            if (chartInstance.options.validated) {
                                chartInstance.config.data.datasets[0]._meta[0].data[len - 1]._model.pointStyle = check;
                            } else {
                                chartInstance.config.data.datasets[0]._meta[0].data[len - 1]._model.pointStyle = cross;
                                console.log(chartInstance.config.data.datasets[0]._meta[0]);
                            }*/

                            if (chartInstance.options.horizontalLine) {
                                for (index = 0; index < chartInstance.options.horizontalLine.length; index++) {
                                    line = chartInstance.options.horizontalLine[index];

                                    if (!line.style) {
                                        style = "rgba(169,169,169, .6)";
                                    } else {
                                        style = line.style;
                                    }

                                    if (line.y) {
                                        yValue = yScale.getPixelForValue(line.y);
                                    } else {
                                        yValue = 0;
                                    }

                                    ctx.lineWidth = 3;

                                    if (yValue) {
                                        ctx.beginPath();
                                        ctx.moveTo(0, yValue);
                                        ctx.lineTo(canvas.width, yValue);
                                        ctx.strokeStyle = style;
                                        ctx.stroke();
                                    }

                                    if (line.text) {
                                        ctx.fillStyle = style;
                                        ctx.fillText(line.text, 0, yValue + ctx.lineWidth);
                                    }
                                }
                                return;
                            }
                            ;
                        }
                    };
                    Chart.pluginService.register(horizonalLinePlugin);

                    /*var validatePlugin = {
                        afterUpdate: function (chart) {

                            var len = datas.length;
                            if (validateMetric(ontologyListSel[b], thr)) {
                                console.log("VALIDATED");
                                chart.config.data.datasets[0]._meta[0].data[len - 1]._model.pointStyle = check;
                            }
                            else {
                                console.log("NOT VALIDATED")
                                chart.config.data.datasets[0]._meta[0].data[len - 1]._model.pointStyle = cross;
                            }
                        }
                    };*/
                    //Chart.pluginService.register(validatePlugin);

                    var datasetValue = [];

                    if (validatedDatas.length > 0) {

                        /*var titleEl = document.getElementById("title");
                        titleEl.innerHTML = $rootScope.selGoal.title;*/
                        var titlemodel = document.getElementById("modeltitle");


                        var rgba = randomRgba();
                        var p = document.createElement("p");
                        var span1 = document.createElement("span");
                        var span2 = document.createElement("span");
                        var span3 = document.createElement("span");
                        span1.style.color = "#000000";
                        span2.style.color = rgba;
                        span3.style.color = "#000000";
                        var text1 = document.createTextNode(" ");
                        span1.appendChild(text1);
                        p.appendChild(span1);
                        span1.appendChild(span2);
                        span2.appendChild(span3);
                        element.appendChild(p);


                        datasetValue[0] = {
                            label: ontologyListSel[b].measurementModel.name + ": " + ontologyListSel[b].measurementModel.description,
                            fillColor: "#48A497",
                            strokeColor: "#48A4D1",
                            backgroundColor: rgba,
                            borderColor: rgba,
                            data: dataToGraph
                        }


                        console.log("TIMEDATAX", timeDataX);
                        var dat = {
                            labels: timeDataX,
                            datasets: datasetValue,
                            legend: {
                                verticalAlign: "bottom",
                                horizontalAlign: "center"
                            },
                        }
                        //end bar charts


                    }
                    else {
                        var element = document.getElementById("title");
                        element.innerHTML = "The metric " + $rootScope.selMetric.id + " doesn't have validated data";
                        element.style.color = "#ff0000";


                        var dat = {
                            labels: timeDataX,
                            datasets: datasetValue,
                            legend: {
                                verticalAlign: "bottom",
                                horizontalAlign: "center"
                            },
                        }
                    }

                    thrRows = [];
                    angular.forEach(thr, function (t) {
                        thrRows.push({
                            "y": t.threshold,
                            "style": "rgb(0, 51, 102)",
                        });
                    });


                    var myBarChart = new Chart(barChart, {
                        type: 'bar',
                        data: dat,
                        options: {
                            "horizontalLine": thrRows
                        }
                    });
                }
            }


            $scope.sendImprovement = function () {


                var el = document.getElementById("goalList");
                var selIndex = el.selectedIndex;

                //Goal selezionato
                // $rootScope.selGoal = $rootScope.goalSelProject[selIndex];


                var goalId = $rootScope.selGoal.id;
                var comment = document.getElementById("improvecomment").value;
                console.log("ECCOMI");
                console.log($rootScope.arrValData);


                if ($rootScope.arrValData.length > 0) {
                    $http({
                        url: 'http://localhost:8082/createDataImprovement/',
                        method: "POST",
                        data: {
                            'message': comment,
                            'goalId': goalId,
                            'active': true

                        }
                    })
                        .then(function (response) {
                                window.alert("Improvement Sent");
                                console.log("Success!");
                            },
                            function (response) { // optional
                            window.alert("Failed");
                            console.log("Failed!");
                            });
                    $('#diagModal').modal('hide');


                }
            }

            $scope.sendData = function () {
                //get selected index
                if ($rootScope.arrValData.length > 0) {
                    var el = document.getElementById("strategyList");
                    var selIndex = el.selectedIndex;
                }

                var comment = document.getElementById("comment").value;

                var r = confirm("Are you sure to send the message?");
                if (r == true) {
                    console.log("ADJ " + $rootScope.arrValData.length)
                    if ($rootScope.arrValData.length > 0) {
                        $http({
                            url: 'http://localhost:8082/createDataAdjustement/',
                            method: "POST",
                            data: {
                                'businessWorkflowInstanceId': $rootScope.arrValData[selIndex].dataAdjId,
                                'issueMessage': comment
                            }
                        })
                            .then(function (response) {
                                    console.log("Success!");
                                },
                                function (response) { // optional
                                    console.log("Failed!");
                                });
                        $('#diagModal').modal('hide');
                    }
                    else {
                        for (var i = 0; i < $rootScope.allValData.length; i++) {
                            var prova = $rootScope.allValData[i];
                        }

                        $http({
                            url: 'http://localhost:8082/SendDataAdjustementToBus/',
                            method: "POST",
                            data: {
                                'businessWorkflowInstanceId': $rootScope.allValData[0].businessWorkflowInstanceId,
                                'issueMessage': comment
                            }
                        })
                            .then(function (response) {
                                    console.log("Success!: " + response);
                                },
                                function (response) { // optional
                                    console.log("Failed!: " + response);
                                });

                        $http({
                            url: 'http://localhost:8082/createDataAdjustement/',
                            method: "POST",
                            data: {
                                'businessWorkflowInstanceId': $rootScope.allValData[0].businessWorkflowInstanceId,
                                'issueMessage': comment
                            }
                        })
                            .then(function (response) {
                                    console.log("Success!");
                                },
                                function (response) { // optional
                                    console.log("Failed!");
                                });
                        $('#diagModal').modal('hide');
                    }
                }
                else {
                    console.log("NON hai completato l'invio!");
                }

                document.getElementById("comment").value = " ";
                document.getElementById("chartContainer").style.visibility = "hidden";
            }

        }
    );
})();