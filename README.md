#Start the project

To launch the application you need to go on application.properties and set some parameters:
 server.port corresponding to server port
 bus.address corresponding to bus address

```
server.port = 8082
bus.address = http://localhost:8080/inboundChannel.html

```
### Actions

To start in a right way the phase 5 project from the browser you need to go on this url : http://localhost:8082/home.html
and activate mongodb for bus applications and local db  

## Running 

Once you have started the project the pull from previous phases it will be done automatically

After doing this you need to click on Project and select it, so you can see the diagram visualization

##Interpretation model validation

In the JEXLvalidator.java the method analyzeInterpretationModel uses the library ANTLR if you want to modify the grammar you need to go in interpratationaModelGrammar.g4 
and for compile it you need to install the ANTLR plugin for intelliJ


